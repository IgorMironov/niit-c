/********************************************************************************
 * Программа, которая находит сумму чисел во введенной строке.                  *
 * Замечание:                                                                   *
 * Программа рассматривает непрерывные последовательности цифр в строке как     *
 * числа и обрабатывает их как единое целое. В программе предусмотреть ограни-  *
 * чение на максимальное число разрядов, то есть если пользователь вводит очень *
 * длинную последовательность цифр, её нужно разбить на несколько групп.        *
 * By Wilson Castro. 12.2016.                                                   *
 ********************************************************************************/
#include <stdio.h>
#include <string.h>
#include <stdlib.h> 
#define SIZE 256
#define MAX 4

int main() {

    char lineNums[SIZE] = {0};
    char divLineNums[MAX] = {0};
    int s = 0;
    int t = 0;
    int i = 0;
    int summa = 0;
    int value = 0;
    printf("Type your number\n");
    fgets(lineNums, SIZE, stdin);
    s = strlen(lineNums);
    lineNums[s - 1];

    for (i = 0; i <= s; i++) {
        if (lineNums[i] >= '0' && lineNums[i] <= '9') {
            divLineNums[t] = lineNums[i]; // разделяет строку на массив 4-ех элеметов
            value = atoi(divLineNums);    // переводит характер на цифру
            summa += value;  // производит сумму цифр
            t++;  //счетчик до 4
        }
            t = 0; 
    }
    printf("Element's summa of your massive equal %d", summa);
    return 0;
}
