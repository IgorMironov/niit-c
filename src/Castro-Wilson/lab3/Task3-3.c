/***************************************************
 * Программа, которая для введенной строки выводит *
 * самое длинное слово его длину.                  *
 * By Wilson Castro. 11.2016.                      *
 ***************************************************/
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define TRUE 1
#define FALSE 0

int main() {
    char line[SIZE] = {0};
    int i, j, s, k = 0;
    int lengthWord = 0;
    int newLengthWord = 0;
    int cuonterWords = 0;
    int firstLeter = 0;
    int secondLeter = 0;
    int firstWord = FALSE;
    int secundWord = FALSE;
    int indexFirstLeter = 0;
    int indexSecondLeter = 0;

    printf("Введите строку для обработки\n");
    fgets(line, 256, stdin);
    j = strlen(line);
    line[j - 1] = '\0';

    for (i = 1; i < j; i++) {
        if (i - 1 == 0 && line[i - 1] != ' ') { // пойск первого слово
            firstWord = TRUE;
            firstLeter = i - 1;
        } else
            if (line[i - 1] == ' ' && line[i] != ' ') { // пойск первого слово
            firstWord = TRUE;
            firstLeter = i;
        }
        if (line[i - 1] != ' ' && line[i] == ' ') { //цикл для случая окончаяния слова с пробелом 
            secundWord = TRUE;
            secondLeter = i - 1;
        }
        if (line[i - 1] != ' ' && line[i] == '\0') { //цикл для случая окончаяния слова и строка 
            secundWord = TRUE;
            secondLeter = i - 1;
        }

        if (firstWord == TRUE && secundWord == TRUE) { 
            cuonterWords++;
            for (k = firstLeter; k <= secondLeter; k++) {
                if (!line[k] != ' ') {
                    newLengthWord = (secondLeter - firstLeter) + 1;
                    if (lengthWord < newLengthWord) {  // цикл для сравнения длины слов.
                        lengthWord = newLengthWord;
                        indexFirstLeter = firstLeter;
                        indexSecondLeter = secondLeter;
                    }
                }
            }
            firstWord = FALSE;
            secundWord = FALSE;
            firstLeter = 0;
            secondLeter = 0;
        }
    }
    printf("Cамого длиного слова:\n");
    for (s = indexFirstLeter; s <= indexSecondLeter; s++) {
        printf("%c", line[s]);
    }
    printf("\n");
    printf("и содержит %d букв\n", lengthWord);
    printf("Количество введенних слов: %d\n", cuonterWords);
    return 0;
}
