/*
 * Программа, выводящая таблицу встречаемости символов
 * для введенной пользователем строки.
 * By Wilson Castro. 11.2016.
 */
#include <stdio.h>
#include <string.h>
#define SIZE 256
#define SIZE2 127

int main() {
    int count; //счетчик частоты повторения характер
    int caracter;
    char line[SIZE] = {0}; // массив для сохранения введеной строки
    int frecuency[SIZE2] = {0}; // массив для сохранения частоты повторов характера
    printf("Введите строку\n");
    fgets(line, 256, stdin);
     int j = strlen(line);
    line[j - 1] = '\0';
    for (count = 0; count <= SIZE; count++) { // счетчик частоты повторения характера
        ++frecuency [line[count]];
    }
    printf("%s%17s\n", "character", "Frecuency");
    for (caracter = 32; caracter <= SIZE2; caracter++) {
        if (frecuency[caracter] != 0) {
            printf("%6c%17d\n", caracter, frecuency[caracter]);
        }
    }
    return 0;
}