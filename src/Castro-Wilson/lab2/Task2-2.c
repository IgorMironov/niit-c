/*
 * Программа "Угадай число" 
 * by Wilson Castro. 11.2016
 */
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(void) {
    int i;
    int num = 0; // введеная цифра
    int n = 0; // переменная для random
    srand(time(0));
    n = rand() % 100;
    printf("Для угадания числа введите цифру от 0 до 100. Удачи!\n");
    scanf("%d", &num);
    puts(" ");
    while (num != n) {
        if (num < n) {
            printf("Введите цифру по больше ");
            scanf("%d", &num);
        }
        if (num > n) {
            printf("Введите цифру по меньше ");
            scanf("%d", &num);
        }
    }
    printf("Поздравляем, вы угадали число!");
    return 0;
}