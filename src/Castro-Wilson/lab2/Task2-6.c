/*
 * Программа, очищающая строку от лишних пробелов.
 * By Wilson Castro. 11.2016.
 */
#include <stdio.h>
#include <string.h>
#define SIZE 256
char frase[SIZE] = {0};

int main() {
    int i, j, s;
    printf("Введите строку\n");
    fgets(frase, 256, stdin);
    frase[strlen(frase) - 1] = 0;
    j = strlen(frase) - 1;
    for (i = 0; i <= j; i++) {
        while (frase[i] == ' ') {  // цикл для сдвига элементов строки на лево до польного исчезновения пробелов 
            for (s = i; s <= j; s++) 
                frase[s] = frase[s + 1];
        }
    }
    printf("%s", frase);
    return 0;
}