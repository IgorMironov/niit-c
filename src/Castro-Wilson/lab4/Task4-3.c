/******************************************************************************  
 * Программа, которая запрашивает строку и определяет, не является            *
 * ли строка палиндромом (одинаково читается и слева направо и справа налево) *
 * Замечание:                                                                 *
 * Цель задачи - применить указатели для быстрого сканирования строки с двух  *
 * концов.                                                                    * 
 * By Wilson Castro. 11.2016.                                                 *
 ******************************************************************************/
#include <stdio.h>
#include <string.h>

#define SIZE 80
#define TRUE 1
#define FALSE 0
int j = 0;
int palindr(const char* str);

int main() {
    char word[SIZE];
    printf("Введите слово\n");
    fgets(word, SIZE, stdin);
    j = strlen(word);
    word[j - 1] = '\0';
    if (palindr(word))
        printf("Введенное слово является палиндромом\n");
    else
        printf("Введенное слово  не является палиндромом\n");
    return 0;
}

int palindr(const char* str) {
    char *ptri = str;
    char *ptrj = &str[j - 2];

    while (ptri < ptrj) {
        if (*(ptri++) != *(ptrj--))
            return FALSE;
    }
    return TRUE;
}