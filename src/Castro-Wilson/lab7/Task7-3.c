/******************************************************************************
 * Программа, которая строит таблицу встречаемости символов для произвольного *
 * файла, имя которого задаётся в командной строке. Программа должна выводить * 
 * на экран таблицу встречаемости, отсортированную по убыванию частоты.       *
 * Замечание:                                                                 *
 * В программе необходимо определить структурный тип SYM, в котором нужно     *
 * хранить код символа и частоту встречаемости (вещественное число от 0 до 1).*
 * После анализа файла, массив структур SYM должен быть отсортирован по       *
 * частоте.                                                                   *    
 ******************************************************************************/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

struct sym 
{
	int symbol;
	float freq;
};

int main(int argc,char *argv[])
{
	int i=0, j=0;
	int character;
	int k=0;
	int counter=0;
	float count[256]={0};

	struct sym arr[256];
	for(i=0;i<256;i++)
	{
		arr[i].symbol=0;
		arr[i].freq=0;
	}

	FILE *fp=fopen(argv[1],"rt"); // abre el archivo 
	while((character=fgetc(fp))!=EOF) // lee el archivo letra por letra
	{
		counter++;  // cuneta la cantidad de letras en el archivo
		for(i=0; i<256; i++)  // rellena el masivo de estructuras 
		{
			if(arr[i].symbol==0) // se introduce al masivo el caracter 
			{
				arr[i].symbol=character;
				count[i]=1;  // se inicializa el contador de un caracter
				k++;  // contador de numero de simbolos encontrados en el archivo
				break;
			}
			if(character==arr[i].symbol) // si el caracter del archivo ya esta en el arreglo
			{                            // se aunmenta el contador de dicho simbolo     
				count[i]++;
				break;
			}
		}
	}
	fclose(fp);

	for(i=0; i<k; i++) //calculo de la frecuencia de aparecion de cada simb.
		arr[i].freq=(float)count[i]/counter;

	for(i=1; i<k; i++) // ordenamiento por decremento de frec. de simb 
		for(j=0; j<k-1; j++)
			if(arr[j].freq<arr[j+1].freq)
			{
				float temp=arr[j].freq;
				arr[j].freq=arr[j+1].freq;
				arr[j+1].freq=temp;

				temp=arr[j].symbol;
				arr[j].symbol=arr[j+1].symbol;
				arr[j+1].symbol=temp;
			}

	for(i=0; i<k; i++)
		printf("Symbol: %c - frecuency: %f\n",arr[i].symbol,arr[i].freq);
	
	return 0;
}