/******************************************************************************
 * Написать программу, которая читает построчно текстовый файл и              *
 * переставляет случайно слова в каждой строке.                               *    
 * Замечание:                                                                 *
 * Программа открывает существующий тектстовый файл и читает его построчно.   *
 * Для каждой строки вызывается функция, разработанная в рамках задачи 1.     *
 ******************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#define SIZE 256
#define SIZE2 80
#define TRUE 1
#define FALSE 0

int get_words(char *line, char** pline) {
    int i;
    int countW = 0;
    int inword = FALSE;

    for (i = 0; line[i] != '\0'; i++) {
        if (!inword && !isspace(line[i])) {
            pline[countW++] = &line[i];
            inword = TRUE;
        }
        if (inword && isspace(line[i]))
            inword = FALSE;
    }
    return countW;
}

void randoms(int *index, int countW) {
    int i;
    int nrand = 0;
    int nrand2 = 0;
    int temp = 0;

    srand(time(NULL));
    for (i = 0; i < countW; i++)
        index[i] = i;

    for (i = 0; i < countW; i++) {
        do {
            nrand = rand() % countW;
            nrand2 = rand() % countW;
        } while (nrand == nrand2);
        temp = index[nrand];
        index[nrand] = index[nrand2];
        index[nrand2] = temp;
    }
}

void printWord(char **pline, int *index, int countW) {
    int i, j;

    for (i = 0; i < countW; i++) {
        j = index[i];
        while (*pline[j] && *pline[j] != ' ')
            printf("%c", *pline[j]++);
        putchar(' ');
    }
    printf("\n");
}

int main() {
    char *pline[SIZE] = {0};
    char line[SIZE] = {0};
    int index[SIZE2] = {0};
    int countW = 0;
    int len = 0;
    FILE *fp;

    fp = fopen("in.txt", "rt");
    if (!fp) {
        perror("File:");
        return 1;
    }
    while (!feof(fp)) {
        line[0] = '\0';
        fgets(line, SIZE, fp);
        len = strlen(line);
        line[len - 1] = '\0';
        countW = get_words(line, pline);
        randoms(index, countW);
        printWord(pline, index, countW);
    }
    return 0;
}