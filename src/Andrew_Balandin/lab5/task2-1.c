/*
написать программу ”Калейдоскоп”, выводящую на экран изобра-
жение, составленное из симметрично расположенных звездочек ’*’.
Изображение формируется в двумерном символьном массиве, в од-
ной его части и симметрично копируется в остальные его части.
Замечание:
Решение задачи протекает в виде следующей последовательности шагов:
1) Очистка массива (заполнение пробелами)
2) Формирование случайным образом верхнего левого квадранта (занесение
’*’)
3) Копирование символов в другие квадранты массива
4) Очистка экрана
5) Вывод массива на экран (построчно)
6) Временная задержка
7) Переход к шагу 1
*/
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 20
#define M 79
#define NSTARS 70
#define SPACE ' '
#define STAR '*'
#define TIMES_OF_REPEATS 7
#define DELAY 3
#define COMMAND "clear"  // "clear" for *nix, "cls" for win


void clearArr(char (*arr)[M], int n)
{
    int i,
        j;
    
    for(i = 0; i < n; i++)
        for(j = 0; j < M; j++)
            arr[i][j] = SPACE;
}

void fillLeftUpPartArr(char (*arr)[M], int n, int nstars)
{
    int i,
        j,
        k;
    
    clearArr(arr, n);
    
    k = 0;
    while(k < nstars)
    {
        i = rand() % (N / 2);
        j = rand() % (M / 2);
        
        if(arr[i][j] == SPACE)
        {
            arr[i][j] = STAR;
            k++;
        }
    }
}

void copyToOtherPartsArr(char (*arr)[M], int n)
{
    int i,
        j;
    
    for(i = 0; i < n / 2; i++)
        for(j = 0; j < M / 2; j++)
            if(arr[i][j] == STAR)
            {
                arr[N - 1 - i]  [j]         = STAR;
                arr[N - 1 - i]  [M - 1 - j] = STAR;
                arr[i]          [M - 1 - j] = STAR;
            }
}

void printArr(const char (*arr)[M], int n)
{
    int i, j;
    
    for(i = 0; i < n; i++)
    {
        for(j = 0; j < M; j++)
            putchar(arr[i][j]);
        putchar('\n');
    }
}

void sleep(int tsec) //sec
{
    int time = clock() + tsec * CLOCKS_PER_SEC;
    while(clock() <= time)
        ;
}

//void printNewKaleidoscope(char )



int main()
{
    int i;
    char arr[N][M];
    
    srand(time(NULL));
    
    for(i = 0; i < TIMES_OF_REPEATS; i++)
    {
        fillLeftUpPartArr(arr, N, NSTARS);
        copyToOtherPartsArr(arr, N);
        printArr(arr, N);
        sleep(DELAY);
        system(COMMAND);
    }
    
    return 0;
}