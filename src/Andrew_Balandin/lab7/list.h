#ifndef LIST_H
#define LIST_H

struct country 
{
    char country_name[3];
    char region_code[3];
    char region_name[256];
};
typedef struct country Item;

struct node
{
    Item item;
    struct node * next;
};

typedef struct node Node;
typedef Node * List;

enum Bool {false, true};
typedef enum Bool bool;

//allocate dynamic memory for List
Node * newNode(void);

//find last node in List
Node * findLastNodeInList(List list);

//add Item to List
void addItem(List * plist, Item item);

//apply pf-function for all List's nodes
void apply(List * plist, void (*pf)(Item item));

//free List's memory
void freeList(List list);

#endif /* LIST_H */

