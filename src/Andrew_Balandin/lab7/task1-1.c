/*
Написать программу, создающую связанный список с записями о
регионах и их кодах в соответствии с содержанием файла данных
Замечание:
Файл скачивается по адресу: http://introcs.cs.princeton.edu/java/data/fips10_4.csv
Программа должна поддерживать следующие функции:
(a) Формирование списка на основе данных файла.
(b) Поиск и вывод всех данных по буквенному обозначению страны.
(c) Поиск конкретного региона по названию.
*/
//list.c и list.h относятся к этому файлу
//запуск без параметров - вывод всего списка, -c страна, -r регион

#include "list.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define SEPARATOR ','

char search[256];

void convertStrToLower(char * str);
void printCountry(Item item);
void printRegion(Item item);
void printItem(Item item);
void readFileToList(FILE * fp, List * plist, bool existHeader);

int main(int argc, char * argv[])
{
    List list = NULL;
//    Item item;
    FILE * fp = fopen("fips10_4.csv", "rt");
    if(!fp)
    {
        printf("Error file\n");
        return -1;
    }

    readFileToList(fp, &list, true);

    if(argc > 1)
        if(!strcmp(argv[1], "-c"))
        {
            strcpy(search, argv[2]);
            apply(&list, printCountry);
        }
        else if(!strcmp(argv[1], "-r"))
        {
            strcpy(search, argv[2]);
            apply(&list, printRegion);
        }
        else
            printf("Error: -c Country_Code or -g Region\n");
    else
        apply(&list, printItem);
    
    freeList(list);
    return 0;
}
    
void convertStrToLower(char * str)
{
    if(isalpha(*str))
        *str = tolower(*str);
    else if(*str == '\0')
        return;
    convertStrToLower(str + 1);
}

void printCountry(Item item)
{
    char tmp[256];
    strcpy(tmp, item.country_name);
    convertStrToLower(tmp);
    convertStrToLower(search);
    if(strstr(tmp, search))
        printf("%s %s %s\n", item.country_name, item.region_code, item.region_name);
}

void printRegion(Item item)
{
    char tmp[256];
    strcpy(tmp, item.region_name);
    convertStrToLower(tmp);
    convertStrToLower(search);
    if(strstr(tmp, search))
        printf("%s %s %s\n", item.country_name, item.region_code, item.region_name);
}

void printItem(Item item)
{
    printf("%s %s %s\n", item.country_name, item.region_code, item.region_name);
}

void readFileToList(FILE * fp, List * plist, bool existHeader)
{
    int ch;
    Item item;
    int i = 0;
    
    if((ch = fgetc(fp)) == '\n' || ch == EOF)
        return;
    ungetc(ch, fp);
    
    if(existHeader)
        while(getc(fp) != '\n')
            ;

    
    while((ch = fgetc(fp)) != SEPARATOR)
        item.country_name[i++] = ch;
    item.country_name[i] = '\0';
    
    i = 0;
    while((ch = fgetc(fp)) != SEPARATOR)
        item.region_code[i++] = ch;
    item.region_code[i] = '\0';
    
    i = 0;
    while((ch = fgetc(fp)) != '\n' && ch != EOF)//???
        if(ch != '\"')                          //skip '\"'
            item.region_name[i++] = ch;
    item.region_name[i] = '\0';
    
    addItem(plist, item);
    
    readFileToList(fp, &((*plist)->next), false);
}