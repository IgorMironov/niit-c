#include "tree.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

NODE * newNode(void)
{
    NODE * pnode = NULL;
    pnode = (NODE *)malloc(sizeof(NODE));
    if(pnode == NULL)
    {
        printf("Error memory\n");
        exit(1);
    }
    pnode->left = NULL;
    pnode->right = NULL;
    return pnode;
}

TREE initTree(ITEM rootItem)
{
    TREE tree = newNode();
    tree->item = rootItem;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

int comp(ITEM item1, ITEM item2)
{
    return strcmp(item1.word, item2.word);
}

void addNode(TREE tree, ITEM item, int (*comp)(ITEM, ITEM))
{
    if(comp(tree->item, item) > 0)
        if(tree->left == NULL)
        {
            tree->left = newNode();
            tree->left->item = item;
            return;
        }
        else
        {
            addNode(tree->left, item, comp);
        }
    else if(comp(tree->item, item) < 0)
        if(tree->right == NULL)
        {
            tree->right = newNode();
            tree->right->item = item;
            return;
        }
        else
        {
            addNode(tree->right, item, comp);
        }
}

TREE makeTree(FILE * fp)
{
    TREE tree = NULL;
    ITEM item;
    char word[BUF];
    fgetWord(word, fp);
    strcpy(item.word, word);
    item.count = 0;
    tree = initTree(item);
    while(fgetWord(word, fp) != EOF)
    {
        strcpy(item.word, word);
        item.count = 0;
        addNode(tree, item, comp);
    }
    return tree;
}

NODE * searchTree(TREE tree, ITEM item, int (*comp)(ITEM, ITEM))
{
    if(comp(tree->item, item) == 0)
    {
        return tree;
    }
    else if(comp(tree->item, item) > 0)
    {
        if(tree->left != NULL)
            searchTree(tree->left, item, comp);
        else
            return NULL;
    }
    else if(comp(tree->item, item) < 0)
    {
        if(tree->right != NULL)
            searchTree(tree->right, item, comp);
        else
            return NULL;
    }
}

void printTree(TREE tree)
{
    if(tree != NULL)
    {
        printTree(tree->left);
        printf("%15s  %ld\n", tree->item.word, tree->item.count);
        printTree(tree->right);
    }
                
}

int fgetWord(char * word, FILE * fp)
{
    int i = 0;
    int ch;
    BOOL inword = false;
    while((ch = getc(fp)) != EOF)
    {
        if(isalpha(ch) || (ch == '_'))
        {
            inword = true;
            word[i] = ch;
            i++;
        }
        else if(inword == true)
        {
            word[i] = '\0';
            return ch;
        }
    }
    return -1;
}

void fillTreeFromFile(TREE tree, FILE * fp)
{
    ITEM item;
    NODE * node = NULL;
    while(fgetWord(item.word, fp) != EOF)
    {
        node = searchTree(tree, item, comp);
        if(node != NULL)
            node->item.count++;
    }
 }

void freeTree(TREE tree)
{
    if(tree != NULL)
    {
        freeTree(tree->left);
        free(tree);
        freeTree(tree->right);
    }
                
}