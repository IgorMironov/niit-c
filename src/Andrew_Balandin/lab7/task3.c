/*
Написать программу, которая строит таблицу встречаемости сим-
волов для произвольного файла, имя которого задаётся в команд-
ной строке. Программа должна выводить на экран таблицу встре-
чаемости, отсортированную по убыванию частоты
Замечание:
В программе необходимо определить структурный тип
SYM
, в котором нужно
хранить код символа и частоту встречаемости (вещественное число от 0 до 1).
После анализа файла, массив структур
SYM
должен быть отсортирован по
частоте.
*/

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define AMOUNT_OF_SYM 255//('z' - 'a' + 1)
#define TRUE 1
#define FALSE 0

typedef struct sym
{
    int code;
    double freq;
}SYM;

SYM * initSYM(void);
void analyzeFile(FILE *, SYM *);
void printTabOfSym(SYM *);

int main(int argc, char * argv[])
{
    SYM * psym = initSYM();
    FILE * fp = NULL;
    if(argc > 1)
    {
        fp = fopen(argv[1], "rt");
        if(fp == NULL)
        {
            perror("File");
            exit(1);
        }
    }
    else
    {
        printf("Something wrong!\n");
        return -1;
    }

    analyzeFile(fp, psym);
    int comp(const void * i1, const void * i2)
    {return ((SYM*)i1)->freq > ((SYM*)i2)->freq ? -1 : 1;}
    qsort(psym, AMOUNT_OF_SYM, sizeof(SYM), comp);
    printTabOfSym(psym);
    fclose(fp);
    free(psym);
    return 0;
}

SYM * initSYM(void)
{
    SYM * psym = (SYM *)malloc(AMOUNT_OF_SYM * sizeof(SYM));
    for(int i = 0; i < AMOUNT_OF_SYM; i++)
    {
        psym[i].code = i;
        psym[i].freq = 0.0;
    }
    return psym;
}

void analyzeFile(FILE * fp, SYM * psym)
{
    int ch;
    long long int symbolsInFile = 0;
    while((ch = getc(fp))!= EOF)
    {
        if(!isspace(ch))
        {
            for(int i = 0; i < AMOUNT_OF_SYM; i++)
                if(ch == psym[i].code)
                {
                    psym[i].freq += 1.0;
                    symbolsInFile++;
                    break;
                }
        }
    }
    for(int i = 0; i < AMOUNT_OF_SYM; i++)
        psym[i].freq = psym[i].freq / (double)symbolsInFile;
}

void printTabOfSym(SYM * psym)
{
    double sumOfFreq = 0;
    for(int i = 0; i < AMOUNT_OF_SYM; i++)
        if(psym[i].freq)
        {
            printf("'%c' %f\n",psym[i].code ,psym[i].freq);
            sumOfFreq += psym[i].freq;
        }
    printf("\n%f\n", sumOfFreq);
}