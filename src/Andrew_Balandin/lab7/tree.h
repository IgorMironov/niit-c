#ifndef TREE_H
#define TREE_H

#include <stdio.h>
#define BUF 50

typedef enum bool {false, true} BOOL;

typedef struct item
{
    char word[BUF];
    long int count;
}ITEM;

typedef struct node
{
    ITEM item;
    struct node * left;
    struct node * right;
}NODE;

typedef NODE * TREE;

NODE * newNode(void);
void chomp(char *);
TREE makeTree(FILE *);
NODE * searchTree(TREE, ITEM, int (*)(ITEM, ITEM));
void fillTreeFromFile(TREE, FILE *);
void printTree(TREE);
void freeTree(TREE);
int fgetWord(char *, FILE *);

#endif