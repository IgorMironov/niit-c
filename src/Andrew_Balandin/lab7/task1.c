/*
Написать программу, создающую связанный список с записями о
регионах и их кодах в соответствии с содержанием файла данных
Замечание:
Файл скачивается по адресу: http://introcs.cs.princeton.edu/java/data/fips10_4.csv
Программа должна поддерживать следующие функции:
(a) Формирование списка на основе данных файла.
(b) Поиск и вывод всех данных по буквенному обозначению страны.
(c) Поиск конкретного региона по названию.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SEPARATOR ','

struct countries 
{
    char country_name[5];
    char region_code[5];
    char region_name[256];
    struct countries * next;
};

struct countries * newRecOfList(void)
{
    struct countries * new_rec = NULL;
    new_rec = (struct countries*) malloc(sizeof (struct countries));
    if(new_rec == NULL)
        exit(1);
    new_rec->next = NULL;
    return new_rec;
}

void freeList(struct countries * list)
{
    if(list->next != NULL)
        freeList(list->next);
    else
    {
        free(list);
        return;
    }
    free(list);
}

void loadList(struct countries ** list, FILE * fp)//******************************************************************************
{
    int ch;
    int i = 0;
    if((ch = fgetc(fp)) == '\n' || ch == EOF)
        return;

    ungetc(ch, fp);
    *list = newRecOfList();
    //first->next = NULL;
    
    while((ch = fgetc(fp)) != SEPARATOR)
        (*list)->country_name[i++] = ch;
    (*list)->country_name[i] = '\0';
    
    i = 0;
    while((ch = fgetc(fp)) != SEPARATOR)
        (*list)->region_code[i++] = ch;
    (*list)->region_code[i] = '\0';
    
    i = 0;
    while((ch = fgetc(fp)) != '\n' && ch != EOF)//???
        if(ch != '\"')                          //skip '\"'
            (*list)->region_name[i++] = ch;
    (*list)->region_name[i] = '\0';
    
    loadList(&((*list)->next), fp);
        
}

void printList(struct countries * list)
{
    printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
    if(list->next == NULL)
        return;
    else
        printList(list->next);
}

void printCountry(struct countries * list, char * country)
{
    if(!strcmp(list->country_name, country))
        printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
    if(list->next == NULL)
        return;
    else
        printCountry(list->next, country);
}

void printRegion(struct countries * list, char * region)
{
    if(!strcmp(list->region_name, region))
        printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
    if(list->next == NULL)
        return;
    else
        printRegion(list->next, region);
}

int main(int argc, char * argv[])
{
    FILE * fp;
    struct countries * list;
    fp = fopen("fips10_4.csv", "rt");
    if(!fp)
    {
        printf("Error file\n");
        return -1;
    }
    //skip header
    while(getc(fp) != '\n')
        ;
    loadList(&list, fp);
    if(argc > 1)
        if(!strcmp(argv[1], "-c"))
            printCountry(list, argv[2]);
        else if(!strcmp(argv[1], "-r"))
            printRegion(list, argv[2]);
        else
            printf("Error: -c Country_Code or -g Region\n");
    else
        printList(list);
    
    freeList(list);
    return 0;
}