#include <stdio.h>
#include <stdlib.h>
#include "list.h"

Node * newNode(void)
{
    Node * retVal = NULL;
    retVal = (Node *) malloc(sizeof(Node));
    if(retVal == NULL)
        exit(1);
    retVal->next = NULL;
    return retVal;
}

Node * findLastNodeInList(List list)
{
    if(list->next == NULL)
        return list;
    else
        findLastNodeInList(list->next);
}

void addItem(List * plist, Item item)
{
    if(*plist == NULL)//list is empty
    {
        *plist = newNode();
        (*plist)->item = item;
    }
    else
    {
        *plist = findLastNodeInList(*plist);
        (*plist)->next = newNode();
        ((*plist)->next)->item = item;
    }
}

void apply(List * plist, void (*pf)(Item item))
{
    pf((*plist)->item);
    if((*plist)->next == NULL)
        return;
    apply(&((*plist)->next), pf);
}

void freeList(List list)
{
    if(list->next == NULL)
    {
        free(list);
        return;
    }
    freeList(list->next);
    free(list);
}