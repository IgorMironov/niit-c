/*
 написать программу, которая переводит значение угла из граду-
сов в радианы, и, наоборот, в зависимости от символа при вводе.
Например:
45.00D - означает значение в градусах, а
45.00R - в радианах. Ввод данных осуществляется по шаблону %f%c
 */


#include <stdio.h>
#define PI 3.141592653589

int main()
{
    float   angle = 0;

    char    angletype = 0,
            bufclear = 0, 
            restart = 0;
	
    while(1)
    {
        printf("Input angle in format nn.nnX where X - R for rad, X - D for grad and nn.nn - number => ");

        if((scanf("%f%c", &angle, &angletype) != 2) || ((angletype != 'D') && (angletype != 'R')))
        {
            
            printf("Wrong input\n");
            if(angletype != '\n')
                while(bufclear != '\n')
                    scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        //check symbols after input
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }

        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;

        if(angletype == 'D')
            printf("%.3f grad = %.3f rad\n", angle, angle * PI / 180.0);
        else
            printf("%.3f rad = %.3f grad\n", angle, angle * 180.0 / PI);

        while(1)
        {
            printf("Restart? (y/n): ");

            if((scanf("%c", &restart) != 1) || ((restart != 'y') && (restart != 'n')))
            {
                printf("Wrong input\n");
                if(restart != '\n')
                    while(bufclear != '\n')
                        scanf("%c", &bufclear);//clear buffer
                bufclear = 0;
                continue;
            }

        //check symbols after char
        scanf("%c", &bufclear);
        if(bufclear != '\n')
        {
            printf("Wrong input\n");
            while(bufclear != '\n')
                scanf("%c", &bufclear);//clear buffer
            bufclear = 0;
            continue;
        }
        
        while(bufclear != '\n')
            scanf("%c", &bufclear);//clear buffer
        bufclear = 0;


        if(restart == 'y')
            break;
        else
            return 0;
        }
    }
}