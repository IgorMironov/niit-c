/*
Написать программу, которая выводит на экран 10 паролей, сгенерированных
случайным образом из латинских букв и цифр, причём буквы должны быть
как в нижнем, так и в верхнем регистрах. Длина пароля - 8 символов.
Замечание:
Пример сгенерированного пароля: Nh1ku83k
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>

#define N_DIGITS_LETTERS ('z' - 'a' + 1 + 'Z' - 'A' + 1 + '9' - '0' + 1)
#define TRUE 1
#define FALSE 0

void fillArr(char arr[], int n)
{
    int i, m;
    
    char alph[N_DIGITS_LETTERS] = {0};
    
    for(i = 0; i <= 'z' - 'a'; i++)
        alph[i] = 'a' + i;
    m = i;
    for(i = 0; i <= 'Z' - 'A'; i++)
        alph[i + m] = 'A' + i;
    m += i;
    for(i = 0; i <= '9' - '0'; i++)
        alph[i + m] = '0' + i;

    for(i = 0; i < n; i++)
        arr[i] = alph[rand() % N_DIGITS_LETTERS];

}

void genPass(char pass[], int n)
{
    int digexist,
        loexist,
        upexist,
        i;
    
    while(1)
    {   
        digexist = loexist = upexist = FALSE;
        fillArr(pass, n);
        for(i = 0; i < n; i++)
            if(isdigit(pass[i]))
                digexist = TRUE;
            else if(isupper(pass[i]))
                upexist = TRUE;
            else
                loexist = TRUE;
        if(digexist & upexist & loexist)
            break;
    }
}

int main()
{
    char pass[8 + 1] = {0};
    int i;
    srand(time(NULL));
    
    for(i = 1; i <= 10; i++)
    {
        genPass(pass, 8);
        printf("%s\n", pass);
    }
    
    return 0;
}