

#include <stdio.h>

int main()
{
    unsigned    n = 1, 
                i, 
                j;
    
    char        stars[256] = {0},
                format[20] = {0};
    
    printf("Input number of rows: ");
    scanf("%u", &n);
    
    for(i = 0; i < 2 * n - 1; i++)
        stars[i] = '*';
    stars[i] = '\0';
    
    for(i = 0; i < n; i++)
    {
        sprintf(format, "%%%u.%us*\n", n + i, 2 * i);
        printf(format, stars); 
    }
    
    return 0;
}