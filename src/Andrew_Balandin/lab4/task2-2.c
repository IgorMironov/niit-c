/*
Написать программу, которая с помощью массива указателей выводит слова
строки в обратном порядке
Замечание:
Вместе со строкой создается массив указателей на char, в который заносятся
адреса первых символов каждого слова (альтернатива - адреса первого и по-
следнего символов). Затем мы организуем вывод новой строки, используя этот
массив из указателей.
*/

#include <stdio.h>
#include <strings.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    int i = 0,
        j = 0,
        inword = FALSE;
        
    char str[N] = {0};
    
    char* pwords[N][2];
    
    printf("Input your string :");
    fgets(str, N, stdin);
    
    for(i = 0; str[i] != '\0'; i++)
    {
        if(!isspace(str[i]) && !inword)
        {
            inword = TRUE;
            pwords[j][0] = &str[i];
            
        }
        if(isspace(str[i]) && inword)
        {
            inword = FALSE;
            pwords[j][1] = &str[i];
            j++;
        }
    }
    j--;
    while(j >= 0)
    {
        for(i = 0; (pwords[j][0] + i) < pwords[j][1]; i++)
            putchar(*(pwords[j][0] + i));
        putchar(' ');
        j--;
    }
    putchar('\n');
    return 0;
}