/*
Написать программу, которая находит выход из лабиринта
*/

#include <stdio.h>

#define WALL '#'
#define MAN '*'
#define FREE ' '
#define PATH '.'
#define BLOCK '@'
#define N 9
#define M 27
#define TRUE 1
#define FALSE 0

char lab[N][M] = 
{
"##########################",
"#                        #",
"########### ##### ### ####",
"#           # ####### ####",
"# ######### #            #",
"#      #### # ####### ####",
"#####  #### # #         ##",
"# *    #### # ## #########",
"################ #########"
};

int k[] = {-1, 1, 0, 0};//how we go
int l[] = {0, 0, -1, 1};

void printDebug()
{
    int i, j;
    for(i = 0; i < N; i++)
    {
        for(j = 0; j < M; j++)
            putchar(lab[i][j]);
        putchar('\n');
    }
    putchar('\n');
}
void print()
{
    int i, j;
    for(i = 0; i < N; i++)
    {
        for(j = 0; j < M; j++)
            if(lab[i][j] == BLOCK)
                putchar(FREE);
            else
                putchar(lab[i][j]);
        putchar('\n');
    }
    putchar('\n');
}

void findMan(int * n, int * m) // find coordinates of MAN '*'
{
    if(lab[*n][*m] == MAN)
        return;
    else if(lab[*n][*m] == '\0')
    {
        (*n)++;
        *m = 0;
    }
    else
        (*m)++;
    findMan(n, m);
}

int findPath(int n, int m)
{
    if(n >= N - 1 || m >= M - 2 || n <= 0 || m <= 0)
    {
        lab[n][m] = PATH;//final dot
        return TRUE;
    }

    lab[n][m] = PATH;

    for(int i = 0; i < 4; i++)
        if(lab[n + k[i]][m + l[i]] == FREE)
            if(findPath(n + k[i], m + l[i]))
                return TRUE;

    lab[n][m] = BLOCK;
    return FALSE;
}

int main()
{
    int n = 0,
        m = 0;
    
    print();
    findMan(&n, &m);
    
    printf("n = %d\n"
           "m = %d\n\n", n, m);
    
    findPath(n,m);
    print();
    printf("Done!\n\n");
    return 0;
}