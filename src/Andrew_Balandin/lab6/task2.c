/*
Написать программу, которая находит в диапазоне целых чисел от
2 до 1000000 число, формирующее самую длинную последователь-
ность Коллатца
*/

#include <stdio.h>

long long int Collatz(long long int n,long long int * i)
{
    (*i)++;
    if(n == 1)
        return n;
    if(n % 2)
        return Collatz(3 * n + 1, i);
    else 
        return Collatz(n / 2, i);
}

int main()
{
    long long int posl = 0, i, max = 0, n = 2;

    for(i = 2; i <= 1000000; i++)
    {
        posl = 0;
        Collatz(i, &posl);
        if(posl > max)
        {
            max = posl;
            n = i;
        }
    }
    printf("max = %ld \n", max);
    printf("n = %ld \n", n);
    
    return 0;
}