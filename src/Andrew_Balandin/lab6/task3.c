/*
Написать программу, которая переводит введённое пользователем
целое число в строку с использованием рекурсии и без каких-либо
библиотечных функций преобразования
*/



#include <stdio.h>

char * itoa(int i, char * a)
{
    if(i > 0)
    {
        a = itoa(i / 10, a);
        *a++ = i % 10 + '0';
        *a = '\0';
        return a;
    }
    else if(i < 0)
    {
        *a++ = '-';
        itoa(-i, a);
    }
    else
        return a;
}

int main()
{
    int i = -1235780;
    char str[100];
    //printf("Input any number: ");
    //scanf("%d", &i);
    itoa(i, str);
    printf(str);
    putchar('\n');
    return 0;
}