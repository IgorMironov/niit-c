/*
Написать реализацию рекурсивной функции, вычисляющую n-ый
элемент ряда Фибоначчи, но без экспоненциально растущей рекур-
сии
*/

#include <stdio.h>
#include <time.h>
#define N 35

long long fib3(long long f1, long long f2, long long n)
{
    if(n == 0)
        return f1;
    else
        return fib3(f2, f1 + f2, n - 1);
}

long long fib1(long long n)
{
    return fib3(0, 1 , n);
}

int fib2(int n)
{
    int i, fn = 0, f1 = 0, f2 = 1;
    for(i = 2; i <= n; i++)
    {
        
        fn = f1 + f2;
        f1 = f2;
        f2 = fn;
    }
    return fn;
}

int main()
{
    FILE * fp;
    long long i, f;
    long t;

    fp = fopen("fib6.txt", "wt");
    if(fp == NULL)
    {
        perror("FILE: ");
        return 1;
    }
    for(i = 2; i <= N; i++)
    {
        
        t = clock();
        f = fib1(i);
        t = clock() - t;
        fprintf(fp, "%lld; %lld; %ld\n", i, f, t);
    }

    //printf("%d \n", fib1(9));
    fclose(fp);
    printf("Done!\n");
    return 0;
}