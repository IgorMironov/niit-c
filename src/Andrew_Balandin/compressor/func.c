#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "comp.h"

char signature[4] = "CMP";

void initSYMS(SYM syms[], int N)
{
    for(int i = 0; i < N; i++)
    {
        syms[i].ch = i;
        syms[i].code[0] = '\0';
        syms[i].freq = 0.0;
        syms[i].left = NULL;
        syms[i].right = NULL;
    }
}

SYM* buildTree(SYM *psyms[], int N)
{
    SYM *temp = (SYM*)malloc(sizeof(SYM));
    if(N < 2)//file has only 1 symbol
    {
        temp->left = psyms[N - 1];
        temp->right = NULL;
        temp->freq = psyms[N - 1]->freq;
        temp->code[0] = 0;
        return temp;
    }
    temp->freq = psyms[N - 2]->freq + psyms[N - 1]->freq;
    //temp->ch = '*';//debug************************
    temp->left = psyms[N - 1];
    temp->right = psyms[N - 2];
    temp->code[0] = 0;
    if(N == 2) 
        return temp;
    //insert temp in psyms ascend
    for(int i = N - 2; i >= 0; i--)
    {
        psyms[i + 1] = psyms[i];
        if(psyms[i]->freq > temp->freq)
        {
            psyms[i + 1] = temp;
            break;
        }
        else if(i == 0)
        {
            psyms[i] = temp;
            break;
        }
    }
    
    return buildTree(psyms, N - 1);
}

long long int analyzeFile(FILE * fp, SYM syms[])
{
    int ch;
    long long int bytesInFile = 0;
    
    while((ch = getc(fp))!= EOF)
    {
        for(int i = 0; i < AMOUNT_OF_SYM; i++)
            if(ch == syms[i].ch)
            {
                syms[i].freq += 1.0;
                bytesInFile++;
                break;
            }
    }
    
    for(int i = 0; i < AMOUNT_OF_SYM; i++)
        syms[i].freq = syms[i].freq / (double)bytesInFile;
    
    int compare(const void * i1, const void * i2)
    {
        return ((SYM*)i1)->freq >  ((SYM*)i2)->freq ? -1 : 
               ((SYM*)i1)->freq == ((SYM*)i2)->freq ? 0 : 1;
    }
    qsort(syms, AMOUNT_OF_SYM, sizeof(SYM), compare);
    
    return bytesInFile;
}

void makeCodes(SYM *root)
{
    if(root->left)
    {
        strcpy(root->left->code,root->code);
        strcat(root->left->code,"0");
        makeCodes(root->left);
    }
    if(root->right)
    {
        strcpy(root->right->code,root->code);
        strcat(root->right->code,"1");
        makeCodes(root->right);
    }
}

unsigned char write101FileFromIn(FILE* fp_101, FILE* fp_in, SYM syms[], int N)
{
    int ch;
    long long int size_file101 = 0;
    rewind(fp_in);
    while((ch = fgetc(fp_in)) !=  EOF)
    {
        size_file101++;
        for(int i = 0; i < N; i++)
            if(syms[i].ch == (unsigned char)ch) 
            {
                fputs(syms[i].code, fp_101);
                break;
            }
    }
    return size_file101 % SIZE_BYTE;
}

void write101FileFromCmp(FILE* fp_101, FILE* fp_cmp, SYM syms[], int N)
{
    unsigned char buf[SIZE_BYTE + 1] = {0};
    int ch;
    while((ch = fgetc(fp_cmp)) !=  EOF)
    {
        fputs(unpack((unsigned char)ch, buf), fp_101);
    }
    //tail****************************************************
}

int cpSymsToPsyms(SYM syms[], SYM* psyms[], int N)
{
    int i;
    for(i = 0; i < AMOUNT_OF_SYM; i++)
    {
        if(syms[i].freq != 0.0)
            psyms[i] = &syms[i];
        else
            break;
    }
    return i;
}

char* addFileExt(const char source[], char dest[], const char extension[])
{
    strcpy(dest, source);
    strcat(dest, extension);
    //printf("%s\n", dest);
    return dest;
}

char* delFileExt(const char source[], char dest[], const char extension[])
{
    int len_source = strlen(source);
    if(strcmp(&source[len_source - 4], extension) == 0)
        strncpy(dest, source, len_source - 4);
    else
        addFileExt(source, dest, ".decomp");
    return dest;
}

unsigned char pack(unsigned char buf[])
{
    union CODE code;
    code.byte.b1 = buf[0] - '0';
    code.byte.b2 = buf[1] - '0';
    code.byte.b3 = buf[2] - '0';
    code.byte.b4 = buf[3] - '0';
    code.byte.b5 = buf[4] - '0';
    code.byte.b6 = buf[5] - '0';
    code.byte.b7 = buf[6] - '0';
    code.byte.b8 = buf[7] - '0';
    return code.ch;
}

unsigned char* unpack(unsigned char ch, unsigned char buf[])
{
    union CODE code;
    code.ch = ch;
    buf[0] = code.byte.b1 + '0';
    buf[1] = code.byte.b2 + '0';
    buf[2] = code.byte.b3 + '0';
    buf[3] = code.byte.b4 + '0';
    buf[4] = code.byte.b5 + '0';
    buf[5] = code.byte.b6 + '0';
    buf[6] = code.byte.b7 + '0';
    buf[7] = code.byte.b8 + '0';
    return buf;
}

void writeHeaderCompFile(FILE* fp_comp, int nsyms, SYM syms[], unsigned char tail, long long int size_infile)
{
    rewind(fp_comp);
    fputc(signature[0], fp_comp); fputc(signature[1], fp_comp); fputc(signature[2], fp_comp); //signature
    fwrite((void*) &nsyms, sizeof(int), 1, fp_comp); //write number of symbols in infile
    for(int i = 0; i < nsyms; i++)//write table of frequency
    {
        fwrite((void*) &syms[i].ch, sizeof(unsigned char), 1, fp_comp);
        fwrite((void*) &syms[i].freq, sizeof(float), 1, fp_comp);
    }
    fwrite(&tail, sizeof(unsigned char), 1, fp_comp);
    fwrite(&size_infile, sizeof(long long int), 1, fp_comp);
}

unsigned char writeDataCompFile(FILE* fp_101, FILE* fp_comp)
{
    unsigned char buf[SIZE_BYTE] = {0};
    unsigned char i = 0;
    int ch;
    rewind(fp_101);
    //rewind(fp_comp);
    while ((ch = fgetc(fp_101)) != EOF)
    {
        if(i < SIZE_BYTE)
        {
            buf[i] = (unsigned char)ch;
            i++;
        }
        else
        {
            fputc(pack(buf), fp_comp);
            i = 0;
            buf[i] = (unsigned char)ch;
            i++;
        }
    }
    if(i != 0)//exist tail
    {
        for(int j = i; j < SIZE_BYTE; j++)
            buf[j] = '0';
        fputc(pack(buf), fp_comp);
    }
    return i;
}

FILE* openFile(char name_file[], const char mode[])
{
    FILE* fp = fopen(name_file, mode);
    if(fp == NULL)
    {
        perror("Wrong file");
        exit(EXIT_FAILURE);
    }
    return fp;
}

BOOL readHeaderCompFile(FILE* fp_comp, int* nsyms, SYM syms[], unsigned char* tail, long long int* size_outfile)
{
    //rewind(fp_comp);
    char buf[4] = {0};
    buf[0] = fgetc(fp_comp);//signature
    buf[1] = fgetc(fp_comp);//signature
    buf[2] = fgetc(fp_comp);//signature
    if(strcmp(buf, signature) != 0)
        return false;
    
    fread((void*) nsyms, sizeof(int), 1, fp_comp); //read number of symbols in infile
    for(int i = 0; i < *nsyms; i++)//read table of frequency
    {
        fread((void*) &syms[i].ch, sizeof(unsigned char), 1, fp_comp);
        fread((void*) &syms[i].freq, sizeof(float), 1, fp_comp);
    }
    fread(tail, sizeof(unsigned char), 1, fp_comp);
    fread(size_outfile, sizeof(long long int), 1, fp_comp);
    return true;
}

void writeOutFile(FILE* fp_out, FILE* fp_101, SYM* tree, long long int size_outfile)
{
    
    int ch;
    unsigned char buf[SIZE_BYTE] = {0};
    unsigned char deCode(SYM* root)
    {
        if(ch == '0')
        {
            if(root->left->left == NULL && root->left->right == NULL)
                return root->left->ch;
            ch = fgetc(fp_101);
            deCode(root->left);
        }
        else
        {
            if(root->right->left == NULL && root->right->right == NULL)
                return root->right->ch;
            ch = fgetc(fp_101);
            deCode(root->right);
        }
    }
    rewind(fp_101);
    //fgetc(fp_101);
    while(size_outfile != 0 && (ch = fgetc(fp_101)) !=  EOF)
    {
        size_outfile--;
        fputc(deCode(tree), fp_out);
    }
}

void printTree(SYM* tree)
{
    if(tree)
    {
        printTree(tree->left);
        if(tree->left == NULL && tree->right == NULL)
        //if(tree->left == NULL && tree->right == NULL && tree->freq != 0)
            printf("%c - %15s - %f\n", tree->ch, tree->code, tree->freq);
        printTree(tree->right);
    }
}