#include <stdio.h>

#ifndef COMP_H
#define COMP_H

#define AMOUNT_OF_SYM 256
#define SIZE_BYTE 8

//char signature[4] = "CMP"; //????????????????????????

enum Bool {false, true};
typedef enum Bool BOOL;

typedef struct sym
{
    unsigned char ch; 
    float freq; 
    char code[256];
    struct sym* left;
    struct sym* right;
}SYM;

union CODE 
{
    unsigned char ch;
    struct 
    {
        unsigned short b1:1;
        unsigned short b2:1;
        unsigned short b3:1;
        unsigned short b4:1;
        unsigned short b5:1;
        unsigned short b6:1;
        unsigned short b7:1;
        unsigned short b8:1;
    } byte;
};

FILE* openFile(char name_file[], const char mode[]);
char* addFileExt(const char source[], char dest[], const char extension[]);
char* delFileExt(const char source[], char dest[], const char extension[]);
int cpSymsToPsyms(SYM syms[], SYM* psyms[], int N);
void printTree(SYM* tree);
void initSYMS(SYM syms[], int N);
SYM* buildTree(SYM* psym[], int N);
long long int analyzeFile(FILE* fp, SYM syms[]);
void makeCodes(SYM* root);
unsigned char write101FileFromIn(FILE* fp_101, FILE* fp_in, SYM syms[], int N); 
void write101FileFromCmp(FILE* fp_101, FILE* fp_cmp, SYM syms[], int N); 
void writeHeaderCompFile(FILE* fp_comp, int nsyms, SYM syms[], unsigned char tail, long long int size_infile);
unsigned char writeDataCompFile(FILE* fp_101, FILE* fp_comp);
unsigned char pack(unsigned char buf[]);
void writeOutFile(FILE* fp_out, FILE* fp_cmp, SYM* tree, long long int size_outfile);
unsigned char* unpack(unsigned char ch, unsigned char buf[]);
BOOL readHeaderCompFile(FILE* fp_comp, int* nsyms, SYM syms[], unsigned char* tail, long long int* size_outfile);


void compress(char file_name_in[]);
void decompress(char file_name_cmp[]);

#endif /* COMP_H */

