#include <stdio.h>
#include <stdlib.h>
#include "comp.h"

void compress(char file_name_in[])
{
    int nsyms;
    unsigned char tail;
    float sum = 0;
    long long int size_infile = 0;
    char file_mame_101[256] = {0};
    char file_mame_cmp[256] = {0};
    FILE* fp_in = NULL;
    FILE* fp_101 = NULL;
    FILE* fp_cmp = NULL;
    SYM syms[AMOUNT_OF_SYM];
    SYM* psyms[AMOUNT_OF_SYM];
    SYM* tree = NULL;
    
    initSYMS(syms, AMOUNT_OF_SYM);

    fp_in  = openFile(file_name_in, "rb");
    fp_101 = openFile(addFileExt(file_name_in, file_mame_101, ".101"), "w+b");
    fp_cmp = openFile(addFileExt(file_name_in, file_mame_cmp, ".cmp"), "wb");

    size_infile = analyzeFile(fp_in, syms);
    nsyms = cpSymsToPsyms(syms, psyms, AMOUNT_OF_SYM);
    tree = buildTree(psyms, nsyms);
    makeCodes(tree);
    tail = write101FileFromIn(fp_101, fp_in, syms, nsyms);
    
/*
    printf("bytes = %lld\n", size_infile);//debug*********************
    printTree(tree); //debug***********************
*/
    
    writeHeaderCompFile(fp_cmp, nsyms, syms, tail, size_infile);
    writeDataCompFile(fp_101, fp_cmp);
    
    
    fclose(fp_in);
    fclose(fp_101);
    remove(file_mame_101);
    fclose(fp_cmp);
    
    printf("File \"%s\" pack to \"%s\"!\n", file_name_in, file_mame_cmp);
}

void decompress(char file_name_cmp[])
{
    int nsyms;
    unsigned char tail;
    float sum = 0;
    long long int size_outfile = 0;
    char file_mame_101[256] = {0};
    char file_mame_out[256] = {0};
    FILE* fp_out = NULL;
    FILE* fp_101 = NULL;
    FILE* fp_cmp = NULL;
    SYM syms[AMOUNT_OF_SYM];
    SYM* psyms[AMOUNT_OF_SYM];
    SYM* tree = NULL;
    
    initSYMS(syms, AMOUNT_OF_SYM);
    
    fp_cmp  = openFile(file_name_cmp, "rb");
    if(!readHeaderCompFile(fp_cmp, &nsyms, syms, &tail, &size_outfile))
    {
        printf("File %s is wrong!\n", file_name_cmp);
        return;
    }
    
    fp_101 = openFile(addFileExt(file_name_cmp, file_mame_101, ".101"), "w+b");
    fp_out = openFile(delFileExt(file_name_cmp, file_mame_out, ".cmp"), "wb");
    
    cpSymsToPsyms(syms, psyms, AMOUNT_OF_SYM);
    tree = buildTree(psyms, nsyms);
    makeCodes(tree);
    write101FileFromCmp(fp_101, fp_cmp, syms, nsyms);
    writeOutFile(fp_out, fp_101, tree, size_outfile);
    
/*
    //debug***********************
    printTree(tree); 
    for(int i = 0; i < AMOUNT_OF_SYM; i++)
        printf("%c - %f\n", syms[i].ch, syms[i].freq);
    printf("%d - %d - %lld\n", nsyms, tail, size_outfile);
    //debug***********************
    
*/
  
    
    fclose(fp_cmp);
    fclose(fp_101);
    remove(file_mame_101);
    fclose(fp_out);
    
    printf("File \"%s\" unpack to \"%s\"!\n", file_name_cmp, file_mame_out);
}