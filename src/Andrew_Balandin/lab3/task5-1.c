/*
Написать программу, которая формирует целочисленный массив размера N, а
затем находит сумму элементов, расположенным между первым отрицатель-
ным и последним положительным элементами.
Замечание:
Массив заполняется случайными числами: отрицательными и положительны-
ми поровну (или почти поровну...)
*/


#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define N 10

int main()
{
    int i = 0,
        j = 0,
        k = 0,
        nneg = 0,
        npos = 0,
        sum = 0;
    int arr[N] = {0};
    
    srand(time(NULL));
    for(i = 0; i < N / 2;)
    {
        k = rand() % N;
        if(!arr[k])
            arr[k] = rand() % 10 + 1, i++;
    }
    for (i = 0; i < N; i++)
        if(!arr[i])
            arr[i] = -rand() % 10 - 1;
    
    for (i = 0; i < N; i++)
        printf("%d ", arr[i]);
    printf("\n");
    
    for(i = 0; i < N; i++)
    {
        if(arr[i] < 0)
        {
            nneg = i; 
            break;
        }
    }
    for(j = N - 1; j > 0; j--)
    {
        if(arr[j] > 0)
        {
            npos = j; 
            break;
        }
    }
    for(i = nneg; i <= npos; i++)
        sum += arr[i];
    
    printf("Sum = %d\n", sum);
    
    return 0;
}