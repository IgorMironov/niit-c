//������� �4 - ������� ����� �� ������������ � ����������� �������.

#include <stdio.h>
#include <conio.h>

int main() {

	unsigned int feet, inches, c;
	float cm;

	//���� ����� �������� � ���������

	while (1) {
		printf("What is your height?\n");
		if ((scanf_s("%u\'%u\"", &feet, &inches) != 2) || (feet > 7 || inches > 12)) {
			printf("Input error!\n");
			do {
				c = getchar();
			} while (c != '\n' && c != EOF); 
			continue;
		}
		else
			break;
	}

	cm = ((feet * 12) + inches)*2.54f;
	printf("Your height: %.1f cm.\n", cm);

	return 0;
 }