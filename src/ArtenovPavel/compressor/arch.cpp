#include "stdio.h"
#include "string.h"
#include "stdlib.h"


int countAllCh=0;
int countCh101=0;

union CODE
{
	unsigned char ch;
	struct
	{
		unsigned short b1:1;
		unsigned short b2:1;
		unsigned short b3:1;
		unsigned short b4:1;
		unsigned short b5:1;
		unsigned short b6:1;
		unsigned short b7:1;
		unsigned short b8:1;
	} byte;
};

struct SYM
{
	unsigned char ch;
	float freq;
	char code[256];
	struct SYM * left;
	struct SYM * right;
};



unsigned char pack (unsigned char buf[])
{
	union CODE code;
	code.byte.b1 = buf[0] - '0';
	code.byte.b2 = buf[1] - '0';
	code.byte.b3 = buf[2] - '0';
	code.byte.b4 = buf[3] - '0';
	code.byte.b5 = buf[4] - '0';
	code.byte.b6 = buf[5] - '0';
	code.byte.b7 = buf[6] - '0';
	code.byte.b8 = buf[7] - '0';
	return code.ch;
}

void createResultFile(FILE * fpw_101,FILE * fpw_result,int tail)
{
	int i;
	char symbol;
	rewind(fpw_101);
	unsigned char buf[9];
	int buf_size;
	while((buf_size = fread(buf, sizeof(char), 8, fpw_result)) > 0)
	{
		if(buf_size < 8)
			for(i = buf_size; i < 8; i++)
				buf[i] = '0';
		buf[8] = '\0';
		symbol = pack(buf);
		fputc(symbol, fpw_result);
		if(buf_size < 8)
			break;
	}	
}



void createHeader(FILE * fpw_result,int tail, int countCh,struct SYM * rate)
{
	int i=0;
	fwrite("Itsagoodtime",sizeof(char),12,fpw_result);               //�������
	fwrite(&countCh,sizeof(int),1,fpw_result);                        //���-�� ���������� ��������
	while(i<=countCh){
		fwrite(&rate[i].ch,sizeof(char),1,fpw_result);                 //������ � �������
		fwrite(&rate[i].freq,sizeof(float),1,fpw_result);
		i++;
	}
	fwrite(&tail,sizeof(int),1,fpw_result);                            //"�����"
}


int searchTail(FILE * fpw_101)                                        
{
	int tail;
	while(fgetc(fpw_101) != EOF)
		countCh101++;
	return tail = countCh101 % 8;
}

int writeTo101(FILE * fpw_101,struct SYM rate[],int countCh)           //�������� ����� � �������������� �������
{
	char ch;
	FILE * fpr1;
	fpr1=fopen("arch.txt","rb");
	if(fpr1==NULL){
		perror("File:");
		return 1;
	}
	while((ch=fgetc(fpr1)) != EOF){
		for(int i=0;i<countCh;i++)
			if(rate[i].ch==(unsigned char)ch){
				fputs(rate[i].code,fpw_101);
				break;
			}
	}
	fclose(fpr1);
	return 0;
}

void makeCodes(struct SYM *root)
{
	if(root->left){
		strcpy(root->left->code,root->code);
		strcat(root->left->code,"0");
		makeCodes(root->left);
	}
	if(root->right){
		strcpy(root->right->code,root->code);
		strcat(root->right->code,"1");
		makeCodes(root->right);
	}
}

struct SYM* buildTree(struct SYM *prate[], int N)                         //���������  ������ �� ����������
{
	int j;
	//������� ��������� ����
	 struct SYM *temp=(struct SYM*)malloc(sizeof(struct SYM));
	//� ���� ������� ������������ ����� ������
	//���������� � �������������� ��������� ������� psym
	temp->freq=prate[N-2]->freq+prate[N-1]->freq;
	//��������� ��������� ���� � ����� ���������� ������
	temp->left=prate[N-1];
	temp->right=prate[N-2];
	temp->code[0]=0;
	if(N==2)   //�� ������������ �������� ������� � �������� 1.0
		return temp;
	else {
		
		for(int i=0;i<N;i++){
			if(temp->freq > prate[i]->freq){
				for(j=N-1;j>i;j--){
					prate[j]=prate[j-1];
				}
				prate[i]=temp;
				break;
			}
		}
	}
	
	return buildTree(prate,N-1);
		
}



void pointerToSym(struct SYM rate[],struct SYM * prate[],int countCh)       //�������� ������� ���������� �� ������ ��������
{
	for(int i=0;i<countCh;i++)
		prate[i] = &rate[i];

}
void countToFreq(struct SYM rate[],int countCh)                   //������� � �� ���-�� �������� � %
{
	for(int i=0;i<countCh;i++)
		rate[i].freq=rate[i].freq/countAllCh;
}

void bubbleSort(struct SYM rate[],int count)                      //����������
{
	int i,j,lastIndex,temp_symbol,temp_count_symbol;
	i = count-1;
	while(i > 0){
		lastIndex=0;
		for(j=0;j<i;j++)
			if(rate[j+1].freq > rate[j].freq){
				temp_symbol=rate[j].ch;temp_count_symbol=rate[j].freq;
				rate[j].ch=rate[j+1].ch;rate[j].freq=rate[j+1].freq;
				rate[j+1].ch=temp_symbol;rate[j+1].freq=temp_count_symbol;
				lastIndex=j;
			}
			i=lastIndex;
	}
}

int createTable(struct SYM rate[],FILE * fpr) //�������� ������� �������������
{
	char ch;
	int i,count=0;
	while((ch=fgetc(fpr)) != EOF){
		countAllCh++;
		i=0;
		while(i<256){
			if(rate[i].ch == ch){
				rate[i].freq++;
				break;
			}
			if(rate[i].ch == 0){
				rate[i].ch=ch;
				rate[i].freq++;
				count++;
				break;
			}
			i++;
		}
	}

	return count;
}

int main()
{
	int i,countCh,tail;
	struct SYM * root;
	struct SYM rate[256];            //������ ��������

	
	for(i=0;i<256;i++){
		rate[i].ch=0;
		rate[i].freq=0;
		rate[i].code[0]='\0';
		rate[i].left=0;
		rate[i].right=0;            //������� ������� ��������
	}

	struct SYM * prate[256];            //������ ���������� �� ���������
	
	FILE * fpw_101;
	fpw_101=fopen("arch_101.txt","r+b");
	if(fpw_101==NULL){
		perror("File:");
		return 1;
	}

	FILE * fpw_result;
	fpw_result=fopen("result.txt","wb");
	if(fpw_result==NULL){
		perror("File:");
		return 1;
	}

	FILE * fpr;
	fpr=fopen("arch.txt","rb");
	if(fpr==NULL){
		perror("File:");
		return 1;
	}

	countCh=createTable(rate,fpr);
	fclose(fpr);
	bubbleSort(rate,countCh);
	countToFreq(rate,countCh);
	pointerToSym(rate,prate,countCh);
	root=buildTree(prate,countCh);
	makeCodes(root);
	writeTo101(fpw_101,rate,countAllCh);
	tail=searchTail(fpw_101);
	createHeader(fpw_result,tail,countCh,rate);
	createResultFile(fpw_101,fpw_result,tail);
	
	for(i=0;i<countCh;i++){
		printf("%c  %f\n",rate[i].ch,rate[i].freq);
	}
	puts("\n\n\n\n");
	for(i=0;i<countCh;i++)
	printf("%s\n",rate[i].code);

	fclose(fpr);
	fclose(fpw_101);
	fclose(fpw_result);
	return 0;
}