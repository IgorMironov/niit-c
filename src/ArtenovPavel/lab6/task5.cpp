/*��������� �������� ����� ���������� N-��� ����� ���� ���������.*/
#include "stdio.h"
#include "time.h"
#include "windows.h"

#define N 40

int fib(int i)
{
	if(i<=2)
		return 1;
	else
		return fib(i-1) + fib(i-2);
}

int main()
{
	int i;
	FILE * fp;
	long int result;
	int start,finish;
	
	fp=fopen("task5.txt","w");
	if(fp==NULL)
		perror("File:");
	for(i=1;i<=N;i++){
		start=clock();
		result=fib(i);
		finish=clock();
		printf("N=%d fib=%d time=%dms\n",i,result,finish-start);
		fprintf(fp,"N=%d fib=%d time=%d\n",i,result,finish-start);
	}
	fclose(fp);
}