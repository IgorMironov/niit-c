/*��������� ������� ����� �� ���������"*/
#include "stdio.h"

#define N 10
#define M 17

void printArr (char (*arr)[M]);
void findStart (char (*arr)[M], int * i,int * j);
void findExit(char (*arr)[M],int i,int j);
int flag = 0;

void printArr (char (*arr)[M])
{
	int n,m;
	for(n=0;n<N;n++){
		for(m=0;m<M;m++){
			putchar(arr[n][m]);
		}
		putchar('\n');
	}
}

void findStart (char (*arr)[M], int * i,int * j)
{
	int n,m;
	for(n=0;n<N;n++)
		for(m=0;m<M;m++){
			if(arr[n][m]=='x'){
				(*i)=n;
				(*j)=m;
			}
		}
}

void findExit(char (*arr)[M],int i,int j)
{
	//arr[i][j]='!';
	
	if(arr[i][j]=='E'){
		flag=1;
		arr[i][j]='X';
	}
	else
		arr[i][j]='!';
	if((arr[i-1][j]==' ' || arr[i-1][j]=='E') && flag==0)
		findExit(arr,i-1,j);                              //up
	if((arr[i][j+1]==' ' || arr[i-1][j]=='E') && flag==0)
		findExit(arr,i,j+1);                              //right
	if((arr[i+1][j]==' ' || arr[i+1][j]=='E') && flag==0)
		findExit(arr,i+1,j);                              //down
	if((arr[i][j-1]==' ' || arr[i][j-1]=='E')  && flag==0)
		findExit(arr,i,j-1);                            //left
}

int main()
{
	int i,j;
	char arr[N][M]=
	{
		"################",
		"#     #    #   #",
		"#  #  #    #   #",
		"#  #  #        #",
		"#  #  #######  #",
		"#  #   x       #",
		"#  #############",
		"#              #",
		"#    #   #     #",
		"#######E########",
	};

	findStart(arr,&i,&j);
	findExit(arr,i,j);
	printf("%d %d\n",i,j);
	printArr(arr);
	return 0;


}

