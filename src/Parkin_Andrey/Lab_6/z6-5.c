/*5. �������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� �������� ��� N
� ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)�� �����
� � ����.
���������:��������� ���� �� ���������� ����� ������� � �����������
������� � ��������� ������ ����������� ������� �� ����� ���� N.*/
#include<stdio.h>
#include <time.h>
#define N 46

unsigned long int GetTime()
{
	return time(NULL);
}

int FibTr(int num)
{
	int i, f1 = 1, f2 = 1, fib;
	if(num == 1 || num == 2)
		return f1; 
	for(i=3; i<=num; i++)
	{
		fib = f1 + f2;
		f1 = f2;
		f2 = fib;
	}
	return fib;
}

int FibRec(int num)
{
	if(num <= 2)
		return 1;
	return (FibRec(num - 1) + FibRec(num - 2));
}
void Print(FILE *fp, int f_num, unsigned long long int f_fib, int sec)
{
	fprintf(fp, "#%d - %lld - %d sec.\n", f_num, f_fib, sec);
	printf("For %d 'Fibonacci number' is: %lld (%d sec)\n", f_num, f_fib, sec);
}

int main()
{
	unsigned long int start, stop;
	unsigned long long int fib;
	unsigned int num;
	FILE* fp;

	fp = fopen("Fibonacci.txt", "w");

	if(fp == NULL)
	{
		printf("File error!!!");
		return 1;
	}

	for(num = 1; num<=N; num++)
	{
		start = GetTime();
		fib = FibRec(num);
		stop = GetTime();
		Print(fp, num, fib, stop - start);
	}
	fclose(fp);
	return 0;
}