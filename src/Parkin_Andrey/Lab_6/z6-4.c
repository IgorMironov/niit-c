/*4. �������� ���������, ������� ��������� ������ ������������ �
����������� ���������.
���������: ��������� ��������� ��������� ������������������ ��������:
(a) ��������� �� ��������� ������ �������� ������� ������ M;
(b) ������� ������ ������������� ������� N = 2M;
(c) �������� ������ ��� ������������ ������;
(d) ��������� ������� ��������� ������ �������;
(e) ������� ����� ������������ � ������������ ��������;
(f) ���������� ����� ���������� ������������ ������������ �
	����������� ��������;
(g) ����������� ������������ ������.*/
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<time.h>

void RandArr(int* p_arr, int cnt)
{
	int i;
	for(i=0; i<cnt; i++)
		p_arr[i] = rand()%20;
}

void PrintArr(int* p_arr, int cnt)
{
	int i;
	for(i=0; i<cnt; i++)
		printf("%d ", p_arr[i]);
	printf("\n");
}

long int GetTime()
{
	return time(NULL);
}

int SumTr(int* p_arr, int cnt)
{
	int i, sum = 0;
	for(i=0; i<cnt; i++)
		sum+=p_arr[i];
	return sum;
}

int SumRec(int* p_arr, int cnt)
{
	if(cnt == 1)
		return p_arr[0];
	return p_arr[cnt-1] + SumRec(p_arr, cnt-1);
}
int main()
{
	int* p_arr = NULL;
	long int t_rec_1, t_rec_2, t_tr_1, t_tr_2;
	int num, arr_cnt, sum_rec, sum_tr;
	srand(time(NULL));
	printf("Please, enter number from 0 to 12: \n");
	scanf("%d", &num);
	arr_cnt = (int)pow(2, (float)num);
	p_arr = calloc (arr_cnt, sizeof(int));
	if(!p_arr)
	{
		printf("Memory error!!!\n");
		return 1;
	}
	RandArr(p_arr, arr_cnt);

	t_tr_1 = GetTime();
	sum_tr = SumTr(p_arr, arr_cnt);
	t_tr_2 = GetTime();

	t_rec_1 = GetTime();
	sum_rec = SumRec(p_arr, arr_cnt);
	t_rec_2 = GetTime();
	
	printf("sum_tr = %d(%d), sum_rec = %d(%d)\n", sum_tr, (t_tr_2 - t_tr_1), sum_rec, (t_rec_2 - t_rec_1));
	free(p_arr);
	return 0;
}
