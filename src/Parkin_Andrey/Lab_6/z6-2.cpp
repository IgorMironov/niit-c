/*2. �������� ���������, ������� ������� � ��������� ����� ����� ��
2 �� 1000000 �����, ����������� ����� ������� ������������������ ��������
*/
#include<stdio.h>
#define MIN_N 2
#define MAX_N 100000

int GetColzCnt(int N)
{
	int next_n;
	next_n = (N%2 == 0) ? N/2 : (3*N + 1);
	if(N == 1)
		return 1;
	return GetColzCnt(next_n) + 1;
}


int main()
{
	int n, cnt = 0, max_cnt = -1, max_num = 0;
	for(n = MIN_N; n <= MAX_N; n++)
	{
		cnt = GetColzCnt(n);
		if(cnt > max_cnt)
		{
			max_cnt = cnt;
			max_num = n;
		}
	}
	printf("max = %d (%d elements)\n", max_num, max_cnt);
	return 0;
}
