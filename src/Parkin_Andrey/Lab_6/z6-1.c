/*1. �������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� �������.*/
#include <math.h>
#include <stdio.h>
#define N 100
#define ITERATIONS 4

void CleanArr(char (*arr)[N])
{
	int i, j;
	for(i=0; i<N; i++)
		for(j=0; j<N; j++)
			arr[i][j] = ' ';
}
int Draw(char (*p_arr)[N], int it, int x, int y)
{
	int radius;
	if(it == 0)
	{
		p_arr[x][y] = '*';
		return 1;
	}
	radius = pow(3,it-1);
	// ������ ����� ��������;
	Draw(p_arr, it-1, x, y);
	// ���������� ���� ��������;
	Draw(p_arr, it-1, x-radius, y);
	Draw(p_arr, it-1, x, y-radius);
	Draw(p_arr, it-1, x+radius, y);
	Draw(p_arr, it-1, x, y+radius);
	return radius;
}

int main()
{
	int i, j;
	char arr[N][N];
	CleanArr(arr);
	Draw(arr, ITERATIONS, N/2, N/2);
	for(i=0; i<N; i++)
	{
		for(j=0; j<N; j++)
		{
			printf("%c", arr[i][j]);
		}
		printf("\n");
	}
	return 0;
}
