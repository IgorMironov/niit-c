/*3. �������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������: ��������� ��������� ������������ ���������� ���� � ������
��� ���������. ��� ������ ������ ����������� �������� �� ����� � 
����������� ��������� ������� �����.*/
#include<stdio.h>
#include<time.h>

#define N 256

int GetWords(char *p_str, char *(*p_words)[2])
{
	int cnt=0, i=0;
	while(*p_str)
	{
		if(*p_str == ' ' || *p_str == '\t' || *p_str == '\n' || *p_str == '\0')
		{
			p_str++;
		}
		else
		{
			cnt++;
			p_words[i][0] = p_str;
			while(*p_str != ' ' && *p_str != '\t' && *p_str != '\n' && *p_str != '\0')
				p_words[i][1] = p_str++;
			i++;
		}
	}
	return cnt;
}

void MixWord(char *p_start, char *p_end)
{
	int i, len, rnd;
	char dlt;
	len = p_end - p_start + 1;
	for(i=1; i<len-1; i++)
	{
		rnd = i + rand()%(len-1-i);
		dlt = *(p_start + i);
		*(p_start + i) = *(p_start + rnd);
		*(p_start + rnd) = dlt;
	}
}

int main()
{
	char str[N];
	char *p_words[100][2];
	int i, cnt_words = 0;
	FILE *f_in, *f_out;
	srand(time(NULL));

	f_in = fopen("input.txt", "r");
	f_out = fopen("output.txt", "w");
	if(f_in == NULL || f_out == NULL)
	{
		perror("File:");
		return 1;
	}
	while(!feof(f_in))
	{
		fgets(str, N, f_in);
		cnt_words = GetWords(str, p_words);
		for(i=0; i<cnt_words; i++)
			MixWord(p_words[i][0], p_words[i][1]);
		fputs(str, f_out);
	}
	fclose(f_in);
	fclose(f_out);
	return 0;
}