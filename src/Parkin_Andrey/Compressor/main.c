#include "common.h"

void Compress(char* file_name)
{
	int ascii[ASCII_CNT] = {0};//������������� ��������� �������� �� ASCII-�����
	int unique_sym_cnt = 0;//���������� �� ������������� �������� � ��������� �����
	int tree_sym_cnt = 0;//
	int all_sym_cnt = 0; //���������� ���� �������� � ����� - ��� �� ������ �����
	struct SYM** sym_arr;
	int i, sym_i = 0, tail = 0;
	char cps_file_name[256];
	char base_file_ext[10];

	GenerateSymFreqFromFILE(file_name, ascii, &unique_sym_cnt, &all_sym_cnt);
	tree_sym_cnt = unique_sym_cnt;
	if(unique_sym_cnt > 0)
	{
		sym_arr = malloc(unique_sym_cnt*sizeof(struct SYM*)*2);
		for(i = 0; i < ASCII_CNT; i++)
		{
			if(ascii[i] != 0)
				sym_arr[sym_i++] = AddNewSym(i, (float)ascii[i]/(float)all_sym_cnt);
		}
		SortSymArr(sym_arr, unique_sym_cnt);
		//������ ������������� ���� ������
		BuildTree(sym_arr, unique_sym_cnt, &tree_sym_cnt);
		MakeCodes(sym_arr[0]);
		for(i = 0; i < tree_sym_cnt; i++)
			PrintSYM(sym_arr[i], "sym", i);

		//�������� � ���������� ��������������� �����
		tail = GenerateCompressTmpBinaryFile(file_name, sym_arr, tree_sym_cnt);
		GenerateCompressedFileName(file_name, base_file_ext, cps_file_name);
		FillCompressedHeader(cps_file_name, unique_sym_cnt, sym_arr, tree_sym_cnt, tail, all_sym_cnt, base_file_ext);
		FillCompressedDate(cps_file_name);

		// ������� ������
		for(i = 0; i < tree_sym_cnt; i++)
			free(sym_arr[i]);
		free(sym_arr);
		remove(TMP_BINARY_FILE);
	}
}

void DeCompress(char* cps_file_name)
{
	int unique_sym_cnt = 0;//���������� �� ������������� �������� � ��������� �����
	int tree_sym_cnt = 0;//
	int all_sym_cnt = 0; //���������� ���� �������� � ����� - ��� �� ������ �����
	struct SYM* sym_arr[ASCII_CNT*2];
	int i, tail = 0, header_len;
	char base_file_name[256] = {NULL};
	char base_file_ext[10];

	header_len = ReadHeaderFromFile(cps_file_name, sym_arr, &unique_sym_cnt, &all_sym_cnt, &tail, base_file_ext);
	tree_sym_cnt = unique_sym_cnt;
	if(header_len <= 0)
		return -1;
	SortSymArr(sym_arr, unique_sym_cnt);

	//������ ������������� ���� ������
	BuildTree(sym_arr, unique_sym_cnt, &tree_sym_cnt);
	MakeCodes(sym_arr[0]);
	for(i = 0; i < tree_sym_cnt; i++)
		PrintSYM(sym_arr[i], "sym", i);

	//�������� � ���������� �������� �����
	GenerateDeCompressTmpBinaryFile(cps_file_name, header_len, tail);
	GenerateBaseFileName(cps_file_name, base_file_ext, base_file_name);
	GenerateBaseFile(base_file_name, sym_arr, tree_sym_cnt);

	// ������� ������
	for(i = 0; i < tree_sym_cnt; i++)
		free(sym_arr[i]);
	remove(TMP_BINARY_FILE);
}

int main(int argc, char* argv[])
{
	if(argc != 3)
	{
		printf("Invalid params number(1 - compress/decompress, 2 - file name)!\n");
		return 1;
	}
	if(strcmp(argv[1], "compress") == 0)
		Compress(argv[2]);
	else if(strcmp(argv[1], "decompress") == 0)
		DeCompress(argv[2]);
	else
	{
		printf("Unsupported type!!!\n");
		return 2;
	}
	return 0;
}