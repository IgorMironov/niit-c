/*1. �������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������.*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct CODE {
	int code;
	char country[3];
	char region[100];
    struct CODE* next;
};

void PrintNode(struct CODE* node) {
	printf("Country: %s, Code: %d, Region: %s\n", node->country, node->code, node->region);
}

void PrintList(struct CODE* node) {
	while(node) 
	{
		PrintNode(node);
		node = node->next;
	}
}

void SerchByCountry(char* country, struct CODE* node)
{
	int cnt = 0;
	while(node) 
	{
		if(strcmp(node->country, country) == 0)
		{
			PrintNode(node);
			cnt++;
		}
		node = node->next;
	}
	if(cnt == 0)
		printf("Can not find data for country %s\n", country);
}

void SerchByRegion(char* region, struct CODE* node)
{
	int cnt = 0;
	while(node) 
	{
		if(strcmp(node->region, region) == 0)
		{
			PrintNode(node);	
			cnt++;
		}
		node = node->next;
	}
	if(cnt == 0)
		printf("Can not find data for region %s\n", region);
}

int ParseFile(char* filename, struct CODE** head) 
{
	FILE* fp;
	int str_num = 0;
	char str[256];
	char str_parts[3][100];
	struct CODE* tmp;
	char* pstr;

	fp = fopen(filename, "r");
	if(fp == NULL)
	{
		printf("File open error\n");
		return -1;
	}
	while(!feof(fp) && fgets(str, 256, fp) != NULL ) 
	{
		str_num++;
		// ���������� ���������
		if(str_num == 1 || strlen(str) == 0) 
			continue;
		// �������� ������ ��� ���������
		tmp = (struct CODE*) malloc(sizeof(struct CODE));

		pstr = strtok(str, ",");
		strcpy(tmp->country, pstr);
		tmp->country[strlen(pstr)]='\0';

		pstr = strtok(NULL, ",");
		tmp->code = atoi(pstr);

		pstr = strtok(NULL, ",");
		strcpy(tmp->region, pstr+1); // �������� ������� � ������� ������
		tmp->region[strlen(pstr)-3]='\0';

		tmp->next = *head;
		*head = tmp;
	}	
	fclose(fp);
	return str_num-1;
}

int main() 
{
	struct CODE* head = NULL;
	int list_size;
	char user_choise;
	char user_str[100];
	
	list_size = ParseFile("fips10_4.csv", &head);
	PrintList(head);

	if(list_size <= 0) 
	{
		printf("Empty list\n");
		return -1;
	}
	while(1) {
		printf("Enter 1 for search by country, 2 for search by region, other for exit\n");
		user_choise = fgetc(stdin);
		fgetc(stdin); // C�������� ������� ������
		if(user_choise == '1') 
		{
			printf("Please enter 2 symbols country code (for example RU, US)\n");
			fgets(user_str, 3, stdin);
			SerchByCountry(user_str, head);
		} else if(user_choise == '2') 
		{
			printf("Please enter region name (for example Novgorod)\n");
			fgets(user_str, 100, stdin);
			user_str[strlen(user_str)-1]='\0'; // ������� ������� ������
			SerchByRegion(user_str, head);
		}
		break;
	}
	
	return 0;
}