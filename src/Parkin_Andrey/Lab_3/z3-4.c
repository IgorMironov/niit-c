/*4. �������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����. */
#include<stdio.h>
#include<string.h>
#include < math.h > 
#define N 256
#define MAX_NUM_LEN 6

int main()
{
	int sum = 0, i = 0, j = 0, k = 0, num = 0;
	char ent_arr[N], num_str[MAX_NUM_LEN+1];
	printf("Please enter numbers as string (max length string = %d)\n", N);
	fgets(ent_arr, N, stdin);
	while(ent_arr[i] != '\n' && ent_arr[i] != '\0')
	{
		if(ent_arr[i] <= '9' && ent_arr[i] >= '0')
		{
			j=0;
			while(j < MAX_NUM_LEN && ent_arr[i] <= '9' && ent_arr[i] >= '0')
			{
				num_str[j] = ent_arr[i];
				j++;
				i++;
			}
			num_str[j]='\0';
			num=0;
			for(k = 0; k<=j-1; k++)
				num+=((int)num_str[k]-'0') * (int)pow(10,j-1-k);
			sum+=num;
		}
		else 
			i++;
	}
	printf("Summa: %d\n", sum);
	return 0;
}
