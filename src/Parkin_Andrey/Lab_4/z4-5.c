/*5. �������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
1. �������� ���������, ������� ��������� ������������ ������ �������-
�� ����� � ����������, � ����� ��������� �� � ������� ��������-
��� ����� ������.
���������:
������ �������� �� ��������� ������ ������ � ������������ � ���������
���������� ������. ������������ ���������� �������� � ���������� �������
���������� �� char. ����� ��������� ����� ��������� ��������� �������� �
������� � ������� ������ � ������������ � ���������������� �����������.*/
#include<stdio.h>
#include<string.h>
#define ROW_NUM 100
#define ROW_LEN 256

int main()
{
	FILE *fp_in, *fp_out;
	char ent_arr[ROW_NUM][ROW_LEN];
	char *p_arr[ROW_NUM], *p_tmp;
	int i, j, rows_num = 0, row_len=0;
	
	fp_in = fopen("strings_in.txt", "r");
	fp_out = fopen("strings_out.txt", "w");
	if(fp_in==NULL || fp_out==NULL)
	{
		perror("File:");
		return 1;
	}
	while(!feof(fp_in))
	{
		fgets(ent_arr[rows_num], ROW_LEN, fp_in);
		row_len = strlen(ent_arr[rows_num]);
		if(ent_arr[rows_num][row_len-1] != '\n')
		{
			row_len++;
			ent_arr[rows_num][row_len-1] = '\n';
			ent_arr[rows_num][row_len] = '\0';
		}
		p_arr[rows_num] = ent_arr[rows_num];
		if(rows_num >= ROW_NUM-1)
			break;
		rows_num++;
	}
	printf("Old array\n");
	for(i=0;i<rows_num;i++)
		printf("%d - %s", i, p_arr[i]);
	printf("\n");
	for(i=0; i<rows_num;i++)
	{
		for(j=1; j<rows_num-i;j++)
		{
			if(strlen(p_arr[j])> strlen(p_arr[j-1]))
			{
				p_tmp = p_arr[j];
				p_arr[j] = p_arr[j-1];
				p_arr[j-1] = p_tmp;
			}
		}
	}
	printf("New array\n");
	for(i=0;i<rows_num;i++)
	{
		printf("%d - %s", i, p_arr[i]);
		fputs(p_arr[i], fp_out);
	}
	printf("\n");
	fclose(fp_in);
	fclose(fp_out);
	return 0;
}

