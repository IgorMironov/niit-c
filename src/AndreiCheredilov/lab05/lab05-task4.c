/*
�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ���������� �������, ������������� � ������ ������
1.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#include <time.h>

#define SIZE 256

FILE* inputdata;

char arr[SIZE];
char arrtwo[SIZE];
char* twodimarr[SIZE] = { 0 };
char* ptr;

int inputData()
{
    if (fgets(arr, SIZE, inputdata) != 0)
    {
        ptr = arr;
        return 1;
    }
    else
        return 0;
}

void PrintWord(char** words, int RWord)
{
    char* p;
    {
        p = words[RWord];
        while ((*p != ' ') && (*p != '\0'))
            putchar(*p++);
        putchar(' ');
    }
}

int GetWords(char* str, char** words)
{
    int j = 0;
    int flag = 0;
    int i = 0;

    for (i = 0; str[i] != '\0'; i++)
    {
        if (str[i] != ' ' && flag == 0)
        {
            flag = 1;
            words[j++] = str + i;
        }
        else if (str[i] == ' ' && flag == 1)
            flag = 0;
    }
    return j;
}


int main()
{
    int c = 0;
    int numRW = 0;
    inputdata = fopen("strings.txt", "wt");
    if (inputdata == 0)
    {
        perror("An error type 1 occurred!");
        return 1;
    }
    while (inputdata)
    {
        arr[strlen(arr) - 1] = 0;
        c = GetWords(arr, twodimarr);
        while (c > 0)
        {
            numRW = rand() % c;
            PrintWord(twodimarr, numRW);
            twodimarr[numRW] = twodimarr[c - 1];
            c--;
        }
        putchar('\n');
    }

    printf("\n");
    fclose(inputdata);
    return 0;
}
