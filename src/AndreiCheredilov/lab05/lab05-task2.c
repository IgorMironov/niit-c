/*
�������� ��������� ������������, ��������� �� ����� ������-
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��-
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (���������
�*�)
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <ctype.h>
#include <windows.h>
#define N 32
#define M 32
#define SPACE ' '
#define STAR '*'
#define Numberofstars 12

void clear2dimarr(char (*twodimmarr)[N])
{
    for (int i = 0; i < N; i++)
    {
        for (int j = 0; j < M; j++)
            twodimmarr[i][j] = SPACE;
    }
}

void randArray(char (*twodimmarr)[M], int nstars)
{
    clear2dimarr(twodimmarr);
    for (int c = 0; c < nstars; c++)
    {
        int i = rand() % (M / 2);
        int j = rand() % (N / 2);

        if (twodimmarr[i][j] == SPACE)
        {
            twodimmarr[i][j] = STAR;
        }
    }
}

void cpyStars(char (*twodimmarr)[M])
{
    for (int i = 0; i < N / 2; i++)
        for (int j = 0; j < M / 2; j++)
            if (twodimmarr[i][j] == STAR)
            {
                twodimmarr[N - 1 - i][j] = STAR;
                twodimmarr[N - 1 - i][M - 1 - j] = STAR;
                twodimmarr[i][M - 1 - j] = STAR;
            }
}

void print(char (*twodimmarr)[M])
{
    int i,j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < M; j++)
            putchar(twodimmarr[i][j]);
        puts("$");
    }
}


int main(void)
{
    char Kaleidoscope[N][M];
    while (1)
    {
        srand(time(NULL));
        clear2dimarr(Kaleidoscope);
        randArray(Kaleidoscope, Numberofstars);
        cpyStars(Kaleidoscope);
        print(Kaleidoscope);
        Sleep(1000);
    }
    return 0;
}
