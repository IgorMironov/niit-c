/*
�������� ���������, ������� ��������� �� ������������ ������ �
������� �� �� �����, ��������� ����� � ��������� �������.
���������:
��������� ������ �������� ������� �� ���� �������:
a) printWord - ������� ����� �� ������ (�� ����� ������ ��� �������)
b) getWords - ��������� ������ ���������� �������� ������ ���� ����
c) main - �������� �������
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#include <time.h>

#define SIZE 256

void inputstr(char* str)
{
    printf("Please enter random string here:\n");
    fgets(str, SIZE, stdin);
    str[strlen(str) - 1] = 0;
}

void PrintWord(char** words, int RWord)
{
    char* p;
    {
        p = words[RWord];
        while ((*p != ' ') && (*p != '\0'))
            putchar(*p++);
        putchar(' ');
    }
}

int GetWords(char* str, char** words)
{
    int j = 0;
    int flag = 0;
    int i = 0;

    for (i = 0; str[i] != '\0'; i++)
    {
        if (str[i] != ' ' && flag == 0)
        {
            flag = 1;
            words[j++] = str + i;
        }
        else if (str[i] == ' ' && flag == 1)
            flag = 0;
    }
    return j;
}

int main()
{
    char str[SIZE] = { 0 };
    char* words[SIZE];
    int j = 0, RWord;
    srand(time(NULL));
    inputstr(str);
    j = GetWords(str, words);
    printf("Randomized string is:\n");
    while (j > 0)
    {
        RWord = rand() % j;
        PrintWord(words, RWord);
        words[RWord] = words[j - 1];
        j--;
    }
    printf("\n");
    return 0;
}
