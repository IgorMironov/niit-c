/*
 �������� ���������, �������������� ��������� ������� �����-
�� ������� ����� ������ ������ ���������� �����, ����� �������
� ����������, �� ���� ������ � ����� ����� �������� �� ������.
���������:
��������� ��������� ������������ ���������� ���� � ������ ��� �������-
��. ��� ������ ������ ����������� �������� �� ����� � ����������� ����-
����� ������� �����.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <windows.h>
#include <time.h>

#define SIZE 256

FILE* inputdata;
FILE* outputdata;

char arr[SIZE];
char* ptr;
char* b;
int car = 0;
int i;

int inputData()
{
    if (fgets(arr, SIZE, inputdata) != 0)
    {
        ptr = arr;
        return 1;
    }
    else
        return 0;
}
void randchar()
{
    int i, rnd;
    char temp;
    car -= 2;
    for (i = 0; i < car; i++)
    {
        rnd = rand() % car;
        temp = *(b + i);
        *(b + i) = *(b + rnd);
        *(b + rnd) = temp;
    }
}
void findword()
{

    while (*ptr != '\n')
    {
        if ((ptr == arr && *ptr != ' ') || *(ptr - 1) == ' ')
        {
            b = ptr + 1;
            car = 0;
        }
        if (*(ptr + 1) == ' ' || *(ptr + 1) == '\n' || *(ptr + 1) == '\t')
            randchar();
        ptr++;
        car++;
    }
}

int main()
{
    srand(time(NULL));

    inputdata = fopen("strings.txt", "rt");
    if (inputdata == 0)
    {
        perror("An error type 1 occurred!");
        return 1;
    }

    outputdata = fopen("results.txt", "wt");
    if (outputdata == 0)
    {
        perror("An error type 2 occurred!");
        return 2;
    }

    while (inputData() == 1)
    {
        findword();
        fputs(arr, outputdata);
    }

    fclose(inputdata);
    fclose(outputdata);

    return 0;
}
