/*Написать программу, которая переводит значение угла из граду-
сов в радианы, и, наоборот, в зависимости от символа при вводе.
Например: 45.00D - означает значение в градусах, а 45.00R - в
радианах. Ввод данных осуществляется по шаблону %f%c*/

#include <stdio.h>
#define pi 3.14
int main()
{
    float angle;
    char unit;

    printf("Please enter value of the angle in format 45(D or R):\n");
    scanf("%f  %c", &angle, &unit);
    printf("Value of angle you entered is %.2f\%c\n", angle, unit);

    if (unit == 'D')
    {
        unit = 'R';
        printf("%.2f%c\n", (angle * pi) / (180), unit);
    }
    else if (unit == 'R')
    {
        unit = 'D';
        printf("%.2f%c\n", (angle * 180) / (pi), unit);
    }
    else
    {
        printf("Wrong data type!\n");
    }

    return 0;
}
