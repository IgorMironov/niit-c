/*Написать программу, которая переводит рост из американской си-
стемы (футы, дюймы) в европейскую (сантиметры). Данные вво-
дятся в виде двух целых чисел, выводятся в виде вещественного
числа с точностью до 1 знака. 1 фут = 12 дюймов. 1 дюйм = 2.54
см.*/

#include <stdio.h>

int main()
{
    int height_ft, height_inc;
    float metric;

    const double x = 30.48; // 1 foot =30.38cm
    const double y = 2.54; // 1 inch =2.54cm

    printf("Please enter your height in the form of two integers ft and inc:\n");
    scanf("%d%d", &height_ft, &height_inc);
    printf("Your height is imperial sysem is %3d\ft%3d\inc\n", height_ft, height_inc);
    metric = height_ft * x + height_inc * y;
    printf("Your height in metric system is: %.2f\n", metric);

    return 0;
}
