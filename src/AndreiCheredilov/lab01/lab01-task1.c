/*�������� ���������, ������� ����������� � ������������ ���,
���� � ���, � ����� ����������� ����������� ����� � ����, ����-
��� ������������ � ���������� ��������� (��������, ����������,
�����*/

#include <stdio.h>
int main()
{
    char gender;
    float height, weight, IW, a;

    while (1)
    {
        printf("Please enter your gender (M or F):\n");
        scanf("%c", &gender);
        if (gender == 'M' || gender == 'm' || gender == 'F' || gender == 'f')
        {
            break;
        }
        else
        {
            printf("Wrong data type!\n");
            return 1;
        }
    }

    printf("Please enter your height in cm:\n");
    if (scanf("%f", &height) != 1)
    {
        printf("Wrong data type!\n");
        return 1;
    }

    printf("Please enter your weight in kg:\n");
    if (scanf("%f", &weight) != 1)
    {
        printf("Wrong data type!\n");
        return 1;
    }

    if (gender == 'M' || gender == 'm')
    {
        a = 100;
    }
    else if (gender == 'F' || gender == 'f')
    {
        a = 110;
    }
    IW = height - a;
    if (weight > IW)
        printf("You should lose weight!\n");
    else if (weight < IW)
        printf("You should gain weight!\n");
    else if (weight == IW)
        printf("You are in shape!!\n");
    return 0;
}