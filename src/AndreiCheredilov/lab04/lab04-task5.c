/*
�������� ���������, ����������� ������ (��. ������ 1), �� ������������
������, ����������� �� ���������� �����. ��������� ������ ��������� �����
������������ � ����.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ROWS 5
#define MAXLEN 20

int main()
{
    char str[ROWS][MAXLEN] = {0};

    char* pointer[ROWS];
    char* temp;

    int i = 0, j = 0, ch;

    FILE* inputdata;
    FILE* outputdata;

    inputdata = fopen("strings.txt", "rt");
    if (inputdata == 0)
    {
        perror("An error type 1 occurred!");
        return 1;
    }

    outputdata = fopen("results.txt", "wt");
    if (outputdata == 0)
    {
        perror("An error type 2 occurred!");
        return 2;
    }

    for(i = 0; i < ROWS; pointer[i] = str[i], i++)
    ;
    i = 0;
    j = 0;

    while (i < ROWS && j < MAXLEN)
    {
        ch = fgetc(inputdata);

        if (j == MAXLEN - 2)
        {
            str[i][j] = '\n';
            while ((ch = fgetc(inputdata)) != '\n' && ch != EOF);
            j = 0;
            i++;
        }

        else if (ch == EOF)
        {
            str[i][j] = '\n';
            break;
        }

        else if (ch == '\n')
        {
            str[i][j] = '\n';
            j = 0;
            i++;
        }
        else
        {
            str[i][j] = ch;
            j++;
        }
    }

    for (i = 0; i < ROWS; ++i)
    {
        for (j = i; j < ROWS; ++j)
            if (strlen(pointer[i]) > strlen(pointer[j]))
            {
                temp = pointer[i];
                pointer[i] = pointer[j];
                pointer[j] = temp;
            }
    }


    printf("File is created\n");
    for (i = 0; i < ROWS; i++)
        fputs(pointer[i], outputdata);


    fclose(inputdata);
    fclose(outputdata);

    return 0;
}
