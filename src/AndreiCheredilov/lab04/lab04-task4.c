/*
�������� ��������� ��� ������ ����� ������� ������������������� � ���-
���� � �������������� ���������� ������ �������� ����������.
���������:
���������� ����� �������� ������ ���� �������� � �������������� ��������
���, ����� ������ � ������ ������������� ����� ���������.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define N 256

int main(void)
{
    char str[N];
    char ch = 0;
    char maximumch = 0;
    int i = 0;
    int charnum = 0;
    int countmax = 0;

    printf("Please enter the string here:\n");
    fgets(str, N, stdin);

    char* p = str;
    ch = *p;

    for (i = 0; *(p + i) != '\n'; i++)
    {
        if (*(p + i) == ch)
        {
            charnum++;
        }
        if (*(p + i) != ch)
        {
            ch = *(p + i);
            charnum = 1;
        }
        if (charnum > countmax)
        {
            countmax = charnum;
            maximumch = *(p + i);
        }
    }
    printf("%d - ", countmax);

    for (i = 0; i < countmax; i++)
        putchar(maximumch);

    printf("\n");
    return 0;
}

