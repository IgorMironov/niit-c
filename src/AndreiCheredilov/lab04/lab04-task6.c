/*
�������� ���������, ������� ����������� ���������� ������������� � �����,
� ����� ��������� ������ ��� ������������ � ��� �������. ��������� ������
���������� ������ �������� � ������ ������� ������������
���������:
����� ������� ������ ����� ��� �������� ��� � ��� ���������: young � old,
������� �� ���� �����, ��������� � ������� ��������.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define ROW 5
#define MAXLEN 20

int main()
{
    int num, j = 0, age = 0, maxage = 0, minage = 100;
    char arr[ROW][MAXLEN] = { 0 };
    char* young = 0;
    char* old = 0;
    char name;


    printf("Please enter the number of relatives in your family:\n");
    scanf("%d", &num);

    while (num)
    {
        printf("Please enter name of your relative and his or here age:\n");
        scanf("%s %d", arr[num], &age);
        if (age > maxage)
        {
            maxage = age;
            old = arr[num];
        }
        if (age < minage)
        {
            minage = age;
            young = arr[num];
        }
        num--;
    }
    printf("Oldest member of you family is %s and his age is:%d\n", old, maxage);
    printf("Youngest member of you family is %s and his age is:%d\n", young, minage);
    return 0;
}
















