/*
�������� ���������, ������� � ������� ������� ���������� ������� �����
������ � �������� �������
���������:
������ �� ������� ��������� ������ ���������� �� char, � ������� ���������
������ ������ �������� ������� ����� (������������ - ������ ������� � ��-
�������� ��������). ����� �� ���������� ����� ����� ������, ��������� ����
������ �� ����������.
*/


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>


#define N 256
#define MAXLEN 20

int main()
{
    char str[N];
    char rev[N];
    char* sptr = str;
    char* rptr = rev;
    int i = -1;

    printf("Please enter random string here: \n");
    fgets(str, N, stdin);

    while (*sptr)
    {
        sptr++;
        i++;
    }

    while (i >= 0)
    {
        sptr--;
        *rptr = *sptr;
        rptr++;
        --i;
    }

    *rptr = '\0';

    printf("Reverse of the entered string is: %s\n", rev);

    return 0;
}

