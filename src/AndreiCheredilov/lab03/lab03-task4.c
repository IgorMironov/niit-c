/*�������� ���������, ������� ������� ����� ����� �� �������� ������
���������:
��������� ������������� ����������� ������������������ ���� � ������ ���
����� � ������������ �� ��� ������ �����. � ��������� ������������� ������-
����� �� ������������ ����� ��������, �� ���� ���� ������������ ������ �����
������� ������������������ ����, � ����� ������� �� ��������� �����.*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define N 256
#define MAXSIZEOFSTR 3

int main()
{
    char str[N] = { 0 };
    int i = 0, num = 0, count = 0;
    int sum = 0;


    printf("Please enter the string here:\n");
    fgets(str, N, stdin);

    for (i = 0; i < (strlen(str) - 1); i++)
    {
      if (isdigit(str[i]))
	  {
	  	for(num = 0, count =0; isdigit(str[i]) &&(count <= MAXSIZEOFSTR); i++,count++)
		{
			num=num*10+(str[i]-'0');
		}
		sum = sum + num;
	  }
    }
    printf("%d\n", sum);
    return 0;
}

