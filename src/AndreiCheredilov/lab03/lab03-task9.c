/*�������� ���������, ������������ � ������ ���������� ������������������
������������ �����
���������:
��� ������ AABCCCDDEEEEF ��������� 4 � EEEE
*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define N 256
int main(void)
{
    char str[N];
    char ch = 0;
    char maximumch = 0;
    int i = 0;
    int charnum = 0;
    int countmax = 0;

    printf("Please enter the string here:\n");
    fgets(str, N, stdin);

    ch = str[0];
    for (i = 0; str[i] != '\n'; i++) {
        if (str[i] == ch) {
            charnum++;
        }
        if (str[i] != ch) {
            ch = str[i];
            charnum = 1;
        }
        if (charnum > countmax) {
            countmax = charnum;
            maximumch = str[i];
        }
    }
    printf("%d - ", countmax);

    for (i = 0; i < countmax; i++)
        putchar(maximumch);

    printf("\n");
    return 0;
}


