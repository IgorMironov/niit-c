/*
�������� ���������, ������� ����������� � ������������ ������, ���������
�� ���������� ���� � ����� ����� n, � ����� ������� n - �� ����� �� ������. �
������ ������������� n ��������� ��������� �� ������
���������:
� ���������� ������ ��������� ������� ������ ���� ������� �� �������.
��������� �������������� ������� ������
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define N 256

int main()
{
    int i = 0, //������� �������� ������
        j = 0, //������� �������� ������ � ��������� ������
        digit = 0, //����� ����� ��� ��������
        yy = 0,
        num = 0,
        flag = 0;
    char str[N] = { 0 };

    printf("Please enter random string here:\n");
    fgets(str, N, stdin);

    printf("Please enter number of word to delete here:\n");
    scanf("%d", &digit);

    while (str[i])
    {
        if (isalnum(str[i]) && !flag)
        {
            flag = 1;
            num++;
        }
        if (!isalnum(str[i]) && flag)
        {
            flag = 0;
            if (num == digit)
            {
                j = i - 1;
                while (!isspace(str[j]) && j >= 0)
                {
                    j--;
                    yy++;
                }
                j++;
                while(str[j + yy] != '\0')
                {
                    str[j] = str[j + yy];
                    j++;
                }
                str[j] = '\0';
                puts(str);
                return 0;
            }
        }
        i++;
    }

    printf("An error occured!\n");
    return 0;
}
