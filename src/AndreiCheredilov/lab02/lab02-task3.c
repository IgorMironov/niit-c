/*�������� ���������, ��������� �� ����� ����������� �� ��������*/

#include <stdio.h>
#include <locale.h>
#include <stdlib.h>
#include <string.h>

int main()
{
    int N, x, y, i, k = 0;
    setlocale(LC_ALL, "Rus");

    printf("����������� ������� ���������� ����� � ������������:\n");

    if (scanf("%d", &N) != 1)
    {
        printf("������������ ��� ������\n");
    }
    for (i = 1; i <= N; ++i)
    {
        for (x = 1; x <= N - i; ++x)
        {
            printf(" ");
        }
        while (k != 2 * i - 1)
        {
            printf("*");
            ++k;
        }
        k = 0;
        printf("\n");
    }

    return 0;
}