/*�������� ���������, ��������� ������� ������������� �������� ��� ������-
��� ������������� ������. � ���� ������� ���������� ������ ������ � �����
��� ����������.*/

#include <stdio.h>
#include <string.h>

int main()
{
    char str[256];
    int c = 0;
    int count[26] = { 0 };

    printf("Please enter a string:\n");
    fgets(str, 256, stdin);

    while (str[c] != '\0')
    {
        if (str[c] >= 'a' && str[c])
            count[str[c] - 'a']++;
        c++;
    }

    for (c = 0; c < 26; c++)
    {
        if (count[c] != 0)
            printf("%c occurs %d times in the entered string. \n", c + 'a', count[c]);
    }

    return 0;
}
