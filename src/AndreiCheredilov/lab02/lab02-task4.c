/*�������� ���������, �������������� ������� � ������� �������� �������:
������� ���� ��������� �����, ����� �����. ������ �������� � ���� �������-
�� � ���� ��������� ������������������ ���� � ����. ������������ �������-
�������� ��������� ������.
���������:
���������� � ������ ������ ����������� ����� �� ������������. ����� ��-
���������� ����������� ��������� �������.*/


#include <ctype.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define N 63

int main()
{
    int i, j, buff = 0;
    char alphanum[] = "0123456789"
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                      "abcdefghijklmnopqrstuvwxyz";
    char arr[256] = { 0 };
    char arr2;

    //��������� ��������� ������ arr[i]=(rand()%2) ? rand()%10+'0' : rand()%26+'A';
    srand(time(NULL));
    for (i = 0; i < N; i++)
    {
        arr[i] = alphanum[rand() % strlen(alphanum)];
    }
    printf("%s\n", arr);
    printf("The length of the generated string is: %d\n", strlen(arr));

    //����������� ��������� �������
    for (i = 0, j = (N - 1); i < j; i++, j--)
    {
        while (i < N && isalpha(arr[i]))
            i++;
        while (j >= 0 && isdigit(arr[j]))
            j--;
        if (i > j)
            break;
        arr2 = arr[i];
        arr[i] = arr[j];
        arr[j] = arr2;
    }
    printf("%s\n", arr);
    return 0;
}
