/*�������� ���������, ������� ������� �� ����� 10 �������, ���������������
��������� ������� �� ��������� ���� � ����, ������ ����� ������ ����
��� � ������, ��� � � ������� ���������. ����� ������ - 8 ��������.
���������:
������ ���������������� ������: Nh1ku83k
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>
#define N 8


int main()
{
    int i, j;
    char alphanum[] = "0123456789"
                      "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                      "abcdefghijklmnopqrstuvwxyz";
    char arr[256] = { 0 };
    srand(time(NULL));

    for (j = 0; j < N; j++)
    {
        //��������� ��������� ������
        for (i = 0; i < N; i++)
        {
            arr[i] = alphanum[rand() % strlen(alphanum)];
        }
        printf("%s\n", arr);
    }
    return 0;
}
