/*�������� ���������, ��������� ������ �� ������ ��������. ������� ���-
������ ������� � ������ ������, � ����� ������ � ������� ����� �������,
���� �� ���������� ������ 1.*/

#include <stdio.h>

int main()
{
    char x;
    int count = 0;

    printf("Please enter a string:\n");
    while (x != '\n')
    {
        scanf("%c", &x);
        if (x == ' ')
            count++;

        else
            count = 0;

        if (count >= 2)
        {
            continue;
        }
        printf("%c", x);
    }
    return 0;
}
