#define _CRT_SECURE_NO_WARNINGS
/*
������� �� �������� � ������� � ��������
*/
#include <stdio.h>
#include <string.h>
#define min 0
#define maxD 360
#define maxR 6.3
#define PI 3.1416

int main(void)
{
	int checkmark = 0, checkdigit = 0, R = 0, D = 0;
	char mark;
	float digit;


	while ((checkmark == 0) && (checkdigit == 0))
	{
		printf("Enter 0-360D (for degree) or  0-6.3R (for radian) \n");
		scanf("%f%c", &digit, &mark);

		if (mark == 'R' || mark == 'r')
		{
			checkmark = 1;
			if ((digit >= min) && (digit <= maxR))
			{
				R = 1;
				checkdigit = 1;
			}
			else printf("error R > 6.3\n");
		}
		else if (mark == 'D' || mark == 'd')
		{
			checkmark = 1;
			if ((digit >= min) && (digit <= maxD))
			{
				D = 1;
				checkdigit = 1;
			}
			else printf("error D > 360\n");
		}
		else printf("incorrect mark  (Mark=R or D)\n");
	}

	float  result;
	if (R == 1)
	{
		result = ((digit * (maxD / 2)) / PI);
		printf("%.2f%c = %.2f D\n", digit, mark, result);
	}
	else
	{
		result = ((digit*PI) / (maxD / 2));
		printf("%.0f%c = %.2f R\n", digit, mark, result);
	}

	return 0;
}


