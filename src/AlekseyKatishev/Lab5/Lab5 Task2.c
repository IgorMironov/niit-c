#define _CRT_NO_WARNING
/*
2. �������� ��������� ������������, ��������� �� ����� ������
�����, ������������ �� ����������� ������������� ��������� �*�.
����������� ����������� � ��������� ���������� �������, � ��
��� ��� ����� � ����������� ���������� � ��������� ��� �����.
���������:
������� ������ ��������� � ���� ��������� ������������������ �����:
1) ������� ������� (���������� ���������)
2) ������������ ��������� ������� �������� ������ ��������� (���������
�*�)
3) ����������� �������� � ������ ��������� �������
4) ������� ������
5) ����� ������� �� ����� (���������)
6) ��������� ��������
7) ������� � ���� 1.
*/
#include <stdio.h>
#include <string.h>
#include <windows.h>
#define M 20
#define N 20
#define STARS 4
char BG[M][N];


void clearBG(char (*arr)[N])
{
	int i, j;
	for (i = 0; i < M; i++)
		for (j = 0; j < N; j++)
			arr[i][j] = 0;
}
void fillQTR(char(*arr)[N])
{
	int MaxHorizontal = M / 2,
		MaxVertical = N / 2,
		RndHor, RndVert,
		i;
	for (i = 0; i < STARS; i++)
		if (STARS < (MaxHorizontal*MaxVertical))
		{
			RndHor = rand() % MaxHorizontal;
			RndVert = rand() % MaxVertical;
			arr[RndVert][RndHor] = '*';
		}
		else
		{
			printf("it is too much stars!");
			break;
		}

}
void print(char(*arr)[N])
{
	int i,j;
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < N; j++)
			putchar(arr[i][j]);
		puts("");
	}
}
void cpyStars(char(*arr)[N])
{
	int i, j;
	for (i = 0; i < M/2; i++)
		for (j = 0; j < N/2; j++)
			if (arr[i][j] == '*')
			{
				arr[M-1-i][j] = '*';//������ �����
				arr[M-1-i][N-1-j] = '*';//������ ������
				arr[i][N-1-j] = '*';//������� ������
			}
}


int main()
{
	while (1)
	{
		srand(time(NULL));
		clearBG(BG);
		fillQTR(BG);
		cpyStars(BG);
		system("cls");
		print(BG);
		Sleep(1000);
	}
	return 0;
}