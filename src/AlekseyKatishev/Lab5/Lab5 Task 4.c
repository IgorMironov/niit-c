#define _CRT_SECURE_NO_WARNINGS

/*�������� ���������, ������� ������ ��������� ��������� ���� �
������������ �������� ����� � ������ ������
��������� :
��������� ��������� ������������ ���������� ���� � ������ ��� �������
��.��� ������ ������ ���������� �������, ������������� � ������ ������
1.
*/
#define M 256

#include<stdio.h>
#include <time.h>
FILE *in;
FILE *out;
char line[M];
char result[M];
char *indexes[M] = {0};
int check_opening()
{
	if ((in == NULL) || (out == NULL))
	{
		perror("File:");
		return(1);
	}
	else
		return(0);
}
int get_line()
{
	if (fgets(line, M, in) != NULL)
	{
		return 1;
	}
	else
		return 0;
}

int main()
{
	srand(time(NULL));
	in = fopen("ChangeWord.txt", "rt");
	out = fopen("NewWords.txt", "wt");
	if (check_opening)
	{
		while (get_line())
		{
			getWords(line, indexes);
			clear(result);
			mixWords(result, indexes);
			fputs(result, out);
		}

		fclose(in);
		fclose(out);
	}
	else
		return 1;
	return 0;
}
