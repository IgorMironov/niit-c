/*
1. �������� ���������, ������� ��������� � ��������� ����������
������� ����������� ����������� � ������� ��� �� ������� (��.
�������)
*/
#define _CRT_SECURE_NO_WARNING
#include<stdio.h>
#define STEP 3
#define M 40
#define N 40

char BG[M][N] = {0};
int depth = STEP * 3;


void start(char (*arr)[N],int x,int y,int stp)
{
	if (stp > 1)
	{
		start(arr, x, y, stp / 3);
		start(arr, x + stp, y, stp / 3);
		start(arr, x - stp, y, stp / 3);
		start(arr, x, y + stp, stp / 3);
		start(arr, x, y - stp, stp / 3);
	}
	else
	{
		arr[x + stp][y] = '*';
		arr[x - stp][y] = '*';
		arr[x][y + stp] = '*';
		arr[x][y - stp] = '*';
		arr[x][y] = '*';
	}
}

void print(char(*arr)[N])
{
	int i, j;
	for (i = 0; i < M; i++)
	{
		for (j = 0; j < N; j++)
			putchar(arr[i][j]);
		puts("");
	}
}

int main()
{
	start(BG,M/2,N/2,depth);
	print(BG);
	return 0;
}
