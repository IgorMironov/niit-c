//Remove extra spaces without using additional arrays

#include <stdio.h>
#include <string.h>

#define SIZE 80
#define DeletesChar ' '

void GetLane(char *lane);
void DelitSpase(char *lane);
void MoveLine(char *lane, int flag);

int main() 
{
	char userlane[SIZE];
	GetLane(userlane);
	DelitSpase(userlane);
	printf("%s\n", userlane);
	return 0;
}
void GetLane(char *lane)
{
	fgets(lane, SIZE, stdin);
	lane[strlen(lane) - 1] = 0;
}
void DelitSpase(char *lane)
{
	int i = 0;
	for (i = 0; lane[i] != 0; i++)
	{
		if (lane[i] == DeletesChar &&  lane[i + 1] == DeletesChar || lane[i] == DeletesChar && i == 0
			|| lane[i] == DeletesChar && i == lane[strlen(lane) - 1])
		{
			MoveLine(lane, i);
			i--;
		}
	}
}
void MoveLine(char *lane, int flag)
{
	lane[flag] = lane[flag + 1];
	flag++;
	if (flag <= strlen(lane))
		MoveLine(lane, flag);
}