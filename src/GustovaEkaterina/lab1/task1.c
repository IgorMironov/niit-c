/* Write a program that prompts the user for the gender,
height and weight, and then analyzes the ratio of height and weight, 
giving recommendations for further action (lose weight, get fat,
norm)*/

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	int weight, hight,difference, reference;
	const int  referenceforman = 100;
	const int  referenceforwoman = 110;
	const char *identsgender[] = { "m","M","f","F" };
	char identGender = 0;
	const char *datarequest[] = { "Enter your gender. Male - m, or female - f.", "Enter your height and weight.(Enter a comma)." };
	do
	{
		puts(datarequest[0]);
		identGender = getchar();
	} while (identGender != *identsgender[0] && identGender != *identsgender[1] &&
		     identGender != *identsgender[2] && identGender != *identsgender[3]);
	if (identGender == *identsgender[0] || identGender == *identsgender[1])
		reference = referenceforman;
	else if (identGender == *identsgender[2] || identGender == *identsgender[3])
		reference = referenceforwoman;
    const int minhight = 50;
	const int maxhight = 210;
	const int minweight = 20;
	const int maxweight = 250;
	do
	{
		puts(datarequest[1]);
		scanf("%d,%d", &hight, &weight);
		do
		{
			getchar();
			getchar();
		} while (hight == EOF || weight == EOF);
	} while (hight < minhight || hight > maxhight ||
		     weight < minweight || weight > maxweight);
	difference = hight-weight;
	char *weightindex[] = { "Normal weight", "Underweight", "Overweight" };
	if (difference==reference)
		puts(weightindex[0]);
	else if (difference>reference)
		puts(weightindex[2]);
	else if (difference<reference)
		puts(weightindex[3]);
	return 0;
}