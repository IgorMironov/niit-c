/* Conclusion greetings depending on the time of day*/

#define _CRT_SECURE_NO_WARNINGS
#include<stdio.h>

int main()
{
	int hours, minutes, seconds;
	const int minhours = 0;
	const int maxshours = 24;
	const int minminutes = 0;
	const int maxmimutes = 60;
	const int minseconds = minminutes;
	const int maxseconds = maxmimutes;
	char *datarequest[] = { "Enter the current time.(In format hh : mm:ss)." };
	do
	{
		puts(datarequest[0]);
		scanf("%d:%d:%d", &hours, &minutes, &seconds);
		do
		{
			getchar( );
		} while (hours == EOF || minutes == EOF || seconds == EOF);
	} while (hours < minhours || minutes < minminutes ||
		     hours > maxshours || minutes > maxmimutes ||
		     seconds < minseconds || seconds > maxseconds);
	const int noon = 12;
	const int morning = 6;
	const int evening = 18;
	const char *TimesOfDay[] = { "Good night!","Good morning!", "Good day!", "Good evening!" };
	if (hours < noon)
	{
		if (hours < morning)
			puts(TimesOfDay[0]);
		else if (hours >= morning)
			puts(TimesOfDay[1]);
	}
	else if (hours >= noon)
	{
		if (hours < evening)
		puts(TimesOfDay[2]);
		else if (hours >= evening)
			puts(TimesOfDay[3]);
	}
	return 0;
}
