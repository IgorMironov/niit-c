/*Write a program that randomly permuting the characters 
of each word of each line of a text file*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "HeaderForTasks1,3,4.h"

int main()
{
	srand(time(NULL));
	FILE *fp;
	char line[SIZELEN];
	fp = fopen("input.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	while (fgets(line, SIZELEN, fp))
	{
		ShakeWords(line);
	}
	fclose(fp);
	return 0;
}