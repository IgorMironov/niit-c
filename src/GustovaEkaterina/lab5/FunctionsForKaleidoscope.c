#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <Windows.h>
#include "HeaderForKaleidoscope.h"

void CleanArr(char(*arr)[LEN])
{
	int i, j;
	for (i = 0; i < HIGHT; i++)
		for (j = 0; j < LEN; j++)
		{
			arr[i][j] = ' ';
		}
}
void FormationQuarter(char(*arr)[LEN])
{
	static int FLAG = 0;
	int i, j;
	if (FLAG < NumSym)
	{
		i = rand() % (HIGHT / 2);
		j = rand() % (LEN / 2);
		arr[i][j] = '*';
		FormationArr(i, j, arr);
		FLAG++;
		FormationQuarter(arr);
	}
	else
		FLAG = 0;
}
void FormationArr(int i, int j, char(*arr)[LEN])
{
	arr[i][LEN - j] = '*';
	arr[HIGHT  - i][j] = '*';
	arr[HIGHT  - i][LEN  - j] = '*';
}
void PrintArr(char(*arr)[LEN])
{
	int i, j;

	for (i = 0; i < HIGHT; i++)
	{
		for (j = 0; j < LEN; j++)
			putchar(arr[i][j]);
		putchar('\n');
	}
}