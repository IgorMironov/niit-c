//Write a program "Kaleidoscope"

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <Windows.h>
#include "HeaderForKaleidoscope.h"

int main()
{
	srand(time(0));
	char ARR[HIGHT][LEN];
	while (1)
	{
		CleanArr(ARR);
		FormationQuarter(ARR);
		PrintArr(ARR);
		Sleep(SEC);
		system("cls");
	}
	return 0;
}
