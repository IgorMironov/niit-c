/*Write a program that accepts a string from the user and
displays it on the screen, the words mixed up in random order.*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "HeaderForTasks1,3,4.h"


int main()
{
	srand(time(0));
	char Userlane[SIZELEN];
	GetLane(Userlane);
	CreatePointers(Userlane);
	return 0;
}