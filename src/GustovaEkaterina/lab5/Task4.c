/*Write a program that reads a text file line by line and
randomly rearranges the words in each line*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#include "HeaderForTasks1,3,4.h"

int main()
{
	srand(time(NULL));
	FILE *fp;
	char line[SIZELEN];
	fp = fopen("input.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}
	while (fgets(line, SIZELEN, fp))
	{
		CreatePointers(line);
		line[strlen(line) - 1] = 0;
		putchar('\n');
	}
	fclose(fp);
	return 0;
}