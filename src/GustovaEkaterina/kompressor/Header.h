struct sym 
{
	unsigned char ch;
	float freq;
	char code[255];
	struct sym *left;
	struct sym *right;
};
typedef struct sym SYM;

union code
{
	unsigned char bufer_for_record;

	struct byte
	{
		unsigned b1 : 1;
		unsigned b2 : 1;
		unsigned b3 : 1;
		unsigned b4 : 1;
		unsigned b5 : 1;
		unsigned b6 : 1;
		unsigned b7 : 1;
		unsigned b8 : 1;
	}byte;
};

SYM *makeTree(SYM *psym[], int k);
void makeCodes(SYM *root);