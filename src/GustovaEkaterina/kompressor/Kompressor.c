#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Header.h"

int main()
{
	FILE *fp_input, *fp_binar, *fp_output; 
	fp_input = fopen("input.txt", "rb"); 
	fp_binar = fopen("temp.txt", "wb");
	fp_output = fopen("input_komp.txt", "wb");
	int buf_read;  
	int count_for_num = 0; 
	int count_for_characters = 0; 
	int count_for_binar_file = 0;
	int tail_size;
	int arr_uni_char[256] = { 0 };
	union code code1;
	SYM temp;
	SYM simbols[256] = { 0 };
	SYM *psym[256];
	float sum_frec_occurence = 0;
	int bitmap[8];
	int j = 0, i = 0;
	if (fp_input == NULL)
	{
		perror("File:");
		return 1;
	}
	while ((buf_read = fgetc(fp_input)) != EOF)
	{
		for (int j = 0; j < 256; j++)
		{
			if (buf_read == simbols[j].ch)
			{
				arr_uni_char[j]++;
				count_for_characters++;
				break;
			}
			if (simbols[j].ch == 0)
			{
				simbols[j].ch = (unsigned char)buf_read;
				arr_uni_char[j] = 1;
				count_for_num++; 
				count_for_characters++;
				break;
			}
		}
	}
	for (int i = 0; i < count_for_num; i++)
		simbols[i].freq = (float)arr_uni_char[i] / count_for_characters;
	for (int i = 0; i < count_for_num; i++) 
		psym[i] = &simbols[i];
	for (int i = 1; i < count_for_num; i++)
		for (int j = 0; j < count_for_num - 1; j++)
			if (simbols[j].freq < simbols[j + 1].freq)
			{
				temp = simbols[j];
				simbols[j] = simbols[j + 1];
				simbols[j + 1] = temp;
			}

	for (int i = 0; i < count_for_num; i++)
	{
		sum_frec_occurence += simbols[i].freq;
		printf("ASCII = %d\tFreq = %f\tCharacter = %c\t\n", simbols[i].ch, simbols[i].freq, psym[i]->ch, i);
	}
	printf("\nThe sum of all characters in the file = %d\t\n", count_for_characters);
	printf("The total frequency of occurrence = %f\n", sum_frec_occurence);
	SYM *root = makeTree(psym, count_for_num);
	makeCodes(root);
	rewind(fp_input);
	while ((buf_read = fgetc(fp_input)) != EOF)
	{
		for (int i = 0; i < count_for_num; i++)
			if (buf_read == simbols[i].ch)
				fputs(simbols[i].code, fp_binar);
	}
	fclose(fp_binar);
	fp_binar = fopen("temp.txt", "rb");
	while ((buf_read = fgetc(fp_binar)) != EOF)
		count_for_binar_file++;
	tail_size = count_for_binar_file % 8;
	fwrite("compresing", sizeof(char), 24, fp_output);
	fwrite(&count_for_num, sizeof(int), 1, fp_output);
	fwrite(&tail_size, sizeof(int), 1, fp_output);
	for (i = 0;i < count_for_num; i++)
	{
		fwrite(&simbols[i].ch, sizeof(SYM), 1, fp_output);
		fwrite(&simbols[i].freq, sizeof(SYM), 1, fp_output);
	}
	rewind(fp_binar);
	j = 0;
	for (int i = 0; i < count_for_binar_file - tail_size; i++)
	{
		bitmap[j] = fgetc(fp_binar);
		if (j == 7)
		{
			code1.byte.b1 = bitmap[0] - '0';
			code1.byte.b2 = bitmap[1] - '0';
			code1.byte.b3 = bitmap[2] - '0';
			code1.byte.b4 = bitmap[3] - '0';
			code1.byte.b5 = bitmap[4] - '0';
			code1.byte.b6 = bitmap[5] - '0';
			code1.byte.b7 = bitmap[6] - '0';
			code1.byte.b8 = bitmap[7] - '0';
			fputc(code1.bufer_for_record, fp_output);
			j = 0;
		}
		j++;
	}
	j = 0;
	for (i = 0; i <= tail_size; i++)
	{
		bitmap[j] = fgetc(fp_binar);
		if (j == tail_size)
		{
			code1.byte.b1 = bitmap[0] - '0';
			code1.byte.b2 = bitmap[1] - '0';
			code1.byte.b3 = bitmap[2] - '0';
			code1.byte.b4 = bitmap[3] - '0';
			code1.byte.b5 = bitmap[4] - '0';
			code1.byte.b6 = bitmap[5] - '0';
			code1.byte.b7 = bitmap[6] - '0';
			code1.byte.b8 = bitmap[7] - '0';
			fputc(code1.bufer_for_record, fp_output);
		}
		j++;
	}
	free(root);
	fcloseall();
	return 0;
}