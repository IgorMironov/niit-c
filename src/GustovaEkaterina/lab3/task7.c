/*Write a program that prints the symbol table for the occurrence
entered line, sorted in descending order of frequency*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#define SIZE 80
#define Characters 256

void LineRequest(char *len);         
void Counts(int *counts, char *len); 
void OutputResult(int *count);        
void Print(int letter, int num);    

int main()
{
	char userlane[SIZE];
	LineRequest(userlane,SIZE); 
	int count[Characters] = { 0 };
	Counts(count,userlane);
	OutputResult(count);
	return 0;
}

void LineRequest(char *len)
{
	printf("Enter your line.\n");
	fgets(len, SIZE, stdin);
}
void Counts(int *counts, char *len)
{
	int i = 0;
	while (len[i] != '\n')
	{
		counts[len[i++]]++;
	}
}
void Print(int letter, int num)
{
		printf("%c is found %d times", letter, num);
		putchar('\n');
}
void OutputResult(int *count)
{
	int i = 0;
	int temp = 0;
	int buf;
	for (i = 0; i < Characters; i++)
	{
		if (count[i] > temp)
		{
			temp = count[i];
			buf = i;
		}
	}
	if (temp > 0)
	{
		Print(buf, temp);
		count[buf] = 0;
		OutputResult(count);
	}
}