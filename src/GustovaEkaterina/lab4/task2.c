/*Write a program that uses an array of pointers outputs words
row in reverse order*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>

#define SIZE 80

int main()
{
	int i = 0;
	int count = 0;
	char *bufPointer;
	char userline[SIZE];
	char *pointers[SIZE];
	puts("Enter your line:");
	fgets(userline, SIZE, stdin);
	userline[strlen(userline) - 1] = 0;
	for (i = 0; userline[i]; i++)
	{
		if (isalnum(userline[i]) && i == 0 || 
			isalnum(userline[i]) && isspace(userline[i - 1]))
		{
			pointers[count] = &userline[i];
			count++;
		}
	}
	for ( i = count - 1; i >= 0; i--)
	{
		bufPointer = pointers[i];
		while (isalnum(*bufPointer))
		{
			putchar(*bufPointer);
			bufPointer++;
		}
		putchar(' ');
	}
	putchar('\n');
	return 0;
}

