/* Практикум 5. ФУНКЦИИ для задач: 1 и 4.

a) printWord - выводит слово из строки (до конца строки или пробела)
b) getWords - заполняет массив указателей адресами первых букв слов
c) randomIndex - перемешивание слов в случайном порядке
*/
#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include <time.h>
#include <windows.h>

int getWords(char* string,char** startWord,int len)
{
	int i,j;
	int countWord=0;

	for(i=0,j=0;i<len;i++)
	{
		if(string[0]!=' '&&string[0]!='\0')
			startWord[0]=&string[0];

		if(string[i]!=' '&&string[i-1]==' ')
			startWord[j]=&string[i];

		if(string[i]!=' '&&(string[i+1]==' '||string[i+1]=='\0'))
		{
			j++;
			countWord++;
		}
	}
	return countWord;
}

int randomIndex(int* index,int countWord)
{
	int i,left=0,right=0,temp=0;
	srand(time(NULL));

	for(i=0;i<countWord;i++)
		index[i]=i;

	for(i=0;i<countWord;i++)
	{
		do
		{
			left=rand( )%countWord;
			right=rand( )%countWord;
		} while(left==right);

		temp=index[left];
		index[left]=index[right];
		index[right]=temp;
	}
}

void printWord(char *string,char** startWord,int* index,int countWord)
{
	int i,j;

	for(i=0; i<countWord; i++)
	{
		j=index[i];
		while(*startWord[j]&&*startWord[j]!=' ')
			printf("%c",*startWord[j]++);
		putchar(' ');
	}
	printf("\n");
}