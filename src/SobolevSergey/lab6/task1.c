/* Практикум 6. Задача 1.
Написать программу, которая формирует в двумерном символьном
массиве фрактальное изображение и выводит его на консоль (см.
рисунок)Фракталом обычно называют изображение, любая часть которого подобна це-
лому. Поэтому, при рисовании фрактала используют рекурсию и некоторый
масштабный коэффициент, определяющий размер изображения.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

#define SCALE_FACTOR 3
#define N 28
#define STAR '*'


char screen[N][N];

int pow3(int n)
{
	return n<=0 ? 1 : pow3(n-1)*3;
}

void print( )
{
	int i,j;
	
	for(i=0; i < N; i++)
	{
		for(j=0; j < N; j++)
			putchar(screen[i][j]);
		putchar('\n');
	}
	printf("\n       VICSEK  FRACTAL\n\n");
}

void vicsek_fractal(int n,int m,int deep)
{
	if(deep<=0)
	{
		screen[n][m]=STAR;
		return;
	}
	int size=pow3(deep-1);
	vicsek_fractal(n,m,deep-1);
	vicsek_fractal(n+size,m,deep-1);
	vicsek_fractal(n-size,m,deep-1);
	vicsek_fractal(n,m+size,deep-1);
	vicsek_fractal(n,m-size,deep-1);
}

int main( )
{
	vicsek_fractal(N/2,N/2,SCALE_FACTOR);
	print( );
	return 0;
}