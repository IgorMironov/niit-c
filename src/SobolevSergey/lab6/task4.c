/* Практикум 6. Задача 4.
Написать программу, которая суммирует массив традиционным и
рекурсивным способами
Замечание:
Программа выполняет следующую последовательность действий:
(a) принимает из командной строки значение степени двойки M;
(b) находит размер динамического массива N = 2M;
(c) выделяет память под динамический массив;
(d) случайным образом заполняет массив данными;
(e) находит сумму традиционным и рекурсивыным способом;
(f) сравнивает время выполнения суммирования трпдиционным и рекурсив-
ным способом;
(g) освобождает динамическую память
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h> 

void fill_arr(int * arr,int n)
{
	int i;
	for(i=0; i < n; i++)
		arr[i]=rand( )%10;
}

int traditional_sum(int * arr,int N)
{
	int i;
	long int sum=0;

	for(i=0;i<N;i++)
		sum+=arr[i];
	return sum;
}

long int recursive_sum(long int* arr,int N)
{
	if(N==1)
		return arr[0];
	else
		return recursive_sum(arr,N/2) + recursive_sum(arr+N/2,N-N/2);
}

int main( )
{
	/*(a) принимает из командной строки значение степени двойки M;*/
	int M;
	puts("Please enter the value of the degree 2 (max 28)\n(this is the number of array elements):");
	scanf("%d",&M);

	/*(b) находит размер динамического массива N = 2M;*/
	int N=pow(2,M);

	/*(c) выделяет память под динамический массив;*/
	long int * pointer_arr=malloc(N * sizeof(int));

	/*(d) случайным образом заполняет массив данными;*/
	srand(time(NULL));
	fill_arr(pointer_arr,N);

	/*(e) находит сумму традиционным и рекурсивным способом;*/
	/*(f) сравнивает время выполнения суммирования традиционным и рекурсив-
	ным способом;*/
	clock_t start,stop;
	long int sum;

	start=clock( );
	sum=traditional_sum(pointer_arr,N);
	stop=clock( );
	printf("\n Traditional.\n Sum array %ld , run time: %f\n",sum,(double)(stop-start)/CLOCKS_PER_SEC);

	start=clock( );
	sum=recursive_sum(pointer_arr,N);
	stop=clock( );
	printf("\n Recursive.\n Sum array %ld , run time: %f\n",sum,(double)(stop-start)/CLOCKS_PER_SEC);

	/*(g) освобождает динамическую память*/
	free(pointer_arr);

	return 0;
}