/* Практикум 6. Задача 6.
Написать реализацию рекурсивной функции, вычисляющую n-ый
элемент ряда Фибоначчи, но без экспоненциально растущей рекур-
сии
Замечание:
Нужно создать две функции: одна вызывается непосредственно из main и вы-
зывает вторую, вспомогательную, которая и является рекурсивной.
*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

typedef unsigned long long ULL;

void fib1(ULL* result,ULL past_result,unsigned int i,int n)
{
	ULL tmp=*result;

	if(i<=n) 
	{
		*result+=past_result;
		fib1(result,tmp,i+1,n);
	}
}

ULL fib(int n)
{
	ULL f=1;

	if(n==0)
		return 0;
	else if(n==1||n==2)
		return 1;
	else 
	{
		fib1(&f,0,2,n);
		return f;
	}
}

int main( )
{
	int n;

	puts("Please enter the N element of the Fibonacci series:");
	scanf("%d",&n);
	printf("F(n=%d) = %lld\n",n,fib(n));

	return 0;
}