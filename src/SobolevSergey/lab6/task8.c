/* Практикум 6. Задача 8.
Написать программу, которая вычисляет целочисленный результат
арифметического выражения, заданного в виде параметра команд-
ной строки. Предусмотреть поддержку 4-х основных операций. По-
рядок вычисления определяется круглыми скобками

Замечание:
В строке выражения могут встречаться символы: 0-9,+,-,*,/,(,) Выражения мо-
гут быть простыми, то есть целиком состоять из одного числа 3, 8, а могут
быть сложными, например, ((6+8)*3) или (((7-1)/(4+2))-9). Предполагает-
ся, что скобки в выражении заданы правильно, то есть количество открытых
равно количеству закрытых и они на допустимых позициях.

Программа должна состоять из следующих функций:
(a) int main(int argc, char* argv[]) - главная функция, в которой осуществля-
ется вызов рекурсивной функции eval для вычисления выражения
(b) int eval(char *buf) - функция, вычисляющая строку, содержащуюся в buf
(c) char partition(char *buf, char *expr1, char *expr2) - функция, которая раз-
бивает строку, содержащуюся в buf на три части: строку с первым опе-
рандом, знак операции и строку со вторым операндом
*/

#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char partition(char *buf,char *expr1,char *expr2)
{
	int left=0,right=0,i=1,j=0,k=0,m=0;;
	char op;
	int len=strlen(buf);
	
	buf[len-1]='\0';
	
	while(buf)
	{
		if(buf[i]=='(')
			left++;
		
		if(buf[i]==')')
			right++;
		
		if(left==right)
			break;
		
		i++;
	}
	
	for(j=1;j<=i;j++)
		expr1[j-1]=buf[j];

	expr1[j-1]='\0';
	
	op=buf[i+1];
	
	for(k=i+2,m=0;buf[k]!='\0';k++)
		expr2[m++]=buf[k];
	
	expr2[m]='\0';
	
	return op;
}

int eval(char *buf)
{
	char expr1[256],expr2[256], op;
	
	if(*buf!='(')
		return atoi(buf);
	
	op=partition(buf,expr1,expr2);
	
	switch(op)
	{
	case '+':
		return eval(expr1)+eval(expr2);
	case '-':
		return eval(expr1)-eval(expr2);
	case '/':
		return eval(expr1)/eval(expr2);
	case '*':
		return eval(expr1)*eval(expr2);
	}
}

int main(int argc,char* argv[])
{
	if(argc<2)
	{
		puts("Error. No data.");
		return 1;
	}

	printf("Integer result arithmetic expressions = %d\n",eval(argv[1]));
	return 0;
}