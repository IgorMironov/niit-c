/* Практикум №3. Задача 4.
Написать программу, которая находит сумму чисел во введённой строке
Замечание:
Программа рассматривает непрерывные последовательности цифр в строке как
числа и обрабатывает их как единое целое. В программе предусмотреть ограни-
чение на максимальное число разрядов, то есть если пользователь вводит очень
длинную последовательность цифр, её нужно разбить на несколько групп.*/

#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 256
#define MAX_digits 5

int main( )
{
	char string[N];
	int i,j,len,digits=0,sum=0;

	puts("Enter a string:");
	fgets(string,N,stdin);
	len=strlen(string);
	
	for(i=0;i<len; i++)
	{
		if(string[i]>='0' && string[i]<='9')
		{
			for(j=0,digits=0; (string[i]>='0' && string[i]<='9')&&(digits<MAX_digits); i++,digits++)
			{
				j=j*10+(string[i]-'0');
			}
			printf("Numbers = %d\n",j);
			i--;
			sum+=j;
		}
	}

	printf("The sum of the numbers in the string = %d\n",sum);
	return 0;
}