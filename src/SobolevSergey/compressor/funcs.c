#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "struct.h"

//открытие файла 
FILE* open_file(char name_file[],const char mode[])
{
	FILE* fp=fopen(name_file,mode);
	if(fp==NULL)
	{
		perror("File error: ");
		return 0;
	}
	return fp;
}

//побайтно читаем файл и составляем таблицу встречаемости
int read_file(FILE* fp_input,SYM* simbols,int* arr_us,int* unique_symbol)
{
	int buf=0;
	int count_us=0;
	while((buf=fgetc(fp_input))!=EOF)
	{
		count_us++;
		for(int i=0;i<256;i++)
		{
			if(buf==simbols[i].ch)
			{
				arr_us[i]++;
				break;
			}
			if(simbols[i].ch==0)
			{
				simbols[i].ch=(unsigned char)buf;
				arr_us[i]=1;
				(*unique_symbol)++;
				break;
			}
		}
	}
	return count_us;
}

// Расчёт частоты встречаемости
void calculating_frequency(int unique_symbol,SYM* simbols,int* arr_us,int count_us)
{
	for(int i=0;i<unique_symbol;i++)
		simbols[i].freq=(float)arr_us[i]/count_us;
}

//в массив указателей заносим адреса записей
void records_addresses(int unique_symbol,SYM** psym,SYM* simbols)
{
	for(int i=0;i<unique_symbol;i++)
		psym[i]=&simbols[i];
}

//сортировка по убыванию
void descending_sort(int unique_symbol,SYM* simbols) 
{
	SYM temp;
	for(int i=1;i<unique_symbol;i++)
		for(int j=0;j<unique_symbol-1;j++)
			if(simbols[j].freq<simbols[j+1].freq)
			{
				temp=simbols[j];
				simbols[j]=simbols[j+1];
				simbols[j+1]=temp;
			}
}

// вывод на экран таблицы символов с частатой по убыванию
void print_char_frec(int unique_symbol,int count_us,SYM* simbols)
{
	float sum_freq=0; //сумма частот встречаемости
	for(int i=0;i<unique_symbol;i++)
	{
		sum_freq+=simbols[i].freq;
		printf("Character =  %c\t\tASCII-code =  %d\tFreq =  %f\t\n",simbols[i].ch,simbols[i].ch,simbols[i].freq);
	}
	printf("\n Count unique symbol=  %d\tSum_freq=%f\n",count_us,sum_freq);
}

//рeкурсивная функция создания дерева Хафмана
SYM* buildTree(struct SYM *psym[],int N)
{
	int i,j;
	// создаём временный узел
	SYM *temp=(SYM*)malloc(sizeof(SYM));
	// в поле частоты записывается сумма частот
	// последнего и предпоследнего элементов массива psym
	temp->freq=psym[N-2]->freq+psym[N-1]->freq;
	// связываем созданный узел с двумя последними узлами
	temp->left=psym[N-1];
	temp->right=psym[N-2];
	temp->code[0]=0;
	if(N==2) // мы сформировали корневой элемент с частотой 1.0
		return temp;	// добавляем temp в нужную позицию psym,
	// сохраняя порядок убывания частоты
    else
    {
		for(i=0;i<N;i++)
            if (temp->freq>psym[i]->freq)
            {
				for(j=N-1;j>i;j--)
                    psym[j]=psym[j-1];

				psym[i]=temp;
                break;
            }
    }
	return buildTree(psym,N-1);
}

 //процедура получения префиксного кода
void makeCodes(SYM *root)	
{
    if (root->left)
    {
        strcpy(root->left->code, root->code);
        strcat(root->left->code, "0");
        makeCodes(root->left);
    }
    if (root->right)
    {
        strcpy(root->right->code, root->code);
        strcat(root->right->code, "1");
        makeCodes(root->right);
    }
}

//в цикле читаем исходный файл, и записываем полученные в функциях коды в промежуточный файл
void write_file_fp_101(FILE* fp_input,int unique_symbol,SYM* simbols,FILE* fp_101)
{
	int buf=0;
	rewind(fp_input);//возвращаем указатель в файле в начало файла
	
	while((buf=fgetc(fp_input))!=EOF)
	{
		for(int i=0;i<unique_symbol;i++)
			if(buf==simbols[i].ch)
				fputs(simbols[i].code,fp_101);
	}
}

//Считаем размер бинарного файла(количество символов в нём)
int size_binary_file(FILE* fp_101)
{
	int buf=0;
	int count_01=0;
	while((buf=fgetc(fp_101))!=EOF)
		count_01++;
	return count_01;
}

// вывод на экран таблицы символов с новыми кодами и количеством символов в бинарном файле
void print_count_unique_symbol(int unique_symbol,SYM* simbols,int count_01)
{
	for(int i=0;i<unique_symbol;i++)
	{
		printf("Character =  %c\t\tASCII-code =  %d\tCode =  %d\t\n",simbols[i].ch,simbols[i].ch,simbols[i].code);
	}
	printf("\n Count unique symbol=  %d\n",count_01);
}

//Записываем в сжатый файл таблицу встречаемости
void write_table_occurrence(int unique_symbol,SYM*simbols,FILE* fp_result)
{
	for(int i=0;i<unique_symbol;i++)
	{
		//fwrite(&simbols[i].ch,sizeof(SYM),1,fp_result);
		fwrite(&simbols[i].ch,sizeof(char),1,fp_result);
		//fwrite(&simbols[i].freq,sizeof(SYM),1,fp_result);
		fwrite(&simbols[i].freq,sizeof(float),1,fp_result);
	}
}

//Функция упаковки
void function_packing(int count_01,int tail_length,int* mes,FILE* fp_101,union CODE code,FILE* fp_result)
{
	int i,j=0;
	for(i=0;i<count_01-tail_length;i++)
	{
		mes[j]=fgetc(fp_101);
		if(j==7)
		{
			code.byte.b1=mes[0]-'0';
			code.byte.b2=mes[1]-'0';
			code.byte.b3=mes[2]-'0';
			code.byte.b4=mes[3]-'0';
			code.byte.b5=mes[4]-'0';
			code.byte.b6=mes[5]-'0';
			code.byte.b7=mes[6]-'0';
			code.byte.b8=mes[7]-'0';
			fputc(code.ch,fp_result);
			j=0;
		}
		j++;
	}
}

//Функция записи хвоста
void function_tail_length(int tail_length,int* mes,FILE* fp_101,union CODE code,FILE* fp_result)
{
	int i,j=0;
	for(i=0;i<=tail_length;i++)
	{
		mes[j]=fgetc(fp_101);
		if(j==tail_length)
		{
			code.byte.b1=mes[0]-'0';
			code.byte.b2=mes[1]-'0';
			code.byte.b3=mes[2]-'0';
			code.byte.b4=mes[3]-'0';
			code.byte.b5=mes[4]-'0';
			code.byte.b6=mes[5]-'0';
			code.byte.b7=mes[6]-'0';
			code.byte.b8=mes[7]-'0';
			fputc(code.ch,fp_result);
		}
		j++;
	}
}

//функция добавления имени имя.101 и имя.расширение.arh
char* add_file(const char source[],char dest[],const char extension[])
{
	strcpy(dest,source);
	strcat(dest,extension);
	return dest;
}

//функция раскодирования 
unsigned char search_code(FILE *fp_101,struct SYM *root)
{
	int ch;
	ch=fgetc(fp_101);
	if(ch=='0' && root->left!=NULL && root->right!=NULL)
		return search_code(fp_101,root->left);
	if(ch=='1' && root->left!=NULL && root->right!=NULL)
		return search_code(fp_101,root->right);
	if(root->left==NULL && root->right==NULL)
		ungetc(ch,fp_101);
	return root->ch;

}