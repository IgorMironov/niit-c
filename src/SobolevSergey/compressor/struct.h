#define _CRT_SECURE_NO_WARNINGS

typedef struct SYM SYM;

struct SYM							// представление символа
{
	unsigned char ch;				// ASCII-код
	float freq;						// частота встречаемости
	char code[256];					// массив для нового кода
	SYM *left;						// левый потомок в дереве
	SYM *right;						// правый потомок в дереве
};
union CODE {						// Объединение для Упаковки данных
	unsigned char ch;
	struct {
		unsigned short b1 : 1;
		unsigned short b2 : 1;
		unsigned short b3 : 1;
		unsigned short b4 : 1;
		unsigned short b5 : 1;
		unsigned short b6 : 1;
		unsigned short b7 : 1;
		unsigned short b8 : 1;
	} byte;
};

union CODE code;//инициализируем переменную code

FILE* open_file(char name_file[],const char mode[]);
int read_file(FILE* fp_input,SYM* simbols,int* arr_us,int* unique_symbol);
void calculating_frequency(int unique_symbol,SYM* simbols,int* arr_us,int count_us);
void records_addresses(int unique_symbol,SYM** psym,SYM* simbols);
void descending_sort(int unique_symbol,SYM* simbols);
void print_char_frec(int unique_symbol,int count_us,SYM* simbols);
SYM* buildTree(struct SYM *psym[],int N);
void makeCodes(SYM *root);
void write_file_fp_101(FILE* fp_input,int unique_symbol,SYM* simbols,FILE* fp_101);
int size_binary_file(FILE* fp_101);
void print_count_unique_symbol(int unique_symbol,SYM* simbols,int count_01);
void write_table_occurrence(int unique_symbol,SYM*simbols,FILE* fp_result);
void function_packing(int count_01,int tail_length,int* mes,FILE* fp_101,union CODE code,FILE* fp_result);
void function_tail_length(int tail_length,int* mes,FILE* fp_101,union CODE code,FILE* fp_result);
char* add_file(const char source[],char dest[],const char extension[]);
char search_code(FILE *fp_101,struct SYM *root);