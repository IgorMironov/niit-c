//
//  main.c
//  task5
//
//  Created by Yulia Vorobjeva on 11.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

/*Написать программу, которая измеряет время вычисления N-ого члена ряда Фибоначчи. 
 Предусмотреть вывод таблицы значений для N в диапазоне от 1 до 40
 (или в другом диапазоне по желанию) на экран и в файл
 */
#include <stdio.h>
#include <time.h>

#define N 40
typedef long long ll;

ll fibbonaci(int i)
{
    if(i <=1)
        return i;
    else return fibbonaci(i-1)+ fibbonaci(i-2);
}

int main()
{
    ll i, time_fib, fib;
    FILE * fp = fopen("fibbonaci.txt", "wt");
    if(fp == NULL)
    {
        perror("file:");
        return 1;
    }
    
    for(i=2;i <N;i++)
    {
        time_fib = clock();
        fib = fibbonaci(i);
        time_fib = clock()-time_fib;
        fprintf(fp, "%lld, %lld, %lld\n", i, fib,time_fib);//вывод в файл
        printf("%lld-й член ряда Фибоначчи равен %lld и вычисляется за %lld время\n", i, fib, time_fib);//вывод на экран
    }
    fclose(fp);
    return 0;
}

