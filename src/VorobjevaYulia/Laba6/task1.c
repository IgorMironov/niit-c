//
//  main.c
//  task1
//
//  Created by Yulia Vorobjeva on 04.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#define N 27


char picture [N][N];
int deep = 3;

void print()
{
    int i,j;
    for(i=0;i<N;i++)
    {
        for(j=0;j<N;j++)
            printf("%c", picture[i][j]);
        printf("\n");
    }
}


int isdeep(int n)
{
    if(n<=0)
        n=1;
    else n= isdeep(n-1)*3;
    return n;
}
void paint(int x,int y , int deep)
{
    if(!deep)
    {
        picture[x][y] = '*';
        return;
    }
    int size = isdeep(deep - 1);
    paint(x,y,deep -1);
    paint(x+size,y,deep-1);
    paint(x-size,y,deep-1);
    paint(x,y+size,deep-1);
    paint(x, y-size,deep-1);
}


int main()
{
    int i,j;
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            picture[i][j] = ' ';
    paint(N/2, N/2, deep);
    print();
}
