//
//  main.c
//  task8
//
//  Created by Yulia Vorobjeva on 19.01.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>


#define N 256


char partition(char *data,  char *leftpart, char *rightpart)
{
    int left = 0, right = 0, i = 1, j, k, l;
    char sign;
    int len = strlen(data);
    data[len-1] = '\0';
    
    while(data)

    {
        if(data[i] == '(')
            left++;
        if(data[i] == ')')
            right++;
        if(left==right)
            break;
        i++;
    }
    
    for(j=1;j<=i;j++)
        leftpart[j-1] = data[j];
    
    leftpart[j-1] = '\0';
    
    sign = data[i+1];
    for(k=i+2,l=0;data[k]!='\0';k++)
        rightpart[l++]=data[k];
    
    rightpart[l]='\0';
    
    return sign;
}

int eval(char *data)
{
    char leftpart[N], rightpart[N];
    char sign;
    
    if (*data!= '(')
        return atoi(data);
    
    sign = partition(data, leftpart, rightpart);
    
    switch(sign)
    {
        case '+':
            return eval(leftpart)+eval(rightpart);
        case '-':
            return eval(leftpart)-eval(rightpart);
        case '/':
            return eval(leftpart)/eval(rightpart);
        case '*':
            return eval(leftpart)*eval(rightpart);
    }

    return 0;
}

int main(int argc, char * argv[])
{
    if (argc<=1)
    {
        printf("You don't have a data\n");
        return 1;
    }
    printf("%d\n",eval(argv[1]));
    return 0;
}



