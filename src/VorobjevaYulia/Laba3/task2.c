// programm counts number of words and print each word in the new line
#include <stdio.h>
#include <ctype.h>

#define N 256
#define TRUE 1
#define FALSE 0

int main()
{
    char text[N] = {0};
    int inword = FALSE,
        i = 0,
        word = 0,
        chars = 0;
    
    printf("Enter your text: ");
    for(i = 0; (text[i] = getchar()) != EOF && i < N; i++)
    {
        if(!isspace(text[i]) && !inword)
        {
            inword = TRUE;
            word++;
        }
        if(isspace(text[i]) && inword)
        {
            inword = FALSE;
            printf(" - contain %d symbols\n", chars);
            chars = 0;
        }
        if(!isspace(text[i]) && inword)
        {
            putchar(text[i]);
            chars++;
        }
    }

    printf("\n In your text %d words!\n", word);

    return 0;
}
