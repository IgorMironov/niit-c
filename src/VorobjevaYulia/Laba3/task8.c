#include <stdio.h>
#include <stdlib.h>
#define N 256

int main (){
	char text[N];
	int len, count, i;
	printf("Enter your text:\n");
	fgets(text, N, stdin);
	len  = strlen(text);
	text[len-2] = '\0';
	len = len-2;
	for(i = 0;i<len;i++)
	{
		if(text[i]!= ' ' && text[i+1] == ' ')
			count++;
		else if(text[i]!= ' '&& text[i+1] == '\0')
			count++;
	}
	printf("Number of words is:%d", count);
	return 0;
}
