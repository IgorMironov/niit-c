
/*
 * File:   main.c
 * Author: yuliavorobjeva
 *
 * Created on 1 января 2017 г., 15:00
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>



#define N 20


void clearing (char mass[N][N])
{
    int i,j;
    for(i=0;i<N;i++)
        for(j=0;j<N;j++)
            mass[i][j]= ' ';
}

void drowing(char mass[N][N])
{
    srand(time(NULL));
    int count,i,j;
    for(count=0;count<N;count++)
    {
        i=rand()%(N/2)+1;
        j=rand()%(N/2)+1;
        mass[i][j]= '*';
        mass[i][N-j]='*';
        mass[N-i][j]='*';
        mass[N-i][N-j]='*';
    }
    
}

void clear(void)
{
    system("cls");
}


int main()
{
    int i, j;
    char mass[N][N] = {0};
    while(1)
    {
        clearing(mass);
        drowing(mass);
        for(i=0;i<N;i++)
        {
            for(j=0;j<N;j++)
                printf("%c", mass[i][j]);
            printf("\n");
        }
        sleep(1);
        clear();
    }
    return 0;
}
