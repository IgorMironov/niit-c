#include <stdio.h>
#include <stdlib.h>
#include <locale.h>

int main() {
	setlocale(LC_ALL, "Rus");
	int Foot, Inch;
	float cm;
	char znak;

	printf("enter your height in foots and inches. For example 2.54\n");
	scanf("%d%c%d",&Foot,&znak,&Inch);
	if (znak == 39){
		cm = (Foot*12+Inch)*2.54;
		printf("%.1f cm\n",cm);
	}
	else 
		printf("Uncorrect data\n");
	return 0;
}
