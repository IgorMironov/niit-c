//programm prints a string in a centre
#include<stdio.h>
#include<string.h>
#include<locale.h>

int main()

{
	setlocale(LC_ALL,"Rus");
	char mass[100];
	int dlina, spaces; 
	printf("enter your string\n");
	fgets(mass,100,stdin);
	dlina = strlen(mass);
	spaces = (80 - dlina)/2;
	printf("%*s\n", spaces, mass);
	return 0;
}
