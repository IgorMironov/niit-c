//
//  main.c
//  task3
//
//  Created by Yulia Vorobjeva on 12.02.17.
//  Copyright © 2017 Yulia Vorobjeva. All rights reserved.
//

#include <stdio.h>
#define N 256

typedef struct sum {
    int symbol;
    float count;
} SYM;

int main(int argc, const char * argv[])
{
    SYM arrays[N];
    int i=0,j=0, k=0;
    char symb;
    int count;
    float counts[N];
    float temp;
    for(i=0;i<N;i++)
    {
        arrays[i].symbol = 0;
        arrays[i].count = 0;
    }
    
    FILE * fp = fopen(argv[1],"rt");
    while((symb=fgetc(fp))!=EOF)
    {
        count++;
        for(i=0;i<N;i++)
        {
            if(symb==arrays[i].symbol)
            {
                counts[i]++;
                break;
            }
            if(arrays[i].symbol==0)
            {
                arrays[i].symbol = symb;
                counts[i]++;
                k++;
                break;
            }
            
        }
    }
    fclose(fp);
    
    for(i=1;i<k;i++)
    {
        arrays[i].count = (float)counts[i]/count;
    }
    for(i=1;i<k;i++)
    {
        for(j=0;i<k-1;j++)
            if(arrays[j].count<arrays[j+1].count)
            {
                temp = arrays[j].count;
                arrays[j].count=arrays[j+1].count;
                arrays[j+1].count=temp;
                
                temp = arrays[j].symbol;
                arrays[j].symbol=arrays[j+1].symbol;
                arrays[j+1].symbol=temp;
            }
    }
    for(i=0;i<k;i++)
        printf("%c - %f", arrays[i].symbol, arrays[i].count);
    return 0;
}



