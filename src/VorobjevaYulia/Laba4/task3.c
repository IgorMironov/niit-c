//programm checks if a string is a palindrome

#include <stdio.h>
#define N 256

int main()
{
    char text[N];
    char *first, *last;
    int i;
    
    printf("enter your text\n");
    fgets(text, N, stdin);
    
    unsigned long int len = strlen(text)-2;
    first = &text[0];
    last = &text[len];
    
    for (i=0; i<= len/2;i++)
    {
        if (*first == ' ')
            first++;
        else if(*last ==  ' ')
            last--;
        if(*first != *last)
        {
          printf("string is not a palindrome!\n");
            return 1;
        }
    }
    printf("String is a palindrome!\n");
    return 0;
}

