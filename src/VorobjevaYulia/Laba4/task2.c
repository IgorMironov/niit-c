//programm prints words in reverse order



#include <stdio.h>
#define N 256

int main()
{
    char text[N];
    char *firstLetters[N/2];
    char *p;
    int i, inWord = 0, count = 0;
    printf("Enter your text:\n");
    fgets(text, N, stdin);
    text[strlen(text)-1] = 0;
    while(text[i])
    {
        if(text[i]!= ' ' && inWord == 0)
        {
            firstLetters[count++] = text + i;
            inWord=1;
        }
        else if(text[i]==' '&& inWord == 1)
            inWord=0;
        i++;
    }
    
    printf("your text in reverse order\n");
    for(i=--count;i>=0;i--)
    {
        p = firstLetters[i];
        while((*p!= ' ')&&(*p != '\0'))
            putchar(*p++);
        putchar(' ');
    }
}
