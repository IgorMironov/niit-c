#include <stdio.h>

#define N 20

int main()
{
    char names[N][N];
    int i, age, oldAge, youngAge, relatives;
    char *oldest, *youngest;
    printf("How many relatives do you have?\n");
    scanf("%d", &relatives);
    
    printf("enter a name and age of first human:");
    scanf("%s %d", names[0], &age);
    oldAge = youngAge = age;
    oldest = youngest = names[0];
    
    for(i=1; i < relatives; i++)
    {
        printf("enter a name and age of next human");
        scanf("%s %d", names[i], &age);
        if(age>oldAge)
        {
            oldAge = age;
            oldest = names[i];
        }
        else if(age<youngAge)
        {
            youngAge = age;
            youngest = names[i];
        }
    }
    
    printf("The youngest human is %s and age is %d\n", youngest, youngAge);
    printf("The oldest human is %s and age is %d\n", oldest, oldAge);
    return 0;
    
}
