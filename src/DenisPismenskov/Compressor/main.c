
#include "Compressor.h"

int main(int argc, char *argv[])
{
	if (argc < 2)
	{
		printf("No files.\n");
		return 1;
	}
	if (strcmp(argv[1], "-c") == 0)
		Compressor(argv[2]);
	else if (strcmp(argv[1], "-d") == 0)
		Decompressor(argv[2]);
	else
	{
		printf("Unsupported type.\n");
		return 2;
	}
	return 0;
}