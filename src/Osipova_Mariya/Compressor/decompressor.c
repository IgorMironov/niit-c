#include <stdio.h>
#include <string.h>
#include <stdlib.h>

struct SYM
{
	char symb;
	float vstr;
	char code[256];
	struct SYM * left;
	struct SYM * right;
};
typedef struct SYM SYM;

struct BYTE
{
	unsigned char a1 : 1;
	unsigned char a2 : 1;
	unsigned char a3 : 1;
	unsigned char a4 : 1;
	unsigned char a5 : 1;
	unsigned char a6 : 1;
	unsigned char a7 : 1;
	unsigned char a8 : 1;
};
typedef struct BYTE BYTE;

union BYTES
{
	unsigned char symb;
	BYTE byte;
};
typedef union BYTES BYTES;

union TEST
{
	unsigned char a1;
	unsigned char a2;
	int a;
};
typedef union TEST TEST;

SYM * buildTree(SYM *psym[], int N);
void makeCodes(SYM *root);
unsigned char makeTempFile(FILE * fIN, FILE * fOUT, SYM * arr);
void makeHeader(FILE * fOUT, int kol, SYM * arr, unsigned char tail, char * name);
void compress(FILE * fileOUTtmp, FILE * fileOUT);
void wrongFormat();
void decompress(FILE * fileIN, FILE * fileOUTtmp, unsigned char tail);
int getSymbFromTree(SYM * root, FILE * fileIN);
void buildFile(SYM * tree, FILE * fileOUTtmp, FILE * fileOUT);

int compare(const void *a, const void *b)
{
	SYM *a1, *b1;
	a1 = (SYM*)a;
	b1 = (SYM*)b;
	if (a1->vstr > b1->vstr)
		return -1;
	else
		return 1;
}

int main(int argc, char*argv[])
{
	FILE *fileIN;
	FILE *fileOUTtmp;
	FILE *fileOUT;
	unsigned char ch;
	long count = 0;
	int i, c;
	int kol;
	unsigned char tail;
	SYM arr[256];
	SYM **psym;
	SYM * tree;

	if (argc < 3 || argc > 4)
	{
		wrongFormat();
		return 1;
	}
	/* =====������=====*/
	if (argv[1][0] == '-' && (argv[1][1] == 'c' || argv[1][1] == 'C'))
	{
		if (argc != 4)
		{
			wrongFormat();
			return 1;
		}
		printf("Compressing...\n");
		for (i = 0; i<256; i++)
		{
			arr[i].vstr = 0.0;
			arr[i].symb = 0;
			arr[i].code[0] = 0;
			arr[i].left = NULL;
			arr[i].right = NULL;
		}

		fileIN = fopen(argv[2], "rb");
		if (!fileIN)
		{
			printf("Can't open file %s\n", argv[2]);
			return 1;
		}

		while ((c = fgetc(fileIN)) != -1)
		{
			ch = (unsigned char)c;
			arr[ch].symb = ch;
			arr[ch].vstr++;
			count++;
		}
		printf("%d Kbytes --->", count / 1024);

		qsort(arr, 256, sizeof(SYM), compare);

		for (kol = 0; kol < 256 && arr[kol].vstr > 0; kol++) {}

		for (i = 0; i<kol; i++)
		{
			arr[i].vstr = arr[i].vstr / count;
		}

		//���������� ������    
		psym = (SYM**)malloc(kol*sizeof(SYM*));
		for (i = 0; i<kol; i++)
		{
			psym[i] = &arr[i];
		}
		tree = buildTree(psym, kol);

		makeCodes(tree);

		fileOUTtmp = fopen("temp.tmp", "wb");
		tail = makeTempFile(fileIN, fileOUTtmp, arr);
		fclose(fileIN);
		fclose(fileOUTtmp);

		fileOUT = fopen(argv[3], "wb");
		if (!fileOUT)
		{
			printf("Can't create file %s\n", argv[3]);
			return 1;
		}
		makeHeader(fileOUT, kol, arr, tail, argv[2]);

		fileOUTtmp = fopen("temp.tmp", "rb");
		compress(fileOUTtmp, fileOUT);
		printf("%d Kbytes\n", ftell(fileOUT) / 1024);
		fclose(fileOUTtmp);
		fclose(fileOUT);
		unlink("temp.tmp");

		return 0;
	}
	/* =====����������=====*/
	else if (argv[1][0] == '-' && (argv[1][1] == 'd' || argv[1][1] == 'D'))
	{
		//������ ���������
		char buf[200];
		fileIN = fopen(argv[2], "rb");
		fread(buf, 1, 3, fileIN);
		if (buf[0] != 'A' || buf[1] != 'B' || buf[2] != 'C')
		{
			printf("������������ ������ �����: %s\n", argv[2]);
			fclose(fileIN);
			return 1;
		}

		i = 0;
		if (argc == 4)
		{
			for (; argv[3][i] != 0; i++)
			{
				buf[i] = argv[3][i];
			}
			for (; (c = fgetc(fileIN)) != 0;)	{}
		}

		if (argc == 3 || buf[i - 1] == '\\')
		{
			for (; (c = fgetc(fileIN)) != 0; i++)
			{
				ch = (unsigned char)c;
				buf[i] = ch;
			}
		}
		buf[i] = 0;
		fileOUT = fopen(buf, "wb");

		tail = (unsigned char)fgetc(fileIN);
		fread(&kol, 4, 1, fileIN);
		for (i = 0; i<kol; i++)
		{
			fread(&arr[i].symb, 1, 1, fileIN);
			fread(&arr[i].vstr, 4, 1, fileIN);
		}

		fileOUTtmp = fopen("tempDecomp.tmp", "wb");
		decompress(fileIN, fileOUTtmp, tail);
		fclose(fileOUTtmp);
		fclose(fileIN);

		//���������� ������
		psym = (SYM**)malloc(kol*sizeof(SYM*));
		for (i = 0; i<kol; i++)
		{
			psym[i] = &arr[i];
		}
		tree = buildTree(psym, kol);

		fileOUTtmp = fopen("tempDecomp.tmp", "rb");

		buildFile(tree, fileOUTtmp, fileOUT);
		fclose(fileOUTtmp);
		fclose(fileOUT);
		unlink("tempDecomp.tmp");

		return 0;
	}
	else
	{
		wrongFormat();
		return 1;
	}
}

//////////////////////////////////////////////////////////////����� MAIN/////////////////////////////////////////////////////////////////////////////

SYM* buildTree(SYM *psym[], int N)
{
	int i;
	SYM *temp = (SYM*)malloc(sizeof(SYM));
	temp->vstr = psym[N - 2]->vstr + psym[N - 1]->vstr;
	temp->left = psym[N - 1];
	temp->right = psym[N - 2];
	temp->code[0] = 0;
	temp->symb = '#';
	if (N == 2)
		return temp;

	for (i = N - 3; i >= 0; i--)
	{
		if (psym[i]->vstr < temp->vstr)
		{
			psym[i + 1] = psym[i];
		}
		else
		{
			psym[i + 1] = temp;
			break;
		}
	}
	if (i<0)
	{
		psym[0] = temp;
	}
	return buildTree(psym, N - 1);
}

void makeCodes(SYM *root)
{
	if (root->left)
	{
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		makeCodes(root->left);
	}
	if (root->right)
	{
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		makeCodes(root->right);
	}
}

unsigned char makeTempFile(FILE * fIN, FILE * fOUT, SYM * arr)
{
	char ch;
	int i, n, c, count = 0;
	fseek(fIN, 0L, SEEK_SET);
	while ((c = fgetc(fIN)) != -1)
	{
		ch = (unsigned char)c;
		for (i = 0;; i++)
		{
			if (arr[i].symb == ch)
			{
				for (n = 0; arr[i].code[n] != 0; n++)
				{
					fwrite(&(arr[i].code[n]), 1, 1, fOUT);
					count++;
				}
				break;
			}
		}
	}
	return count % 8;     //������ ������
}

void makeHeader(FILE * fOUT, int kol, SYM * arr, unsigned char tail, char * name)
{
	int i;
	fwrite("ABC", 3, 1, fOUT);
	for (i = 0; name[i] != 0; i++) {}
	for (; i>-1 && name[i] != '\\'; i--) {}
	i++;
	for (; name[i] != 0; i++)
	{
		fwrite(&name[i], 1, 1, fOUT);
	}
	fwrite("\0", 1, 1, fOUT);
	fwrite(&tail, 1, 1, fOUT);
	fwrite(&kol, 4, 1, fOUT);

	for (i = 0; i<kol; i++)
	{
		fwrite(&arr[i].symb, 1, 1, fOUT);
		fwrite(&arr[i].vstr, 4, 1, fOUT);
	}
}

void compress(FILE * fileOUTtmp, FILE * fileOUT)
{
	int c;
	unsigned char ch[8];
	BYTES b;
	int i;
	char end = 0;
	fseek(fileOUTtmp, 0L, SEEK_SET);
	b.symb = 0;
	while (1)
	{
		for (i = 0; i<8; i++)
		{
			if ((c = fgetc(fileOUTtmp)) != -1)
			{
				ch[i] = (unsigned char)c;
			}
			else
			{
				end = 1;
				break;
			}
		}
		b.byte.a1 = ch[0] - '0';
		b.byte.a2 = ch[1] - '0';
		b.byte.a3 = ch[2] - '0';
		b.byte.a4 = ch[3] - '0';
		b.byte.a5 = ch[4] - '0';
		b.byte.a6 = ch[5] - '0';
		b.byte.a7 = ch[6] - '0';
		b.byte.a8 = ch[7] - '0';
		fwrite(&(b.symb), 1, 1, fileOUT);
		if (end) { break; }
	}
}

void decompress(FILE * fileIN, FILE * fileOUTtmp, unsigned char tail)
{
	int c;
	unsigned char ch[8];
	BYTES b;
	int i;
	TEST t;

	c = fgetc(fileIN);
	t.a = c;
	b.symb = (unsigned char)c;
	ch[0] = b.byte.a1 + '0';
	ch[1] = b.byte.a2 + '0';
	ch[2] = b.byte.a3 + '0';
	ch[3] = b.byte.a4 + '0';
	ch[4] = b.byte.a5 + '0';
	ch[5] = b.byte.a6 + '0';
	ch[6] = b.byte.a7 + '0';
	ch[7] = b.byte.a8 + '0';

	while ((c = fgetc(fileIN)) != -1)
	{
		for (i = 0; i<8; i++)
		{
			fwrite(&ch[i], 1, 1, fileOUTtmp);
		}
		b.symb = (unsigned char)c;
		ch[0] = b.byte.a1 + '0';
		ch[1] = b.byte.a2 + '0';
		ch[2] = b.byte.a3 + '0';
		ch[3] = b.byte.a4 + '0';
		ch[4] = b.byte.a5 + '0';
		ch[5] = b.byte.a6 + '0';
		ch[6] = b.byte.a7 + '0';
		ch[7] = b.byte.a8 + '0';
	}

	for (i = 0; i<tail; i++)
	{
		fwrite(&ch[i], 1, 1, fileOUTtmp);
	}
}

void wrongFormat()
{
	printf("������������ ������!\n���������:\ndecompressor.exe [-c <�������� ����> <��������� ����>][-d <�������� ����> [<����� ����������>][<��������� ����>]]\n\n");
	printf("-c\tdecompress <�������� ����> to <��������� ����>\n-d\tdecompress <�������� ����> to <����� ����������> or <��������� ����>\n\n");
	printf("������:\n\t\tdecompressor.exe -c d:\\text.txt d:\\compessed.ABC\n\t\tdecompressor.exe -d compressed.ABC d:\\\n\t\tdecompressor.exe -d d:\\compressed.ABC d:\\decompressed.txt\n\n");
}

int getSymbFromTree(SYM * root, FILE * fileIN)
{
	unsigned char ch;
	int c;
	if (root->code[0] != 0)
	{
		return root->symb;
	}

	c = fgetc(fileIN);
	if (c == -1)
	{
		return -1000;
	}
	ch = (unsigned char)c;
	if (ch == '0')
	{
		return getSymbFromTree(root->left, fileIN);
	}
	else
	{
		return getSymbFromTree(root->right, fileIN);
	}
}

void buildFile(SYM * tree, FILE * fileOUTtmp, FILE * fileOUT)
{
	int c;
	unsigned char ch;
	while ((c = getSymbFromTree(tree, fileOUTtmp)) != -1000)
	{
		ch = (unsigned char)c;
		fwrite(&ch, 1, 1, fileOUT);
	}
}