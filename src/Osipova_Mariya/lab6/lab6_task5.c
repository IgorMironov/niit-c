/*�������� ���������, ������� �������� ����� ���������� N-���
����� ���� ���������. ������������� ����� ������� ��������
��� N � ��������� �� 1 �� 40 (��� � ������ ��������� �� �������)
�� ����� � � ����*/

#include <stdio.h>
#include <math.h>
#include <time.h>
#include <string.h>
#include <windows.h>

int fibonachi(int N)
{
	if (N>2)
	{
		int summ;
		summ = fibonachi(N - 1) + fibonachi(N - 2);
		return summ;
	}
	else
	{
		return 1;
	}
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	int N, i, fib;
	clock_t timeStart, timeEnd;
	float timework;
	FILE *fp;
	double summ;
	N = 40;

	fp = fopen("tabl.txt", "wt");
	for (i = 2; i <= N; i++)
	{
		timeStart = clock();
		fib = fibonachi(i);
		timeEnd = clock();
		timework = ((float)(timeEnd - timeStart) / CLOCKS_PER_SEC);
		fprintf(fp, "%d�������\t%f\n", i, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);
		//printf("%d�������� \t����� %d \t\t����� ���������� %f sec\n", i, fib, (float)(timeEnd - timeStart) / CLOCKS_PER_SEC);
		printf("%d�������� \t����� %d \t\t����� ���������� %f sec\n", i, fib, timework);

	}
	fclose(fp);
}