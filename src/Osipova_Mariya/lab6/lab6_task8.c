/*�������� ���������, ������� ��������� ������������� ���������
��������������� ���������, ��������� � ���� ��������� ������-
��� ������. ������������� ��������� 4-� �������� ��������. ��-
����� ���������� ������������ �������� ��������*/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <windows.h>

#define N 100

char * firstNotDigit(const char * str) /*������� ���������� ������ ������ �� �����*/
{
	while (1)
	{
		if (!isdigit(*str))
			return str;
		str++;
	}
	
}

char * findRoot(const char * str)/*���� ��� ������ ������� ����*/
{
	char * sign = NULL;
	int count = 0;
	int i;

	for (i = 0; str != '\0'; i++)
	{
		if (count == 1 && sign != NULL && *(sign + 1) != '(')
			return str;
		if (count == 1 && sign == NULL && isdigit(*(str)))
			return firstNotDigit(str);
		if (*str == '\0')
			return NULL;
		if (*str == '(')
			count++;
		else if (*str == ')')
			count--;
		else if (*str == '+' || *str == '-' || *str == '*' || *str == '/')
			sign = str;
		str++;
	}
}

char partition(const char * buf, char exprl[], char exprr[]) /* ���� buf �������� ������ ����� �������� �� � exprl and ������� ����� */
{
	int i;
	char * proot = findRoot(buf);
	char sign;

	if (proot == NULL)
	{
		for (i = 0; buf[i] != '\0'; i++)
			exprl[i] = buf[i];
		exprl[i] = '\0';
		return buf[0];
	}

	sign = *proot;
	for (i = 0; (buf + i + 1) != proot; i++)
		exprl[i] = buf[i + 1];
	exprl[i] = '\0';

	for (i = 1; *(proot + i) != '\0'; i++)
		exprr[i - 1] = proot[i];
	exprr[i - 2] = '\0';

	return sign;
}

int eval(char * buf)
{
	char exprl[N];
	char exprr[N];
	char sign = partition(buf, exprl, exprr);

	if (isdigit(sign))
		return atoi(exprl);
	else
	{
		switch (sign)
		{
		case '+':
			return eval(exprl) + eval(exprr);
		case '-':
			return eval(exprl) - eval(exprr);
		case '*':
			return eval(exprl) * eval(exprr);
		case '/':
			return eval(exprl) / eval(exprr);
		default:
			break;
		}
	}
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	char arr[50] = { 0 };
	printf("������� ���������\n");
	scanf("%s", arr);

	printf("%d\n", eval(arr));
	return 0;
}