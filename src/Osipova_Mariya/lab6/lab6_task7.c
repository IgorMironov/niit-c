/*�������� ���������, ������� ������� ����� �� ���������*/
#include <stdio.h>
#include <string.h>
#include <windows.h>


void refreshScr(char **arr, int shir, int vis);
int findExit(char **arr, int manX, int manY, int shir, int vis);

int main(int argc, char*argv[])
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������
	FILE *fileIN;
	long fileLen;
	char **arr;
	char buf[120];
	int shir, vis, i, n, y, x, manX = -1, manY = -1;

	//����������� �������� ���������
	fileIN = fopen(argv[1], "rt");
	if (!fileIN) 
	{ 
		puts("Open file error!\n"); exit(1); 
	}
	fseek(fileIN, 0L, SEEK_END);
	fileLen = ftell(fileIN);
	fseek(fileIN, 0L, SEEK_SET);
	fgets(buf, 120, fileIN);
	shir = strlen(buf);
	vis = fileLen / (shir + 1);
	shir--;
	fseek(fileIN, 0L, SEEK_SET);

	//������ ��������� �� �����
	arr = (char**)calloc(vis, sizeof(char*));
	for (i = 0; i<vis; i++)
	{
		arr[i] = (char*)calloc(shir, sizeof(char));
		fgets(arr[i], shir + 2, fileIN);
	}
	fclose(fileIN);

	//���������� �����������
	refreshScr(arr, shir, vis);

	//����� ��������� ���������
	for (y = 0; y<vis; y++)
	{
		for (x = 0; x<shir; x++)
		{
			if (arr[y][x] == 'x')
			{
				manX = x;
				manY = y;
				break;
			}
		}
		if (manX>-1) { break; }
	}

	//����� ������
	if (findExit(arr, manX, manY, shir, vis))
	{
		printf("\n����� ������!\n\n");
	}
	else
	{
		printf("\n����� �� ������...\n\n");
	}

	return 0;
}

void refreshScr(char ** arr, int shir, int vis)
{
	int x, y;
	system("cls");
	for (y = 0; y<vis; y++)
	{
		for (x = 0; x<shir; x++)
		{
			printf("%c", arr[y][x]);
		}
		printf("\n");
	}
}

int findExit(char **arr, int manX, int manY, int shir, int vis)
{
	//������� � ���������� ���������� ����� 
	int stepX[4] = { 0, 1, 0, -1 };
	int stepY[4] = { 1, 0, -1, 0 };
	int i;

	Sleep(600);
	arr[manY][manX] = 'x';
	refreshScr(arr, shir, vis);

	for (i = 0; i<4; i++)
	{
		//���� ����� �����
		if (manX + stepX[i] < 0 || manX + stepX[i] >= shir || manY + stepY[i] < 0 || manY + stepY[i] >= vis)
		{
			return 1;
		}

		//���� ����� ������ ������
		if (arr[manY + stepY[i]][manX + stepX[i]] == ' ')
		{
			arr[manY][manX] = '+';
			//���� ��������� ������ � ������
			if (findExit(arr, manX + stepX[i], manY + stepY[i], shir, vis))
			{
				return 1;
			}
		}
	}
	//���� ���� ������
	arr[manY][manX] = '.';
	return 0;
}