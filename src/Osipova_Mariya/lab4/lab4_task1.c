//�������� ���������, ������� ��������� ������������ ������ ������� -
//�� ����� � ����������, � ����� ��������� �� � ������� �������� -
//��� ����� ������

#include <stdio.h>
#include <windows.h>

#define _CRT_SECURE_NO_WARNINGS
#define N 20 /*������������ ���������� �����*/
#define DL 80 /*������������ ���������� �������� � ������*/

//����� ��������� ���������
int compare(const void *a, const void *b)
{
	if (strlen(*(char**)a) > strlen(*(char**)b)) //(char**)a - ���������� � � ���� char**
		return 1;
	else
		return -1;
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 

	int kol = 0; /*���������� ��������� �����*/
	char massiv[N][DL];
	char * p[N]; /*������ ����������*/
	int i;

	printf("������� ������\n");

	for (i = 0; i < N; i++)
	{
		fgets(&massiv[i][0], DL, stdin);

		if (massiv[i][0] == '\n')
		{
			break;
		}
		else
		{
			kol++;
			p[i] = &massiv[i][0];
		}
	}

	qsort(p, kol, sizeof(p[0]), compare);
	for (i = 0; i < kol; i++)
	{
		printf("%d - %s\n", i + 1, p[i]);
	}
	return 0;
}
