/*�������� ���������, ������� ������ ������� ������������� ���-
����� ��� ������������� �����, ��� �������� ������� � ������-
��� ������. ��������� ������ �������� �� ����� ������� �����-
��������, ��������������� �� �������� �������*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>

struct SYM
{
	char symb;
	float vstr;
};
typedef struct SYM SYM;

int compare(const void *a, const void *b)
{
	SYM *a1, *b1;
	a1 = (SYM*)a;
	b1 = (SYM*)b;
	if (a1->vstr > b1->vstr)
		return -1;
	else
		return 1;
}

int main(int argc, char*argv[])
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	FILE *fileIN;
	char ch;
	int count = 0;
	int i, kol;
	SYM arr[256];

	for (i = 0; i<256; i++)
	{
		arr[i].vstr = 0.0;
	}

	fileIN = fopen(argv[1], "rt");
	if (!fileIN)
		printf("������! ���� �� ������ %s\n");

	while ((ch = fgetc(fileIN)) != -1)
	{
		arr[ch].symb = ch;
		arr[ch].vstr++;
		count++;
	}

	fclose(fileIN);

	qsort(arr, 256, sizeof(SYM), compare);

	i = 0;
	while (arr[i].vstr > 0){ i++; }
	kol = i;

	for (i = 0; i<kol; i++)
	{
		arr[i].vstr = arr[i].vstr / count;
	}

	for (i = 0; i<kol; i++)
	{
		char symb[3];
		symb[0] = arr[i].symb;
		symb[1] = 0;
		symb[2] = 0;
		if (symb[0] == '\n')
		{
			symb[0] = '\\';
			symb[1] = 'n';
		}
		else if (symb[0] == '\t')
		{
			symb[0] = '\\';
			symb[1] = 't';
		}
		printf("%d ������ '%s':\t%.5f\n", i, symb, arr[i].vstr);
	}
	return 0;
}