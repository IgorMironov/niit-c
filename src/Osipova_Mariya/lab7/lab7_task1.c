/*�������� ���������, ��������� ��������� ������ � �������� �
�������� � �� ����� � ������������ � ����������� ����� ������*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <windows.h>

struct Country
{
	char country_name[5];
	char region_code[5];
	char region_name[256];
	struct Country * next;
};

typedef struct Country CountryElem;

CountryElem * newRecOfList(void) /*������� ������� ����� ������ ������*/
{
	CountryElem * new_rec = (CountryElem*)malloc(sizeof(CountryElem));
	new_rec->next = NULL;
	return new_rec;
}

void freeList(CountryElem * head) /*�������, ������� ������� ������*/
{
	if (head->next != NULL)
		freeList(head->next);
	free(head);
}

void loadList(CountryElem * head, FILE * fp) /*���������� ������*/
{
	int ch;
	int i = 0;
	if ((ch = fgetc(fp)) == '\n' || ch == EOF)
		return;

	ungetc(ch, fp);
	while ((ch = fgetc(fp)) != ',')
		head->country_name[i++] = ch;
	head->country_name[i] = '\0';

	i = 0;
	while ((ch = fgetc(fp)) != ',')
		head->region_code[i++] = ch;
	head->region_code[i] = '\0';

	i = 0;
	while ((ch = fgetc(fp)) != '\n' && ch != EOF)
		if (ch != '\"')                         
			head->region_name[i++] = ch;
	head->region_name[i] = '\0';

	head->next = newRecOfList();
	loadList(head->next, fp);
}

void printList(CountryElem * list)
{
	printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
	if (list->next == NULL)
		return;
	else
		printList(list->next);
}

void printCountry(CountryElem * list, char * country)
{
	if (!strcmp(list->country_name, country))
		printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
	if (list->next == NULL)
		return;
	else
		printCountry(list->next, country);
}

void printRegion(CountryElem * list, char * region)
{
	if (!strcmp(list->region_code, region))
		printf("%s %s %s\n", list->country_name, list->region_code, list->region_name);
	if (list->next == NULL)
		return;
	else
		printRegion(list->next, region);
}

int main()
{
	SetConsoleCP(1251); //��������� ������� �������� win-cp 1251 � ����� �����
	SetConsoleOutputCP(1251); //��������� ������� �������� win-cp 1251 � ����� ������ 
	FILE *fp;
	int search=0;
	char arr[5] = { 0 };
	CountryElem * list = newRecOfList();
	fp = fopen("fips10_4.csv", "rt");
	if (!fp)
	{
		printf("Error file\n");
		return -1;
	}
	
	while (getc(fp) != '\n');
	loadList(list, fp);
	fflush(stdin);
	printf("��� ������ �� ���������� ����������� ������ ������� - �, ��� ������ ������� - r\n");
	search=getchar();

	printf("������� �������� ��� ������\n");
	scanf("%s", arr);

	if (search=='c')
			printCountry(list, arr);
	else if (search == 'r')
			printRegion(list, arr);
	else 
	{
		printf("������ �����, ����� ����\n"); 
		printList(list);
	}

	freeList(list);
	return 0;
}