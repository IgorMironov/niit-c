#pragma once
struct REGION //�������� �������� �� 3 ��-��
{
	char iso[3];
	char code[3];
	char region[256];
};

typedef struct REGION TREGION; //�������� �������� ����, ����� �� ������� struct - ���� ���������
typedef TREGION * PREGION; //��������� �� ��������� 

struct ITEM //�������� ��������� �� 3 ��-�� ����������: �� ���������-����-����
{
	PREGION region_rec;
	struct ITEM *next;
	struct ITEM *prev;
};

typedef struct ITEM TITEM;
typedef TITEM * PITEM;

void cleancash();
PITEM createList(PREGION region_rec); //�� ���� ������ ��������� �� 1 "�����" ����� �������� ���-�
PREGION createStuctRegion(char *line); //�������� ��������� �1
PITEM addToTail(PITEM tail, PREGION region_rec); //tail �������� ������ �� ���������� � ������� ��� ��� ������. addToTail ��������� ����� ����� � ����������
PITEM findByCountry(PITEM head, char *iso, PITEM tail); //�������� ����� ������ ������ � ��� ������� ����������. ���������� ����� ��������� � ������� ���-��
PITEM findByRegion(PITEM head, char *region);
void printCountry(PITEM item);