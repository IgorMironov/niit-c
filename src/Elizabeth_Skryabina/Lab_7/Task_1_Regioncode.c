#define _CRT_SECURE_NO_WARNINGS
#include "Task_1_regioncode.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void cleancash()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}

PITEM createList(PREGION region_rec) //�������� 1-�� ���������
{
	PITEM item = (PITEM)malloc(sizeof(TITEM)); //�������� ����� ��� 3 ��-��
	item->region_rec = region_rec; //��-�� �� ���
	item->prev = NULL;
	item->next = NULL; //������� 1-�� ��-��, ������� ���������� � ���� � ���� ����!
	return item;
}

PREGION createStuctRegion(char *line) //�������� ������ �� ������
{
	int i = 0;
	PREGION rec = (PREGION)malloc(sizeof(TREGION));
	while (*line && *line != ',') //���� �� ������� � ���������� ������� � 1 ��������� � ����
		rec->iso[i++] = *line++;
	rec->iso[i] = 0; //���������� � ���� ��������� �������� 0
	line++; //������������ �������

	i = 0;
	while (*line && *line != ',') //���� �� ������� � ���������� ������� � 1 ��������� � ����
		rec->code[i++] = *line++;
	rec->code[i] = 0; //���������� � ���� ��������� �������� 0
	line++; //������������ �������

	i = 0;
	*line++;
	while (*line != '\"') //� ���� �� ����� ������, ��������� � 1 ��������� � �������
		rec->region[i++] = *line++;
	rec->region[i] = 0; //���������� � �������� ��������� �������� 0
	return rec;
}

PITEM addToTail(PITEM tail, PREGION region_rec) //���������� ������ �1
{
	PITEM item = createList(region_rec); //������� ��-��
	if (tail != NULL) //���� �� 0 (�� ������ ������), �� ���������� �������� � ������������ ��-� ���������� ���������. 
	{
		tail->next = item;
		item->prev = tail;
	}
	return item;
}

PITEM findByCountry(PITEM head, char *iso, PITEM tail) //����� �� ������
{
	int i = 0;
	while (head)
	{
		if (strcmp(head->region_rec->iso, iso) == 0)
		{
			printCountry(head);
			i++;
		}
		head = head->next;
	}
	if (i == 0)
		printf("Not found!\n");
	return 0;
}

PITEM findByRegion(PITEM head, char *region) //����� �� �������
{
	while (head)
	{
		if (strcmp(head->region_rec->region, region) == 0)
		{
			printCountry(head);
			return 0;
		}
		head = head->next;
	}
	printf("Not found!\n");
	return 0;
}

void printCountry(PITEM item) //������ ������
{
	if (item != NULL)
	{
		printf("%s ", item->region_rec->iso);
		printf("%s ", item->region_rec->code);
		printf("%s\n", item->region_rec->region);
	}
}