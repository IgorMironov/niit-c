/*print words with random letters (file)
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <string.h>
#include <time.h>
#define N 256

int GetWords(char *str, char **pwords);
void Changeletters(char **pwords, int i);
void PrintWord(char **pwords, int i);

int main()
{
	char str[N] = { 0 };
	char *pwords[N];
	int count = 0, numRandWord,i=0;
	srand(time(NULL));
	FILE *fp;
	fp = fopen("Task_3_read.txt", "rt");
	if (fp == NULL)
	{
		perror("File:");
		return 1;
	}

	while (fgets(str, N, fp) != 0)
	{
		str[strlen(str) - 1] = 0;
		count = GetWords(str, pwords);
		i = 0;
		while (i<count)
			Changeletters(pwords, i++);
		putchar('\n');
	}
	fclose(fp);
	return 0;
}

int GetWords(char *str, char **pwords)
{
	int count = 0, inWord = 0, i = 0;
	while (str[i])
	{
		if (str[i] != ' ' && inWord == 0)
		{
			inWord = 1;
			pwords[count++] = str + i;
		}
		else
			if (str[i] == ' ' && inWord == 1)
				inWord = 0;
		i++;
	}
	return count;
}

void Changeletters(char **pwords, int i)
{
	char arrword[N] = { 0 };
	int j = 0;
	while (*pwords[i] != ' '&&*pwords[i] != '\0')
	{
		arrword[j] = *pwords[i];
		j++;
		pwords[i]++;
	}
	PrintWord(arrword, j);
	return 0;
}

void PrintWord(char *arrword, int length)
{
	int numrandletter;
	putchar(arrword[0]);
	while (length > 2)
	{
		numrandletter = rand() % (length-2) + 1;
		putchar(arrword[numrandletter]);
		while (arrword[numrandletter] != '\0')
		{
			arrword[numrandletter] = arrword[numrandletter+1];
			numrandletter++;
		}
		length--;
	}
	putchar(arrword[1]);
	putchar(' ');
	return 0;
}