/* guess the number
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void cleancash();

int main()
{
	int number, usernumber;
	srand(time(NULL));
	number = rand() % 100 + 1;
	printf("Guess the number!\n");
	printf("Enter number 0-100\n");
	while (1)
	{
		scanf("%d", &usernumber);
		if (usernumber>number)
		{
			printf("Lower!\n");
			cleancash();
		}
		else if (usernumber<number)
		{
			printf("Higher!\n");
			cleancash();
		}
		else {
			printf("Right!!!\n");
			break;
		}
	}
	return 0;
}

void cleancash()
{
	int c;
	do {
		c = getchar();
	} while (c != '\n' && c != EOF);
}