/* 6.1 Fractal image
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

#define ITERATION 3
#define N 27 // 3^ITERATION

void Clean_ArrFactorial(char(*arrFractal)[N]);
void print_ArrFactorial(char(*arrFractal)[N]);
void Create_ArrFractal(char(*ArrFractal)[N], int x, int y, int depth);

int main()
{
	char arrFractal[N][N];
	Clean_ArrFactorial(arrFractal);
	Create_ArrFractal(arrFractal, N / 2, N / 2, ITERATION);
	print_ArrFactorial(arrFractal);
	return 0;
}

void Clean_ArrFactorial(char(*arrFractal)[N])
{
	int i, j;
	for (i = 0; i < N; i++)
		for (j = 0; j < N; j++)
			arrFractal[i][j] = ' ';
}

void print_ArrFactorial(char(*arrFractal)[N])
{
	int i, j;
	for (i = 0; i < N; i++)
	{
		for (j = 0; j < N; j++)
			putchar(arrFractal[i][j]);
		putchar('\n');
	}
}

void Create_ArrFractal(char(*ArrFractal)[N], int x, int y, int depth)
{
	int size;
	if (depth <= 0)
	{
		ArrFractal[x][y] = '*';
		return;
	}
	size = pow(3, depth - 1);
	Create_ArrFractal(ArrFractal, x, y, depth - 1);
	Create_ArrFractal(ArrFractal, x - size, y, depth - 1);
	Create_ArrFractal(ArrFractal, x + size, y, depth - 1);
	Create_ArrFractal(ArrFractal, x, y - size, depth - 1);
	Create_ArrFractal(ArrFractal, x, y + size, depth - 1);
}