/* 6.4 Recursion and Cycle time for Fibonacci
*/
#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <math.h> 
#include <time.h>

void fillArray(int *a, int N);
long int sumRec(int *a, int size);
long int sum�ycle(int *a, int size);

int main()
{
	int *a;  // ��������� �� ������
	int M, N;
	clock_t start, end;
	printf("Enter the number to calculate Fibonacci: ");
	scanf("%d", &M);
	N = powl(2, M);
	a = (int*)malloc(N * sizeof(int)); // ��������� ������
	fillArray(a,N);

	start = clock();
	printf("\nFibonacci number=%ld\n", sumRec(a, N));
	end = clock();
	printf("Recursion time: %f\n", (double)(end - start) / CLOCKS_PER_SEC);

	start = clock();
	printf("\nFibonacci number=%ld\n", sum�ycle(a, N));
	end = clock();
	printf("Cycle time: %f\n", (double)(end - start) / CLOCKS_PER_SEC);

	free(a); // ������������ ������
	getchar();
	return 0;
}

void fillArray(int *a, int N)
{
	int i;
	srand(time(NULL));
	for (i = 0; i < N; i++)
		a[i] = rand() % 10;
}

long int sumRec(int *a, int size)
{
	if (size == 1)
		return *a;
	else
		return sumRec(a, size / 2) + sumRec(a + size / 2, size - size / 2);
}

long int sum�ycle(int *a, int size)
{
	int i;
	long int sum = 0;
	for (i = 0; i < size; i++)
		sum += a[i];
	return sum;
}