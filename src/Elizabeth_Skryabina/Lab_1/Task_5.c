#define _CRT_SECURE_NO_WARNINGS
# include <stdio.h>
# include <string.h>
#define WINwidth 120

int main()
{
	char text[256];
	char format[256] = "%20s\n";
	int i;
	printf("Enter your text\n");
	fgets(text, 256, stdin);
	i = strlen(text);
	printf("formatted text:\n");
	sprintf(format, "%%%ds\n", (i / 2 + WINwidth / 2));
	printf(format, text);
	printf("\n");
	return 0;
}