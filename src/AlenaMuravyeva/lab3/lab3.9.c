/*
 ============================================================================
 Name        :lab3.9.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program that defines the string character sequence
              maximum length
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE 0
#define ARR_SIZE 256

int main()
{
   char userString[ARR_SIZE];

   setlinebuf(stdout);

   printf("enter string\n");
   fgets(userString,ARR_SIZE,stdin);

   int userStringLen = strlen(userString);
   //remove /r
   userString[userStringLen-1] = '\0';
   userStringLen--;

   int curLetterCounter = 1;
   int maxLetterCounter = 1;
   char maxLetter = '\0';

   for(int i = 0; i<userStringLen; i++)
   {
      if(userString[i] == userString[i+1] )
      {
         curLetterCounter++;
      }
      else
      {
         if(curLetterCounter > maxLetterCounter)
         {
            maxLetterCounter = curLetterCounter;
            maxLetter = userString[i];
         }
         curLetterCounter = 1;
      }
   }
   printf("%d\n",maxLetterCounter);
   for(int i = 0; i<maxLetterCounter; i++)
   {
      printf("%c",maxLetter);
   }
   return 0;
}
