/*
 ============================================================================
 Name        : lab4.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program, which is an array of pointers displays
              the words in the usual manner
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LETTERS 80
#define MAX_WORDS 40
#define TRUE 1
#define FALSE 0

int main()
{
   char userString[MAX_LETTERS];
   char *p [MAX_WORDS];
   int firstLeterIndex=0;
   int firstFound = TRUE;
   int secondFound = FALSE;
   int curLetterIndex=0;
   int curWords=0;

   puts("Enter string");

   setlinebuf(stdout);

   fgets(userString,MAX_LETTERS,stdin);

   int len=strlen(userString);
   userString[len-2] = '\0';
   len-=2;

   while( curWords<MAX_WORDS && curLetterIndex < len)
   {
      if(curLetterIndex==0 && userString[curLetterIndex]!=' ')
      {
         firstFound = TRUE;
         firstLeterIndex=curLetterIndex;
      }
      else if(userString[curLetterIndex+1]!=' ' && userString[curLetterIndex]==' ')
      {
         firstFound = TRUE;
         firstLeterIndex=curLetterIndex+1;
      }
      else if ((userString[curLetterIndex]!=' ' && userString[curLetterIndex+1]==' ')|| (userString[curLetterIndex]!=' ' && userString[curLetterIndex+1]=='\0'))
      {
         secondFound = TRUE;
      }
      if (firstFound == TRUE && secondFound == TRUE)
      {
         p[curWords]=&userString[firstLeterIndex];
         curWords++;
         firstFound=FALSE;
         secondFound=FALSE;
      }
      curLetterIndex++;
   }

   printf("Result: ");
   char *curPtr;
   for(int i=curWords-1;i>=0;i--)
   {
      curPtr=p[i];
      while(*curPtr != ' ' && *curPtr != '\0')
      {
         putchar(*curPtr);
         curPtr++;
      }
      putchar(' ');
   }

   return 0;
}
