/*
 ============================================================================
 Name        : lab 6.2.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : The sequence is of Collatz
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>

#define MIN_VALUE 2
#define MAX_VALUE 1000000
#define FOR_THE_FORMULA 3

unsigned long long nextnumber = 0;
int longestSequenceCollapse=0;
int sequenceLen = 0;

int sequenceCollapse(unsigned long long n)
{
   if(n==1)
   {
      return 1;
   }
   if(n%MIN_VALUE==0)
   {
      nextnumber=n/MIN_VALUE;
      sequenceLen++;
      sequenceCollapse(nextnumber);
      //on return from recursion
   }
   else
   {
      sequenceLen++;
      nextnumber=FOR_THE_FORMULA*n+1;
      sequenceCollapse(nextnumber);
      //on return from recursion
   }
   return 1;
}

int main()
{
   unsigned long long sourseN=0;
   for(unsigned long long n=MAX_VALUE; n>1; n--)
   {
      sequenceLen = 0;
      sequenceCollapse(n);
      //on return from recursion
      if(longestSequenceCollapse < sequenceLen)
      {
         longestSequenceCollapse = sequenceLen;
         sourseN=n;
      }
   }

   printf("The longest sequence of collapse at n=%d equal %d\n",sourseN,longestSequenceCollapse);
   return 0;
}
