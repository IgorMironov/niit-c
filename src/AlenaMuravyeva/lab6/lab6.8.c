/*
 ============================================================================
 Name        : lab6.8.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program computes an integer result arithmetic expressions
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define EXPR_BUF_LEN 10

char partition(char *buf, char *expr1, char *expr2);

int eval(char* buf)
{
   char expr1[EXPR_BUF_LEN];
   char expr2[EXPR_BUF_LEN];
   int buflen = strlen(buf);

   if(buflen==1 && buf[0]<=57 && buf[0]>=48)
   {
      return atoi(buf);
   }
   char oper = partition(buf,expr1,expr2);
   switch(oper)
   {
      case '+':
        return eval(expr1) + eval(expr2);
        break;
      case '-':
        return eval(expr1) - eval(expr2);
        break;
     case '*':
        return eval(expr1) * eval(expr2);
        break;
     case '/':
        return eval(expr1) / eval(expr2);
        break;
   }
   return 0;
}
char partition(char *buf, char *expr1, char *expr2)
{
   int len=strlen(buf);
   int openBr=0;
   int currentBr=0;
   char* startExpr1 = NULL;
   char* endExpr1 = NULL;
   char* startExpr2 = NULL;
   char* endExpr2 = NULL;
   char oper = '+';

   for(int i=0;i<len;i++)
   {
      if(i==0 && buf[i]=='(' && buf[len-1]==')')
         continue;
      if(buf[i]=='(')
      {
         currentBr++;
         if(startExpr1 == NULL)
         {
            openBr = currentBr;
            startExpr1=&buf[i];
            continue;
         }
         else if(endExpr1 != NULL && startExpr2 == NULL)
         {
            openBr = currentBr;
            startExpr2=&buf[i];
            continue;
         }
      }
      else if (buf[i]>=48 && buf[i]<=57 )
      {
         //Assume that numbers can occupy only 1 char
         if(startExpr1 == NULL)
         {
            startExpr1=&buf[i];
            endExpr1 = startExpr1;
            if(buf[i+1]=='+' || buf[i+1]=='-' ||
               buf[i+1]=='*' || buf[i+1]=='/')
            {
               oper=buf[i+1];
            }
            continue;
         }
         else if(endExpr1 != NULL && startExpr2 == NULL)
         {
            startExpr2=&buf[i];
            endExpr2 = startExpr2;
            continue;
         }
      }
      else if(buf[i]==')')
      {
         currentBr--;
         //check if it is our closing bracket
         if(openBr == currentBr+1)
         {
            if(startExpr1 != NULL && endExpr1 == NULL)
            {
               endExpr1 = &buf[i];
               if(buf[i+1]=='+' || buf[i+1]=='-' ||
                  buf[i+1]=='*' || buf[i+1]=='/')
               {
                  oper=buf[i+1];
               }
            }
            else if(startExpr2 != NULL && endExpr2 == NULL)
            {
               endExpr2 = &buf[i];
            }
         }
      }
   }
   //copy expr1 and expr2
   int expr1Len = 0;
   int expr2Len = 0;
   expr1Len = (endExpr1 == startExpr1) ? 1:(endExpr1 - startExpr1 + 1)/sizeof(char);
   expr2Len = (endExpr2 == startExpr2) ? 1:(endExpr2 - startExpr2 + 1)/sizeof(char);

   strncpy(expr1, startExpr1, expr1Len );
   strncpy(expr2, startExpr2, expr2Len );
   expr1[expr1Len] = '\0';
   expr2[expr2Len] = '\0';

   return oper;
}
int main(int argc,char* argv[])
{
   if(argc<2)
   {
      printf("Enter the mathematical expression\n");
   }

   int result = eval(argv[1]);
   printf("Result: %d", result);
   return 0;
}
