/*
 ============================================================================
 Name        : lab6.7.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Maze
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRINGS 11
#define MAX_LETTERS 31
#define START_Y 4
#define START_X 12

char maze[MAX_STRINGS][MAX_LETTERS];

void printArray ()
{
   int i,j;
   for(i=0;i<MAX_STRINGS;i++)
   {
      for(j=0; j<MAX_LETTERS-1;j++)
      {
         putchar(maze[i][j]);
      }
   }
}
int searchExitMaze (int x, int y)
{

   if(maze[y][x]=='0')
   {
      return 1;
   }

   if(maze[y][x]!=' ' || maze[y][x] == '*')
   {
      return 0;
   }

   maze[y][x]='.';

   if(searchExitMaze(x,y-1)||searchExitMaze(x+1,y) ||
      searchExitMaze(x,y+1)||searchExitMaze(x-1,y))
    {
       return 1;
    }
   return 1;
}

int main()
{
   int stringNum=0;
   setlinebuf(stdout);
   FILE* fp;
   fp=fopen("labirint.txt","rt");
   if(fp==NULL)
   {
      perror("File:");
      return 1;
   }
   while(stringNum<MAX_STRINGS && fgets(maze[stringNum],MAX_LETTERS,fp))
   {
      stringNum++;
   }

   searchExitMaze(START_X,START_Y);
   printArray ();
   fclose(fp);
   return 0;
}
