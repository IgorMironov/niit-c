/*
 ============================================================================
 Name        : lab 1.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description : Program converts ft to inches and inches to cm
 ============================================================================
 */

#include <stdio.h>
#include <windows.h>

int main()
{
   int ft=0;
   int inches=0;
   double cm=0;
   double ine=0;
   const double ftCoeffient=2.54;
   const double inchesCoeffient=12;

   setlinebuf(stdout);

   SetConsoleCP(1251);
   SetConsoleOutputCP(1251);

   printf ("������� ���� � ����� � ������\n");
   scanf("%d %d",&ft,&inches);

   ine=ft*inchesCoeffient;
   cm=inches*ftCoeffient;

   if (ft>=0 && inches>=0)
   {
      printf("%d ���= %.2f ����\n",ft,ine);
      printf("%d����= %.2f��\n",inches,cm);
   }
   else
   {
      printf("Enter correct value\n");
   }

   return 0;
}
