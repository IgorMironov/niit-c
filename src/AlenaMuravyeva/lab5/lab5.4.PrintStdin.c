/*
 ============================================================================
 Name        : lab.5.4.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   : Your copyright notice
 Description :The program shuffles the words in random order
 ============================================================================
 */
#include "WordsPermutation.h"
#include <stdio.h>

#define ARR_SIZE 80
#define MAX_POINTER 40
#define MAX_GENERATE 21

char userString [ARR_SIZE]={0};
char *ptrArray[MAX_POINTER];

int main()
{
   setlinebuf(stdout);

   printf("Enter string\n");
   fgets(userString,ARR_SIZE,stdin);

   getWords(userString,ptrArray);

   for(int i=0;i<MAX_GENERATE;i++)
   {
      RandPair nextPair=generateRandPair();

      char* tmp= ptrArray[nextPair.randNumber1];
      ptrArray[nextPair.randNumber1]=ptrArray[nextPair.randNumber2];
      ptrArray[nextPair.randNumber2]=tmp;
   }

   printWords();
   return 0;
}
